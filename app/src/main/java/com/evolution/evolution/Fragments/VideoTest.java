package com.evolution.evolution.Fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaRecorder;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.evolution.evolution.Activities.MainPage;
import com.evolution.evolution.Helpers.AppController;
import com.evolution.evolution.Helpers.ConnectionDetector;
import com.evolution.evolution.Helpers.Constants;
import com.evolution.evolution.Helpers.DatabaseHelper;
import com.evolution.evolution.Helpers.SessionManager;
import com.evolution.evolution.Interface.APIInterface;
import com.evolution.evolution.Models.VideoModel;
import com.evolution.evolution.R;
import com.evolution.evolution.Activities.VideoViewActivity;
import com.google.gson.Gson;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by mcclynreyarboleda on 4/13/15.
 */
public class VideoTest extends Fragment implements View.OnClickListener {

    LinearLayout takeavideo, selectgalery;

    ArrayList<VideoModel> item = new ArrayList<>();
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE2 = 200;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private static final String TAG = "Video";

    //Video or IMAGe
    DatabaseHelper db;
    public SessionManager session;
    private String token;
    OkHttpClient okHttpClient;
    private Uri fileUri; // file url to store image/video
    String fileloc = null;
    private static final String ARG_SECTION_NUMBER = "section_number";
    MenuItem send;
    Uri uri;

    private ImageView playvideo;
    private ImageView videoView;
    Dialog dialog4;
    LinearLayout novideo;
    ScrollView withvideo;
    EditText subject, note;
    VideoModel video = new VideoModel();
    String videoid;
    boolean ismaylaman;
    boolean videoba;
    private ConnectionDetector connection;

    long totalSize = 0;
    String jobidselected;

    String tempStarttime, tempLocation, tempendtime, tempendLocation;


    //Dialog
    public AlertDialog dialog;
    public AlertDialog.Builder builder;
    PendingIntent pIntent;

    public VideoTest() {

    }

    public static VideoTest newInstance(int sectionNumber) {
        VideoTest fragment = new VideoTest();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DatabaseHelper(getActivity());
        session = new SessionManager(getActivity());
        token = session.getToken();
        if (savedInstanceState != null) {
            fileUri = Uri.parse(savedInstanceState.getString("file_uri"));
        }
        connection = new ConnectionDetector(getActivity());
        setHasOptionsMenu(true);

        Intent intent = new Intent(getActivity(), MainPage.class);
        pIntent = PendingIntent.getActivity(getActivity(), 0,
                intent,
                PendingIntent.FLAG_CANCEL_CURRENT);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_videotest, container, false);

        takeavideo = (LinearLayout) view.findViewById(R.id.takeapicture);
        selectgalery = (LinearLayout) view.findViewById(R.id.selectgalery);

        novideo = (LinearLayout) view.findViewById(R.id.novideo);
        withvideo = (ScrollView) view.findViewById(R.id.withvideo);

        takeavideo.setOnClickListener(this);
        selectgalery.setOnClickListener(this);

        videoView = (ImageView) view.findViewById(R.id.videoview);
        playvideo = (ImageView) view.findViewById(R.id.playvideo);

        playvideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uri = Uri.parse("file://" + fileloc);
                Intent viewIntent = new Intent("android.intent.action.VIEW", uri);
                viewIntent.setDataAndType(uri, "video/*");
                startActivity(viewIntent);
            }
        });

        subject = (EditText) view.findViewById(R.id.subject);
        note = (EditText) view.findViewById(R.id.note);

        subject.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                db.updatesubject(subject.getText().toString(), "1");
                Log.e("update", "yes" + subject.getText().toString());
            }
        });

        note.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                db.updatenote(note.getText().toString(), "1");
                Log.e("update n", "yes" + note.getText().toString());
            }
        });

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!isDeviceSupportCamera()) {
            Toast.makeText(getActivity(),
                    "Sorry! Your device doesn't support camera",
                    Toast.LENGTH_LONG).show();
        }

        dialog4 = new Dialog(getActivity(), R.style.FullHeightDialog);
        dialog4.setCanceledOnTouchOutside(false);
        dialog4.setContentView(R.layout.loading);
        dialog4.contentMargin(0, 0, 0, -25);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!(db.getAllVideos().size() <= 0)) {
            Glide.with(getActivity()).load(db.getAllVideos().get(0).getVideo_url()).into(videoView);
            fileloc = db.getAllVideos().get(0).getVideo_url();
            novideo.setVisibility(novideo.GONE);
            withvideo.setVisibility(withvideo.VISIBLE);
            selectgalery.setClickable(true);
            subject.setText(db.getAllVideos().get(0).getSubject());
            note.setText(db.getAllVideos().get(0).getNote());
            video = db.getAllVideos().get(0);
            selectgalery.setAlpha(1);
            ismaylaman = true;
            videoba = true;
        } else {
            novideo.setVisibility(novideo.VISIBLE);
            withvideo.setVisibility(withvideo.GONE);
            selectgalery.setClickable(false);
            selectgalery.setAlpha((float) 0.2);
            ismaylaman = false;
            videoba = false;
        }
    }

    private boolean isDeviceSupportCamera() {
        if (getActivity().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    public void videoUpload() {

        if ((db.getAllVideos().size() <= 0)) {
            Dialog.Builder builder = null;
            builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                @Override
                public void onPositiveActionClicked(DialogFragment fragment2) {
                    Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);
                    intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY,
                            MediaRecorder.OutputFormat.MPEG_4);
                    intent.putExtra("EXTRA_VIDEO_QUALITY", 0);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//                                    intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, 25491520L);
                    intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 120);
                    intent.putExtra("android.intent.extras.CAMERA_FACING", 0);
                    startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
                    Time now = new Time();
                    now.setToNow();
                    long time = System.currentTimeMillis();
                    tempStarttime = time + "";
                    tempLocation = getGPS();
                    HashMap<String, String> data = new HashMap<String, String>();
                    data.put("tempStarttime", tempStarttime);
                    data.put("tempLocation", tempLocation);
                    session.createVideoInfo(data);
                    super.onPositiveActionClicked(fragment2);
                }
            };

            ((SimpleDialog.Builder) builder).message("Please set the camera resolution to low quality mode.")
                    .title("Camera")
                    .positiveAction("OK");
            DialogFragment fragment11 = DialogFragment.newInstance(builder);
            fragment11.show(getFragmentManager(), null);

        } else {
            Dialog.Builder builder4 = null;
            builder4 = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                @Override
                public void onPositiveActionClicked(DialogFragment fragment) {

                    Dialog.Builder builder = null;
                    builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                        @Override
                        public void onPositiveActionClicked(DialogFragment fragment2) {
                            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                            fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);
                            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY,
                                    MediaRecorder.OutputFormat.MPEG_4);
                            intent.putExtra("EXTRA_VIDEO_QUALITY", 0);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//                                    intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, 25491520L);
                            intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 120);
                            intent.putExtra("android.intent.extras.CAMERA_FACING", 0);
                            startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
                            Time now = new Time();
                            now.setToNow();
                            long time = System.currentTimeMillis();
                            tempStarttime = time + "";
                            tempLocation = getGPS();
                            HashMap<String, String> data = new HashMap<String, String>();
                            data.put("tempStarttime", tempStarttime);
                            data.put("tempLocation", tempLocation);
                            session.createVideoInfo(data);
                            super.onPositiveActionClicked(fragment2);
                        }
                    };

                    ((SimpleDialog.Builder) builder).message("Please set the camera resolution to low quality mode.")
                            .title("Camera")
                            .positiveAction("OK");
                    DialogFragment fragment11 = DialogFragment.newInstance(builder);
                    fragment11.show(getFragmentManager(), null);

                    super.onPositiveActionClicked(fragment);
                }

                @Override
                public void onNegativeActionClicked(DialogFragment fragment) {
                    super.onNegativeActionClicked(fragment);
                }
            };

            ((SimpleDialog.Builder) builder4).message("Retake video?")
                    .title("Retake")
                    .positiveAction("OK")
                    .negativeAction("CANCEL");
            DialogFragment fragment5 = DialogFragment.newInstance(builder4);

            fragment5.setCancelable(false);
            fragment5.show(getFragmentManager(), null);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.takeapicture:

                if (!(db.getAllJobs().getmJob().size() <= 0)) {
                    videoUpload();
                } else {
                    Toast.makeText(getActivity(), "No Job Found.", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.selectgalery:

                Dialog.Builder builder2 = null;
                builder2 = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                    @Override
                    public void onPositiveActionClicked(DialogFragment fragment) {

                        db.removeVideo();
                        novideo.setVisibility(novideo.VISIBLE);
                        withvideo.setVisibility(withvideo.GONE);
                        selectgalery.setClickable(false);
                        selectgalery.setAlpha((float) 0.2);
                        ismaylaman = false;
                        videoba = false;

                        super.onPositiveActionClicked(fragment);
                    }

                    @Override
                    public void onNegativeActionClicked(DialogFragment fragment) {
                        super.onNegativeActionClicked(fragment);
                    }
                };

                ((SimpleDialog.Builder) builder2).message("Delete video and data?")
                        .title("Delete")
                        .positiveAction("Ok")
                        .negativeAction("CANCEL");
                DialogFragment fragment3 = DialogFragment.newInstance(builder2);

                fragment3.setCancelable(false);
                fragment3.show(getFragmentManager(), null);

                break;
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }


    private void launchUploadActivity() {
        Intent i = new Intent(getActivity(), VideoViewActivity.class);
        i.putExtra("filePath", fileUri.getPath());
        startActivity(i);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
        outState.putString("file_uri", fileUri + "");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                db.removeVideo();
                if (!fileUri.getPath().isEmpty()) {
                    long time = System.currentTimeMillis();
                    String files = fileUri.getPath() + "";

                    VideoModel videotest = new VideoModel();

                    videotest.setVideo_url(files);
                    videotest.setStartCapture(session.getTemStartime());
                    videotest.setStartLocation(session.getTemStarloc());
                    videotest.setEndLocation(getGPS());
                    videotest.setEndCapture("" + time);
                    videotest.setJobID("" + time);
                    tempendtime = time + "";
                    tempendLocation = getGPS();
                    item.add(videotest);

                    db.addVideo(videotest);
                    Toast.makeText(getActivity(),
                            "Video Added", Toast.LENGTH_SHORT)
                            .show();
                    Glide.with(getActivity()).load(fileUri.getPath()).into(videoView);
                    fileloc = fileUri.getPath();
                    video = videotest;
                    ismaylaman = true;
                    videoba = true;

                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(),
                        "Cancelled", Toast.LENGTH_SHORT)
                        .show();
                tempStarttime = null;
                tempLocation = null;
                ismaylaman = false;
            } else {
            }
        } else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE2) {
            Uri selectedImageUri = data.getData();
            if (!fileUri.getPath().isEmpty()) {
                VideoModel videotest = new VideoModel();
                String files = fileUri.getPath() + "";
                Log.e("file loc", files);
                videotest.setVideo_url(files);
                videotest.setGeotag("14.584618333333333, 121.07596333333335");
                item.add(videotest);
                db.addVideo(videotest);
                Toast.makeText(getActivity(),
                        "Video Added", Toast.LENGTH_SHORT)
                        .show();
                video = videotest;
            }
        }
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "AndroidFileUpload");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + "AndroidFileUpload" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }
        return mediaFile;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_video, menu);
        send = (MenuItem) menu.findItem(R.id.save);
//        savemen = (MenuItem) menu.findItem(R.id.savechecklist);
//        updatechecklist.setVisible(false);
//        if (ismaylaman) {
        send.setVisible(true);
//        } else {
//            send.setVisible(false);
//        }
    }

    List<String> categories;


    public void chooseJob(int layout, LayoutInflater inflater) {
        builder = new AlertDialog.Builder(getActivity());
        dialog = builder.create();
        View dialogView = inflater.inflate(layout, null);
        dialog.setView(dialogView);

        final Spinner categorys = (Spinner) dialogView.findViewById(R.id.selectCategory);
        initSpinner(categorys);

        Button close = (Button) dialogView.findViewById(R.id.close);
        Button save = (Button) dialogView.findViewById(R.id.save);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getActivity(), "" + categorys.getSelectedItem(), Toast.LENGTH_SHORT).show();
                uploadVideo(categorys.getSelectedItem().toString());
                jobidselected = categorys.getSelectedItem().toString();
                dialog.dismiss();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public void initSpinner(Spinner spinner) {

        categories = new ArrayList<String>();
        for (int i = 0; i < db.getAllJobs().getmJob().size(); i++) {
            categories.add(db.getAllJobs().getmJob().get(i).getJobID());
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // On selecting a spinner item
                String item = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinnerlocation, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.spinnerlocation);
        spinner.setAdapter(dataAdapter);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.save:
                if (!(db.getAllJobs().getmJob().size() <= 0)) {
                    if (connection.isConnectedToInternet()) {
                        if (videoba) {
                            if ((subject.getText().toString().equals("")) || (note.getText().toString().equals(""))) {
                                Toast.makeText(getActivity(), "Please complete the form.", Toast.LENGTH_SHORT).show();
                            } else {
                                LayoutInflater inflater2 = getActivity().getLayoutInflater();
                                chooseJob(R.layout.daiglog_selectjobvideo, inflater2);
                            }
                        } else {
                            Toast.makeText(getActivity(), "Please complete the form.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "No Job Found.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public String getFileSize() {
        String re = null;
        try {
            File file = new File(fileloc);
            long length = file.length();
            length = length / 1024;
            System.out.println("File Path : " + file.getPath() + ", File size : " + length + " KB");
            re = length + "";
        } catch (Exception e) {
            System.out.println("File not found : " + e.getMessage() + e);
        }
        return re;
    }

    private OkHttpClient getClient() {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(18, TimeUnit.MINUTES);
        client.setReadTimeout(18, TimeUnit.MINUTES);
        return client;
    }

    public void uploadVideo(final String JobID) {
        Log.e("Filesize", getFileSize());
        AppController.notification("", "", true);
        Toast.makeText(getActivity(), "Uploading Video...", Toast.LENGTH_SHORT).show();
//        dialog4.show();
        Map<String, String> params = new HashMap<>();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setClient(new OkClient(getClient()))
                .setEndpoint(Constants.API_URL)
                .build();

//        TypedFile typedFile = new TypedFile("multipart/form-data", new File("/storage/emulated/0/Pictures/AndroidFileUpload/test.mp4"));
        TypedFile typedFile = new TypedFile("multipart/form-data", new File(fileloc));
        APIInterface interfacem = restAdapter.create(APIInterface.class);

        Log.e("to send",
                db.getAllVideos().get(0).getSubject() + "\n" +
                        db.getAllVideos().get(0).getNote() + "\n" +
                        db.getAllVideos().get(0).getStartCapture() + "\n" +
                        session.getToken() + "\n" +
                        db.getAllVideos().get(0).getEndCapture() + "\n" +
                        db.getAllVideos().get(0).getStartLocation() + "\n" +
                        db.getAllVideos().get(0).getEndLocation() + "\n" +
                        typedFile);

        interfacem.uploadVideos(session.getToken(),
                Constants.REQUEST_JOBINFO,
                JobID,
                db.getAllVideos().get(0).getSubject(),
                db.getAllVideos().get(0).getNote(),
                db.getAllVideos().get(0).getStartCapture(),
                db.getAllVideos().get(0).getEndCapture(),
                db.getAllVideos().get(0).getStartLocation(),
                db.getAllVideos().get(0).getEndLocation(),
                typedFile,
                new Callback<Object>() {
                    @Override
                    public void success(Object o, Response response) {
                        String jsonObjectResponse = new Gson().toJson(o);
                        Log.e("respone", jsonObjectResponse);
                        try {
                            JSONObject jsonObject = new JSONObject(jsonObjectResponse.toString());
                            String status = jsonObject.getString("status");
                            AppController.notification(jsonObject.getString("message"), JobID, false);
                            if (status.equals("true")) {
                                Dialog.Builder builder = null;
//                                builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
//
//                                    @Override
//                                    public void onPositiveActionClicked(DialogFragment fragment) {
                                db.removeVideo();
                                novideo.setVisibility(novideo.VISIBLE);
                                withvideo.setVisibility(withvideo.GONE);
                                selectgalery.setClickable(false);
                                selectgalery.setAlpha((float) 0.2);
                                ismaylaman = false;
                                videoba = false;
                                deleteimageData(fileloc);
//                                        super.onPositiveActionClicked(fragment);
//                                    }
//
//                                    @Override
//                                    public void onNegativeActionClicked(DialogFragment fragment) {
//                                        super.onNegativeActionClicked(fragment);
//                                    }
//                                };
//                                ((SimpleDialog.Builder) builder).message("Video has been successfully uploaded!")
//                                        .title("Video")
//                                        .positiveAction("OK");
//                                DialogFragment fragment = DialogFragment.newInstance(builder);
//                                fragment.setCancelable(false);
//                                fragment.show(getFragmentManager(), null);
                            } else {
                                retryit();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            retryit();
                            AppController.notification("Error while uploading video.", JobID, false);
                        }
                    }

                    @Override
                    public void failure(RetrofitError e) {
                        Log.e("ERROR", e.toString());
                        AppController.notification("Error while uploading video.", JobID, false);
                        retryit();
                    }
                });
    }

    public void deleteimageData(String url) {
        Log.e(TAG, "deleteimageData: " + url);
        String[] separated = url.split("/");
        String finame = separated[separated.length - 1];

        File file = new File(url.replace(finame, ""), finame);
        boolean deleted = file.delete();
        if (deleted) {
            Log.e(TAG, "Deleted: ");
        } else {
            Log.e(TAG, "Error while deleting: ");
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void notificatiodn(String message, String jobid, boolean isStart) {

        if (isStart) {
            Notification n;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                n = new Notification.Builder(getActivity())
                        .setContentTitle("eDocket Video")
                        .setContentText("Video is now uploading..")
                        .setSmallIcon(R.drawable.ic_stat_ic_launcher)
                        .setColor(getActivity().getResources().getColor(R.color.colorPrimary))
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setContentIntent(pIntent)
                        .setProgress(100, 0, true)
                        .setAutoCancel(true).build();

            } else {
                n = new Notification.Builder(getActivity())
                        .setContentTitle("eDocket Video")
                        .setContentText("Video is now uploading..")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setContentIntent(pIntent)
                        .setProgress(100, 0, true)
                        .setAutoCancel(true).build();
            }

            NotificationManager notificationManager =
                    (NotificationManager) getActivity().getSystemService(NOTIFICATION_SERVICE);

            notificationManager.notify(0, n);
        } else {
// build notification
// the addAction re-use the same intent to keep the example short
            Notification n;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                n = new Notification.Builder(getActivity())
                        .setContentTitle("Job #" + jobid + " Video")
                        .setContentText(message)
                        .setSmallIcon(R.drawable.ic_stat_ic_launcher)
                        .setColor(getActivity().getResources().getColor(R.color.colorPrimary))
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setContentIntent(pIntent)
                        .setAutoCancel(true).build();
            } else {
                n = new Notification.Builder(getActivity())
                        .setContentTitle("Job #" + jobid + " Video")
                        .setContentText(message)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setContentIntent(pIntent)
                        .setAutoCancel(true).build();
            }
            NotificationManager notificationManager =
                    (NotificationManager) getActivity().getSystemService(NOTIFICATION_SERVICE);

            notificationManager.notify(0, n);
        }

    }

    public void retryit() {

        Dialog.Builder builder = null;
        builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                dialog4.hide();
                uploadVideo(jobidselected);
                super.onPositiveActionClicked(fragment);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
            }
        };

        ((SimpleDialog.Builder) builder).message("Please try again.")
                .title("Error while uploading")
                .positiveAction("RETRY")
                .negativeAction("CANCEL");
        DialogFragment fragment = DialogFragment.newInstance(builder);

        fragment.setCancelable(false);
        fragment.show(getFragmentManager(), null);

        dialog4.hide();
    }

    private String getGPS() {
        String location = "";
        LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = lm.getProviders(true);
        /* Loop over the array backwards, and if you get an accurate location, then break out the loop*/
        Location l = null;

        for (int i = providers.size() - 1; i >= 0; i--) {
            l = lm.getLastKnownLocation(providers.get(i));
            if (l != null) break;
        }

        double[] gps = new double[2];
        if (l != null) {
            gps[0] = l.getLatitude();
            gps[1] = l.getLongitude();
            location = l.getLatitude() + ", " + l.getLongitude();
        } else {
            location = "-27.4996872,153.0354553";
        }
        return location;
    }


}

