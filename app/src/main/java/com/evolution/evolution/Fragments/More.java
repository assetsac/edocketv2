package com.evolution.evolution.Fragments;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.evolution.evolution.Activities.Help;
import com.evolution.evolution.Activities.Profile;
import com.evolution.evolution.Activities.Requirements;
import com.evolution.evolution.Adapters.AdapterMoreMenu;
import com.evolution.evolution.Helpers.RecyclerItemClickListener;
import com.evolution.evolution.Helpers.SessionManager;
import com.evolution.evolution.R;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by mcclynreyarboleda on 4/13/15.
 */
public class More extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private String token;
    public SessionManager session;

    String[] myDataset = {"Profile", "Settings", "Help", "Logout"};

    int[] icons = {R.mipmap.more3, R.mipmap.more4, R.mipmap.more2, R.mipmap.more5};

    public More() {

    }

    public static More newInstance(int sectionNumber) {
        More fragment = new More();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        session = new SessionManager(getActivity());
        token = session.getToken();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.menu);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Dialog.Builder builder = null;
                        switch (position) {
                            case 0:
                                startActivity(new Intent(getActivity(), Profile.class));
                                break;
                            case 1:
                                Toast.makeText(getActivity(), "This module is not working", Toast.LENGTH_SHORT).show();
                                break;
                            case 2:
                                startActivity(new Intent(getActivity(), Help.class));
                                break;
                            case 3:
                                builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                                    @Override
                                    public void onPositiveActionClicked(DialogFragment fragment) {
                                        session.logout();
                                        super.onPositiveActionClicked(fragment);
                                    }

                                    @Override
                                    public void onNegativeActionClicked(DialogFragment fragment) {
                                        super.onNegativeActionClicked(fragment);
                                    }
                                };

                                ((SimpleDialog.Builder) builder).message("Log out now?")
                                        .title("Log Out")
                                        .positiveAction("LOG OUT")
                                        .negativeAction("CANCEL");
                                DialogFragment fragment = DialogFragment.newInstance(builder);
                                fragment.show(getFragmentManager(), null);
                                break;
                        }
                    }
                })
        );

        mAdapter = new AdapterMoreMenu(myDataset, icons, getActivity());
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }
}
