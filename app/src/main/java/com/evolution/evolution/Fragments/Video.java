package com.evolution.evolution.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.evolution.evolution.Adapters.VideoAdapter;
import com.evolution.evolution.Helpers.DatabaseHelper;
import com.evolution.evolution.Models.VideoModel;
import com.evolution.evolution.R;
import com.evolution.evolution.Activities.VideoViewActivity;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by mcclynreyarboleda on 4/13/15.
 */
public class Video extends Fragment implements View.OnClickListener {

    LinearLayout takeavideo, selectgalery;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView mRecyclerView;
    private VideoAdapter mAdapter;
    ArrayList<VideoModel> item = new ArrayList<>();
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE2 = 200;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private static final String TAG = "Video";
    DatabaseHelper db;
    private Uri fileUri; // file url to store image/video


    private static final String ARG_SECTION_NUMBER = "section_number";

    public Video() {

    }

    public static Video newInstance(int sectionNumber) {
        Video fragment = new Video();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DatabaseHelper(getActivity());
        if(savedInstanceState!=null) {
            fileUri = Uri.parse(savedInstanceState.getString("file_uri"));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.video);
        mLayoutManager = new GridLayoutManager(getActivity(), 3);
        mRecyclerView.setLayoutManager(mLayoutManager);

        takeavideo = (LinearLayout) view.findViewById(R.id.takeapicture);
        selectgalery = (LinearLayout) view.findViewById(R.id.selectgalery);
        takeavideo.setOnClickListener(this);
        selectgalery.setOnClickListener(this);


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();

        // Checking camera availability
        if (!isDeviceSupportCamera()) {
            Toast.makeText(getActivity(),
                    "Sorry! Your device doesn't support camera",
                    Toast.LENGTH_LONG).show();
            // will close the app if the device does't have camera
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter = new VideoAdapter(db.getAllVideos(), getActivity());
        mRecyclerView.setAdapter(mAdapter);
    }

    private boolean isDeviceSupportCamera() {
        if (getActivity().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.takeapicture:
                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);
                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);

                break;
            case R.id.selectgalery:
                Intent selectGallery = new Intent();
                selectGallery.setType("video/*");
                selectGallery.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(selectGallery, "Select Video"), CAMERA_CAPTURE_VIDEO_REQUEST_CODE2);
                break;
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }


    private void launchUploadActivity() {
        Intent i = new Intent(getActivity(), VideoViewActivity.class);
        i.putExtra("filePath", fileUri.getPath());
        startActivity(i);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
        outState.putString("file_uri", fileUri +"");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
//                launchUploadActivity();
                if (!fileUri.getPath().isEmpty()) {
                    VideoModel videotest = new VideoModel();
                    String files = fileUri.getPath() + "";
                    Log.e("file loc", files);
                    videotest.setVideo_url(files);
                    videotest.setGeotag("14.584618333333333, 121.07596333333335");
                    item.add(videotest);
                    db.addVideo(videotest);
                    mAdapter = new VideoAdapter(db.getAllVideos(), getActivity());
                    mRecyclerView.setAdapter(mAdapter);
                    Toast.makeText(getActivity(),
                            "Video Added", Toast.LENGTH_SHORT)
                            .show();
                }

            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(),
                        "Cancelled", Toast.LENGTH_SHORT)
                        .show();
            } else {
            }
        }else  if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            Uri selectedImageUri = data.getData();
            if (!fileUri.getPath().isEmpty()) {
                VideoModel videotest = new VideoModel();
                String files = fileUri.getPath() + "";
                Log.e("file loc", files);
                videotest.setVideo_url(files);
                videotest.setGeotag("14.584618333333333, 121.07596333333335");
                item.add(videotest);
                db.addVideo(videotest);
                mAdapter = new VideoAdapter(db.getAllVideos(), getActivity());
                mRecyclerView.setAdapter(mAdapter);
                Toast.makeText(getActivity(),
                        "Video Added", Toast.LENGTH_SHORT)
                        .show();
            }

        }
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "AndroidFileUpload");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + "AndroidFileUpload" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }
        return mediaFile;
    }

    public void loadData() {
        mAdapter = new VideoAdapter(item, getActivity());
        mRecyclerView.setAdapter(mAdapter);
    }
}
