package com.evolution.evolution.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evolution.evolution.R;

/**
 * Created by mcclynreyarboleda on 4/13/15.
 */
public class Fleet extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    public Fleet() {

    }

    public static Fleet newInstance(int sectionNumber) {
        Fleet fragment = new Fleet();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fleet, container, false);
        return view;
    }
}
