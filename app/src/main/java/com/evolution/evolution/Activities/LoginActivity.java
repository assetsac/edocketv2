package com.evolution.evolution.Activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.evolution.evolution.Helpers.AppController;
import com.evolution.evolution.Helpers.ConnectionDetector;
import com.evolution.evolution.Helpers.Constants;
import com.evolution.evolution.Helpers.DatabaseHelper;
import com.evolution.evolution.Helpers.FormValidation;
import com.evolution.evolution.Helpers.GPSTracker;
import com.evolution.evolution.Helpers.SessionManager;
import com.evolution.evolution.Interface.APIInterface;
import com.evolution.evolution.Models.CheckManstatStatus;
import com.evolution.evolution.R;
import com.google.gson.Gson;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG = LoginActivity.class.getSimpleName();
    private SessionManager session;
    private ConnectionDetector connection;

    Button login;
    RelativeLayout loginlayout;
    RelativeLayout loginloading;
    EditText password;
    EditText username;
    FormValidation validate;
    DatabaseHelper db;
    LinearLayout retriveold;

    // GPSTracker class
    GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        retriveold = (LinearLayout) findViewById(R.id.retriveold);

        retriveold.setOnClickListener(this);

        login = (Button) findViewById(R.id.login);
        loginlayout = (RelativeLayout) findViewById(R.id.loginlayout);
        loginloading = (RelativeLayout) findViewById(R.id.loginloading);

        login.setOnClickListener(this);
        db = new DatabaseHelper(this);
        validate = new FormValidation(this);
        session = new SessionManager(this);
        connection = new ConnectionDetector(this);

        gps = new GPSTracker(this);
        if (!gps.canGetLocation()) {
            com.rey.material.app.Dialog.Builder builder = null;
            builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {

                @Override
                public void onPositiveActionClicked(DialogFragment fragment) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                    super.onPositiveActionClicked(fragment);
                }

                @Override
                public void onNegativeActionClicked(DialogFragment fragment) {
                    super.onNegativeActionClicked(fragment);
                }
            };

            ((SimpleDialog.Builder) builder).message("Please turn on location service to get your current location.")
                    .title("Location")
                    .negativeAction("Cancel")
                    .positiveAction("Ok");

            DialogFragment fragment = DialogFragment.newInstance(builder);
            fragment.show(getSupportFragmentManager(), null);
        }
        if (!connection.isConnectedToInternet()) {
            if (session.getToken() != "") {
                Intent noInterRedirectihav = new Intent(LoginActivity.this, MainPage.class);
                noInterRedirectihav.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(noInterRedirectihav);
                finish();
            } else {
                showLoading(true);
                Toast.makeText(getApplication(), "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        } else {
            loginloading.setVisibility(loginloading.GONE);
            loginlayout.setVisibility(loginloading.GONE);
            initSession();
        }
    }

    public void initSession() {
        if (session.isLoggedIn()) {
            loginMethod(session.getUsername(), session.getPassword());
        } else {
            loginlayout.setVisibility(loginloading.VISIBLE);
        }
    }

    public void checkManstat() {
        if (connection.isConnectedToInternet()) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Constants.API_URL)
                    .build();
            APIInterface interfacem = restAdapter.create(APIInterface.class);
            interfacem.getCheckManstat(session.getToken(), Constants.REQUEST_JOBINFO, new Callback<CheckManstatStatus>() {
                @Override
                public void success(CheckManstatStatus mans, Response response) {
                    String jsonObjectResponse = new Gson().toJson(mans);
                    Log.e("login r", jsonObjectResponse);
                    if (mans.getStatus().equals("false")) {
                        Toast.makeText(LoginActivity.this, "Failed to connect to ManStat. Please try again.", Toast.LENGTH_SHORT).show();
                    } else {

                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Response r = error.getResponse();
                    switch (error.getKind()) {
                        case NETWORK:
                            Log.e("error", "error while connectiong to NETWORK");
                            break;
                        case UNEXPECTED:
                            Log.e("error", "error while connectiong to server");
                            throw error;
                        case HTTP:
                            switch (r.getStatus()) {
                                case 401:

                                    break;
                                case 404:

                                    break;
                                case 422:
                                    Toast.makeText(LoginActivity.this, "Failed to connect to ManStat. Please try again", Toast.LENGTH_SHORT).show();
                                    break;
                                case 500:

                                    break;
                            }
                            Log.e("error ", "error while connectiong to HTTP" + r.getStatus());
                            break;
                        default:
                            Log.e("error ", "error while connectiong to HTTP" + r.getStatus());
                            break;
                    }
                }
            });
        } else {

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:

//                File pdfFile = new File("res/raw/file.pdf");
//                Uri path = Uri.fromFile(pdfFile);
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//                intent.setDataAndType(path, "application/pdf");
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);

                if (validate.validateUsername(username) && validate.validatePassword(password)) {
                    showLoading(false);
                    loginMethod(username.getText().toString(), password.getText().toString());
                }
                break;
            case R.id.retriveold:
                if (!session.getRecentID().equals("")) {
                    username.setText(session.getRecentID());
                }
                break;
        }
    }

    public void loginMethod(final String usernameL, final String passwordL) {
        Map<String, String> params = new HashMap<>();

        params.put("etccode", usernameL);
        params.put("password", passwordL);

        AppController.getInstance().getBoardsAPI().getLogin(Constants.REQUEST_JOBINFO, params, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                String jsonObjectResponse = new Gson().toJson(o);
                Log.d(TAG, jsonObjectResponse.toString());
                Log.e(TAG + " Return Login: ", response.getStatus() + "");
                Log.e(TAG + " Return Login: ", jsonObjectResponse.toString());
                try {
                    JSONObject jsonObject = new JSONObject(jsonObjectResponse.toString());
                    if (jsonObject.has(Constants.KEY_TOKEN)) {
                        String status = jsonObject.getString("status");
                        String token = jsonObject.getString(Constants.KEY_TOKEN);
                        HashMap<String, String> data = new HashMap<String, String>();
                        data.put(Constants.KEY_USERNAME, usernameL.toString());
                        data.put(Constants.KEY_PASSWORD, passwordL.toString());
                        data.put(Constants.KEY_TOKEN, token);
                        session.createLoginSession(data);
                        if (usernameL.toString().equals(session.getRecentID())) {
                            mainPage();
                            showLoading(true);
                        } else {
                            db.deleteifSuccess2();
                            mainPage();
                            showLoading(true);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Please check your details.", Toast.LENGTH_SHORT).show();
                        showLoading(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("Error", "Error while connecting to server" + e.toString());
                    showLoading(true);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(LoginActivity.class.getSimpleName(), error.toString());

                Response r = error.getResponse();
                switch (error.getKind()) {
                    case NETWORK:
                        Toast.makeText(getApplicationContext(), "Error while connecting to server", Toast.LENGTH_SHORT).show();
                        Log.e("error", "error while connectiong to NETWORK");
                        break;
                    case UNEXPECTED:
                        Log.e("error", "error while connectiong to server");
                        throw error;
                    case HTTP:
                        switch (r.getStatus()) {
                            case 401:

                                break;
                            case 404:

                                break;
                            case 422:
                                Toast.makeText(LoginActivity.this, "User account doesn't exist.", Toast.LENGTH_SHORT).show();
                                username.setText(session.getUsername());
                                password.setText(session.getPassword());
                                break;
                            case 500:
                                Toast.makeText(LoginActivity.this, "Failed to connect to ManStat. Please try again.", Toast.LENGTH_SHORT).show();
                                break;
                            case 503:
                                Toast.makeText(LoginActivity.this, "Failed to connect to ManStat. Please try again.", Toast.LENGTH_SHORT).show();
                                break;
                        }
                        Log.e("error ", "error while connectiong to HTTP" + r.getStatus());
                        break;
                    default:
                        Log.e("error ", "error while connectiong to HTTP" + r.getStatus());
                        break;
                }
                showLoading(true);
            }
        });
    }

    public void HideKeyboard(View view) {
        hideKeyboard();
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void mainPage() {
        Intent home = new Intent(LoginActivity.this, MainPage.class);
        home.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(home);
        finish();
        overridePendingTransition(0, 0);
    }

    public boolean showLoading(boolean flag) {
        if (flag) {
            loginloading.setVisibility(loginloading.GONE);
            loginlayout.setVisibility(loginloading.VISIBLE);
            flag = true;
        } else {
            loginloading.setVisibility(loginloading.VISIBLE);
            loginlayout.setVisibility(loginloading.GONE);
            flag = false;
        }
        return flag;
    }
}


//[19/01/2017, 11:53:42 AM] Colin Ambrosio: http://testevoapp.manstat.com:9997/EDocket/public/docs/v2/forms#generate-pre-start-checklist-report
//        [19/01/2017, 11:54:07 AM] Colin Ambrosio: http://testevoapp.manstat.com:9997/EDocket/public/api/development/job/generate_pre_start_checklist_report