package com.evolution.evolution.Activities.signs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.evolution.evolution.Helpers.DatabaseHelper;
import com.evolution.evolution.Helpers.DateHelper;
import com.evolution.evolution.Models.Job;
import com.evolution.evolution.R;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class AfterCareQLD extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    RadioButton slowlane, fastlane;
    LinearLayout slowlanel, fastlanel;
    DatabaseHelper db;

    Job mJobObject;
    EditText qld1, qld2, qld3, qld4, qld5, qld6, qld7, qld8, qld9, qld10, qld11, qld12, qld13, qld14, qld15, qld16, qld17, qld18, qld19, qld20, qld21, qld22, qld23, qld24, qld25, qld26, qld27, qld28, qld29, qld30, qld31, qld32, qld33, qld34, qld35, qld36, qld37, qld38, qld39, qld40, qld41, qld42, qld43, qld44, qld45, qld46, qld47, qld48, qld49, qld50, qld51, qld52, qld53, qld54, qld55, qld56, qld57, qld58, qld59, qld60, qld61, qld62, qld63, qld64, qld65, qld66, qld67, qld68, qld69, qld70, qld71, qld72, qld73, qld74, qld75, qld76, qld77;
    EditText qlda1, qlda2, qlda3, qlda4, qlda5, qlda6, qlda7, qlda8, qlda9, qlda10, qlda11, qlda12, qlda13, qlda14, qlda15, qlda16, qlda17, qlda18, qlda19, qlda20, qlda21, qlda22, qlda23, qlda24, qlda25, qlda26, qlda27, qlda28, qlda29, qlda30, qlda31, qlda32, qlda33, qlda34, qlda35, qlda36, qlda37, qlda38, qlda39, qlda40, qlda41, qlda42, qlda43, qlda44, qlda45, qlda46, qlda47, qlda48, qlda49, qlda50, qlda51, qlda52, qlda53, qlda54, qlda55, qlda56, qlda57, qlda58, qlda59, qlda60, qlda61, qlda62, qlda63, qlda64, qlda65, qlda66, qlda67, qlda68, qlda69, qlda70, qlda71, qlda72, qlda73, qlda74, qlda75, qlda76, qlda77;
    List<String> categories;

    DateHelper date;

    MaterialEditText cwevolution4, cwlocallandmark4, TimeErected, TimeCollected, TimeChecked1, TimeChecked2, TimeChecked3, TimeChecked4;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_care_qld);
        initToolBar();
        db = new DatabaseHelper(this);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        }
        date = new DateHelper();

    }

    @Override
    protected void onResume() {
        super.onResume();
        initViews();
        populateData();
        initSpinner();
    }

    public void initSpinner() {
        // Spinner click listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // On selecting a spinner item
                String item = parent.getItemAtPosition(position).toString();

                // Showing selected spinner item
//                Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner.setEnabled(false);

        // Spinner Drop down elements
        categories = new ArrayList<String>();
        categories.add("North");
        categories.add("South");
        categories.add("East");
        categories.add("West");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnerlocation, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.spinnerlocation);

        spinner.setAdapter(dataAdapter);

    }

    public void hideItem(EditText ed, JSONObject c, int position) throws JSONException {
        if (getcontetn(c.getString("AuditSigns"), position).equals("")) {
//            ((ViewGroup) ed.getParent()).setVisibility(((ViewGroup) ed.getParent()).GONE);
            ed.setEnabled(false);
        } else {
            ed.setEnabled(false);
        }
    }

    public void initViews() {
        slowlane = (RadioButton) findViewById(R.id.slowlane);
        fastlane = (RadioButton) findViewById(R.id.fastlane);

        slowlanel = (LinearLayout) findViewById(R.id.slowlanel);
        fastlanel = (LinearLayout) findViewById(R.id.fastlanel);

        slowlane.setOnClickListener(this);
        fastlane.setOnClickListener(this);

        TimeErected = (MaterialEditText) findViewById(R.id.TimeErected);
        TimeCollected = (MaterialEditText) findViewById(R.id.TimeCollected);
        TimeChecked1 = (MaterialEditText) findViewById(R.id.TimeChecked1);
        TimeChecked2 = (MaterialEditText) findViewById(R.id.TimeChecked2);
        TimeChecked3 = (MaterialEditText) findViewById(R.id.TimeChecked3);
        TimeChecked4 = (MaterialEditText) findViewById(R.id.TimeChecked4);

        spinner = (Spinner) findViewById(R.id.cwdirectionlist4);

        cwevolution4 = (MaterialEditText) findViewById(R.id.cwevolution4);
        cwlocallandmark4 = (MaterialEditText) findViewById(R.id.cwlocallandmark4);

        qld1 = (EditText) findViewById(R.id.qld1);

        qld2 = (EditText) findViewById(R.id.qld2);
        qld3 = (EditText) findViewById(R.id.qld3);
        qld4 = (EditText) findViewById(R.id.qld4);
        qld5 = (EditText) findViewById(R.id.qld5);
        qld6 = (EditText) findViewById(R.id.qld6);
        qld7 = (EditText) findViewById(R.id.qld7);
        qld8 = (EditText) findViewById(R.id.qld8);
        qld9 = (EditText) findViewById(R.id.qld9);
        qld10 = (EditText) findViewById(R.id.qld10);
        qld11 = (EditText) findViewById(R.id.qld11);
        qld13 = (EditText) findViewById(R.id.qld13);
        qld14 = (EditText) findViewById(R.id.qld14);
        qld15 = (EditText) findViewById(R.id.qld15);
        qld16 = (EditText) findViewById(R.id.qld16);
        qld17 = (EditText) findViewById(R.id.qld17);
        qld18 = (EditText) findViewById(R.id.qld18);
        qld19 = (EditText) findViewById(R.id.qld19);
        qld20 = (EditText) findViewById(R.id.qld20);
        qld21 = (EditText) findViewById(R.id.qld21);
        qld22 = (EditText) findViewById(R.id.qld22);
        qld23 = (EditText) findViewById(R.id.qld23);
        qld24 = (EditText) findViewById(R.id.qld24);
        qld25 = (EditText) findViewById(R.id.qld25);
        qld26 = (EditText) findViewById(R.id.qld26);
        qld27 = (EditText) findViewById(R.id.qld27);
        qld28 = (EditText) findViewById(R.id.qld28);
        qld29 = (EditText) findViewById(R.id.qld29);
        qld30 = (EditText) findViewById(R.id.qld30);
        qld31 = (EditText) findViewById(R.id.qld31);
        qld32 = (EditText) findViewById(R.id.qld32);
        qld33 = (EditText) findViewById(R.id.qld33);
        qld34 = (EditText) findViewById(R.id.qld34);
        qld35 = (EditText) findViewById(R.id.qld35);
        qld36 = (EditText) findViewById(R.id.qld36);
        qld37 = (EditText) findViewById(R.id.qld37);
        qld38 = (EditText) findViewById(R.id.qld38);
        qld39 = (EditText) findViewById(R.id.qld39);
        qld40 = (EditText) findViewById(R.id.qld40);
        qld41 = (EditText) findViewById(R.id.qld41);
        qld42 = (EditText) findViewById(R.id.qld42);
        qld43 = (EditText) findViewById(R.id.qld43);
        qld44 = (EditText) findViewById(R.id.qld44);
        qld45 = (EditText) findViewById(R.id.qld45);
        qld46 = (EditText) findViewById(R.id.qld46);
        qld47 = (EditText) findViewById(R.id.qld47);
        qld48 = (EditText) findViewById(R.id.qld48);
        qld49 = (EditText) findViewById(R.id.qld49);
        qld50 = (EditText) findViewById(R.id.qld50);
        qld51 = (EditText) findViewById(R.id.qld51);
        qld52 = (EditText) findViewById(R.id.qld52);
        qld53 = (EditText) findViewById(R.id.qld53);
        qld54 = (EditText) findViewById(R.id.qld54);
        qld55 = (EditText) findViewById(R.id.qld55);
        qld56 = (EditText) findViewById(R.id.qld56);
        qld57 = (EditText) findViewById(R.id.qld57);
        qld58 = (EditText) findViewById(R.id.qld58);
        qld59 = (EditText) findViewById(R.id.qld59);
        qld60 = (EditText) findViewById(R.id.qld60);
        qld61 = (EditText) findViewById(R.id.qld61);
        qld62 = (EditText) findViewById(R.id.qld62);
        qld63 = (EditText) findViewById(R.id.qld63);
        qld64 = (EditText) findViewById(R.id.qld64);
        qld65 = (EditText) findViewById(R.id.qld65);
        qld66 = (EditText) findViewById(R.id.qld66);
        qld67 = (EditText) findViewById(R.id.qld67);
        qld68 = (EditText) findViewById(R.id.qld68);
        qld69 = (EditText) findViewById(R.id.qld69);
        qld70 = (EditText) findViewById(R.id.qld70);
        qld71 = (EditText) findViewById(R.id.qld71);
        qld72 = (EditText) findViewById(R.id.qld72);
        qld73 = (EditText) findViewById(R.id.qld73);
        qld74 = (EditText) findViewById(R.id.qld74);
        qld75 = (EditText) findViewById(R.id.qld75);
        qld76 = (EditText) findViewById(R.id.qld76);
        qld77 = (EditText) findViewById(R.id.qld77);

        qlda1 = (EditText) findViewById(R.id.qlda1);
        qlda2 = (EditText) findViewById(R.id.qlda2);
        qlda3 = (EditText) findViewById(R.id.qlda3);
        qlda4 = (EditText) findViewById(R.id.qlda4);
        qlda5 = (EditText) findViewById(R.id.qlda5);
        qlda6 = (EditText) findViewById(R.id.qlda6);
        qlda7 = (EditText) findViewById(R.id.qlda7);
        qlda8 = (EditText) findViewById(R.id.qlda8);
        qlda9 = (EditText) findViewById(R.id.qlda9);
        qlda10 = (EditText) findViewById(R.id.qlda10);
        qlda11 = (EditText) findViewById(R.id.qlda11);
//        qlda12 = (EditText) findViewById(R.id.qlda12);
        qlda13 = (EditText) findViewById(R.id.qlda13);
        qlda14 = (EditText) findViewById(R.id.qlda14);
        qlda15 = (EditText) findViewById(R.id.qlda15);
        qlda16 = (EditText) findViewById(R.id.qlda16);
        qlda17 = (EditText) findViewById(R.id.qlda17);
        qlda18 = (EditText) findViewById(R.id.qlda18);
        qlda19 = (EditText) findViewById(R.id.qlda19);
        qlda20 = (EditText) findViewById(R.id.qlda20);
        qlda21 = (EditText) findViewById(R.id.qlda21);
        qlda22 = (EditText) findViewById(R.id.qlda22);
        qlda23 = (EditText) findViewById(R.id.qlda23);
        qlda24 = (EditText) findViewById(R.id.qlda24);
        qlda25 = (EditText) findViewById(R.id.qlda25);
        qlda26 = (EditText) findViewById(R.id.qlda26);
        qlda27 = (EditText) findViewById(R.id.qlda27);
        qlda28 = (EditText) findViewById(R.id.qlda28);
        qlda29 = (EditText) findViewById(R.id.qlda29);
        qlda30 = (EditText) findViewById(R.id.qlda30);
        qlda31 = (EditText) findViewById(R.id.qlda31);
        qlda32 = (EditText) findViewById(R.id.qlda32);
        qlda33 = (EditText) findViewById(R.id.qlda33);
        qlda34 = (EditText) findViewById(R.id.qlda34);
        qlda35 = (EditText) findViewById(R.id.qlda35);
        qlda36 = (EditText) findViewById(R.id.qlda36);
        qlda37 = (EditText) findViewById(R.id.qlda37);
        qlda38 = (EditText) findViewById(R.id.qlda38);
        qlda39 = (EditText) findViewById(R.id.qlda39);
        qlda40 = (EditText) findViewById(R.id.qlda40);
        qlda41 = (EditText) findViewById(R.id.qlda41);
        qlda42 = (EditText) findViewById(R.id.qlda42);
        qlda43 = (EditText) findViewById(R.id.qlda43);
        qlda44 = (EditText) findViewById(R.id.qlda44);
        qlda45 = (EditText) findViewById(R.id.qlda45);
        qlda46 = (EditText) findViewById(R.id.qlda46);
        qlda47 = (EditText) findViewById(R.id.qlda47);
        qlda48 = (EditText) findViewById(R.id.qlda48);
        qlda49 = (EditText) findViewById(R.id.qlda49);
        qlda50 = (EditText) findViewById(R.id.qlda50);
        qlda51 = (EditText) findViewById(R.id.qlda51);
        qlda52 = (EditText) findViewById(R.id.qlda52);
        qlda53 = (EditText) findViewById(R.id.qlda53);
        qlda54 = (EditText) findViewById(R.id.qlda54);
        qlda55 = (EditText) findViewById(R.id.qlda55);
        qlda56 = (EditText) findViewById(R.id.qlda56);
        qlda57 = (EditText) findViewById(R.id.qlda57);
        qlda58 = (EditText) findViewById(R.id.qlda58);
        qlda59 = (EditText) findViewById(R.id.qlda59);
        qlda60 = (EditText) findViewById(R.id.qlda60);
        qlda61 = (EditText) findViewById(R.id.qlda61);
        qlda62 = (EditText) findViewById(R.id.qlda62);
        qlda63 = (EditText) findViewById(R.id.qlda63);
        qlda64 = (EditText) findViewById(R.id.qlda64);
        qlda65 = (EditText) findViewById(R.id.qlda65);
        qlda66 = (EditText) findViewById(R.id.qlda66);
        qlda67 = (EditText) findViewById(R.id.qlda67);
        qlda68 = (EditText) findViewById(R.id.qlda68);
        qlda69 = (EditText) findViewById(R.id.qlda69);
        qlda70 = (EditText) findViewById(R.id.qlda70);
        qlda71 = (EditText) findViewById(R.id.qlda71);
        qlda72 = (EditText) findViewById(R.id.qlda72);
        qlda73 = (EditText) findViewById(R.id.qlda73);
        qlda74 = (EditText) findViewById(R.id.qlda74);
        qlda75 = (EditText) findViewById(R.id.qlda75);
        qlda76 = (EditText) findViewById(R.id.qlda76);
        qlda77 = (EditText) findViewById(R.id.qlda77);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.slowlane:
                slowlanel.setVisibility(slowlanel.VISIBLE);
                fastlanel.setVisibility(slowlanel.GONE);
                break;
            case R.id.fastlane:
                slowlanel.setVisibility(slowlanel.GONE);
                fastlanel.setVisibility(slowlanel.VISIBLE);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_after_care_qld, menu);
        return true;
    }


    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("After Care QLD");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.saveaftercare:
                checksaveCheckboxes();
                break;
            case android.R.id.home:
                finish();
                overridePendingTransition(0, 0);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void checksaveCheckboxes() {

        String returnsslow = "";
        String returnsfast = "";
        LinearLayout layout = (LinearLayout) findViewById(R.id.slowlanel);
        for (int dfd = 0; dfd < layout.getChildCount(); dfd++) {
            LinearLayout la = (LinearLayout) layout.getChildAt(dfd);
            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);
                if (v instanceof CheckBox) {
                    CheckBox b = (CheckBox) v;
                    if (b.isChecked() == true) {
                        returnsslow = returnsslow + "1-";
                    } else {
                        returnsslow = returnsslow + "0-";
                    }
                }


            }
        }

        LinearLayout layout2 = (LinearLayout) findViewById(R.id.fastlanel);

        for (int fa = 0; fa < layout2.getChildCount(); fa++) {
            LinearLayout la = (LinearLayout) layout2.getChildAt(fa);
            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);
                if (v instanceof CheckBox) {
                    CheckBox b = (CheckBox) v;
                    if (b.isChecked() == true) {
                        returnsfast = returnsfast + "1-";
                    } else {
                        returnsfast = returnsfast + "0-";
                    }
                }
            }
        }

        Log.e("before save", returnsslow + "~" + returnsfast);

        db.updateAfterCare(mJobObject.getId(), returnsslow + "~" + returnsfast);
    }

    public String getSignageAudit() {
        String temp = "";
        temp = "{\"mac\":" + db.getSignageAudt(mJobObject.getJobID()) + "}";
        return temp;
    }

    public void populateData() {
        JSONArray data = null;
        Log.e("haha", getSignageAudit());

        if (getSignageAudit().equals("{\"mac\":null}")) {

        } else {
            try {
                JSONObject jsonObj = new JSONObject(getSignageAudit());

                data = jsonObj.getJSONArray("mac");

                JSONObject c = data.getJSONObject(0);
                JSONObject x = data.getJSONObject(1);

                cwevolution4.setText(c.getString("RegoNo"));
                cwlocallandmark4.setText(c.getString("Landmark"));

                spinner.setSelection(Integer.parseInt(c.getString("Direction")), true);

                if (!c.getString("TimeErected").equals("null")) {
                    TimeErected.setText(date.miliSecondConverter(c.getString("TimeErected"), "HH:mm:ss a"));
                }
                if (!c.getString("TimeCollected").equals("null")) {
                    TimeCollected.setText(date.miliSecondConverter(c.getString("TimeCollected"), "HH:mm:ss a"));
                }
                if (!c.getString("TimeChecked1").equals("null")) {
                    TimeChecked1.setText(date.miliSecondConverter(c.getString("TimeChecked1"), "HH:mm:ss a"));
                }
                if (!c.getString("TimeChecked2").equals("null")) {
                    TimeChecked2.setText(date.miliSecondConverter(c.getString("TimeChecked2"), "HH:mm:ss a"));
                }
                if (!c.getString("TimeChecked3").equals("null")) {
                    TimeChecked3.setText(date.miliSecondConverter(c.getString("TimeChecked3"), "HH:mm:ss a"));
                }
                if (!c.getString("TimeChecked4").equals("null")) {
                    TimeChecked4.setText(date.miliSecondConverter(c.getString("TimeChecked4"), "HH:mm:ss a"));
                }

                qld1.setText(getcontetn(c.getString("AuditSigns"), 0));
                qld2.setText(getcontetn(c.getString("AuditSigns"), 1));
                qld3.setText(getcontetn(c.getString("AuditSigns"), 2));
                qld4.setText(getcontetn(c.getString("AuditSigns"), 3));
                qld5.setText(getcontetn(c.getString("AuditSigns"), 4));
                qld6.setText(getcontetn(c.getString("AuditSigns"), 5));
                qld7.setText(getcontetn(c.getString("AuditSigns"), 6));
                qld8.setText(getcontetn(c.getString("AuditSigns"), 7));
                qld9.setText(getcontetn(c.getString("AuditSigns"), 8));
                qld10.setText(getcontetn(c.getString("AuditSigns"), 9));
                qld11.setText(getcontetn(c.getString("AuditSigns"), 10));
                qld13.setText(getcontetn(c.getString("AuditSigns"), 11));
                qld14.setText(getcontetn(c.getString("AuditSigns"), 12));
                qld15.setText(getcontetn(c.getString("AuditSigns"), 13));
                qld16.setText(getcontetn(c.getString("AuditSigns"), 14));
                qld17.setText(getcontetn(c.getString("AuditSigns"), 15));
                qld18.setText(getcontetn(c.getString("AuditSigns"), 16));
                qld19.setText(getcontetn(c.getString("AuditSigns"), 17));
                qld20.setText(getcontetn(c.getString("AuditSigns"), 18));
                qld21.setText(getcontetn(c.getString("AuditSigns"), 19));
                qld22.setText(getcontetn(c.getString("AuditSigns"), 20));
                qld23.setText(getcontetn(c.getString("AuditSigns"), 21));
                qld24.setText(getcontetn(c.getString("AuditSigns"), 22));
                qld25.setText(getcontetn(c.getString("AuditSigns"), 23));
                qld26.setText(getcontetn(c.getString("AuditSigns"), 24));
                qld27.setText(getcontetn(c.getString("AuditSigns"), 25));
                qld28.setText(getcontetn(c.getString("AuditSigns"), 26));
                qld29.setText(getcontetn(c.getString("AuditSigns"), 27));
                qld30.setText(getcontetn(c.getString("AuditSigns"), 28));
                qld31.setText(getcontetn(c.getString("AuditSigns"), 29));
                qld32.setText(getcontetn(c.getString("AuditSigns"), 30));
                qld33.setText(getcontetn(c.getString("AuditSigns"), 31));
                qld34.setText(getcontetn(c.getString("AuditSigns"), 32));
                qld35.setText(getcontetn(c.getString("AuditSigns"), 33));
                qld36.setText(getcontetn(c.getString("AuditSigns"), 34));
                qld37.setText(getcontetn(c.getString("AuditSigns"), 35));
                qld38.setText(getcontetn(c.getString("AuditSigns"), 36));
                qld39.setText(getcontetn(c.getString("AuditSigns"), 37));
                qld40.setText(getcontetn(c.getString("AuditSigns"), 38));
                qld41.setText(getcontetn(c.getString("AuditSigns"), 39));
                qld42.setText(getcontetn(c.getString("AuditSigns"), 40));
                qld43.setText(getcontetn(c.getString("AuditSigns"), 41));
                qld44.setText(getcontetn(c.getString("AuditSigns"), 42));
                qld45.setText(getcontetn(c.getString("AuditSigns"), 43));
                qld46.setText(getcontetn(c.getString("AuditSigns"), 44));
                qld47.setText(getcontetn(c.getString("AuditSigns"), 45));
                qld48.setText(getcontetn(c.getString("AuditSigns"), 46));
                qld49.setText(getcontetn(c.getString("AuditSigns"), 47));
                qld50.setText(getcontetn(c.getString("AuditSigns"), 48));
                qld51.setText(getcontetn(c.getString("AuditSigns"), 49));
                qld52.setText(getcontetn(c.getString("AuditSigns"), 50));
                qld53.setText(getcontetn(c.getString("AuditSigns"), 51));
                qld54.setText(getcontetn(c.getString("AuditSigns"), 52));
                qld55.setText(getcontetn(c.getString("AuditSigns"), 53));
                qld56.setText(getcontetn(c.getString("AuditSigns"), 54));
                qld57.setText(getcontetn(c.getString("AuditSigns"), 55));
                qld58.setText(getcontetn(c.getString("AuditSigns"), 55));
                qld59.setText(getcontetn(c.getString("AuditSigns"), 57));
                qld60.setText(getcontetn(c.getString("AuditSigns"), 58));
                qld61.setText(getcontetn(c.getString("AuditSigns"), 59));
                qld62.setText(getcontetn(c.getString("AuditSigns"), 60));
                qld63.setText(getcontetn(c.getString("AuditSigns"), 61));
                qld64.setText(getcontetn(c.getString("AuditSigns"), 62));
                qld65.setText(getcontetn(c.getString("AuditSigns"), 63));
                qld66.setText(getcontetn(c.getString("AuditSigns"), 64));
                qld67.setText(getcontetn(c.getString("AuditSigns"), 65));
                qld68.setText(getcontetn(c.getString("AuditSigns"), 66));
                qld69.setText(getcontetn(c.getString("AuditSigns"), 67));
                qld70.setText(getcontetn(c.getString("AuditSigns"), 68));
                qld71.setText(getcontetn(c.getString("AuditSigns"), 69));
                qld72.setText(getcontetn(c.getString("AuditSigns"), 70));
                qld73.setText(getcontetn(c.getString("AuditSigns"), 71));
                qld74.setText(getcontetn(c.getString("AuditSigns"), 72));
                qld75.setText(getcontetn(c.getString("AuditSigns"), 73));
                qld76.setText(getcontetn(c.getString("AuditSigns"), 74));
                qld77.setText(getcontetn(c.getString("AuditSigns"), 75));

                qlda1.setText(getcontetn(x.getString("AuditSigns"), 0));
                qlda2.setText(getcontetn(x.getString("AuditSigns"), 1));
                qlda3.setText(getcontetn(x.getString("AuditSigns"), 2));
                qlda4.setText(getcontetn(x.getString("AuditSigns"), 3));
                qlda5.setText(getcontetn(x.getString("AuditSigns"), 4));
                qlda6.setText(getcontetn(x.getString("AuditSigns"), 5));
                qlda7.setText(getcontetn(x.getString("AuditSigns"), 6));
                qlda8.setText(getcontetn(x.getString("AuditSigns"), 7));
                qlda9.setText(getcontetn(x.getString("AuditSigns"), 8));
                qlda10.setText(getcontetn(x.getString("AuditSigns"), 9));
                qlda11.setText(getcontetn(x.getString("AuditSigns"), 10));
                qlda13.setText(getcontetn(x.getString("AuditSigns"), 11));
                qlda14.setText(getcontetn(x.getString("AuditSigns"), 12));
                qlda15.setText(getcontetn(x.getString("AuditSigns"), 13));
                qlda16.setText(getcontetn(x.getString("AuditSigns"), 14));
                qlda17.setText(getcontetn(x.getString("AuditSigns"), 15));
                qlda18.setText(getcontetn(x.getString("AuditSigns"), 16));
                qlda19.setText(getcontetn(x.getString("AuditSigns"), 17));
                qlda20.setText(getcontetn(x.getString("AuditSigns"), 18));
                qlda21.setText(getcontetn(x.getString("AuditSigns"), 19));
                qlda22.setText(getcontetn(x.getString("AuditSigns"), 20));
                qlda23.setText(getcontetn(x.getString("AuditSigns"), 21));
                qlda24.setText(getcontetn(x.getString("AuditSigns"), 22));
                qlda25.setText(getcontetn(x.getString("AuditSigns"), 23));
                qlda26.setText(getcontetn(x.getString("AuditSigns"), 24));
                qlda27.setText(getcontetn(x.getString("AuditSigns"), 25));
                qlda28.setText(getcontetn(x.getString("AuditSigns"), 26));
                qlda29.setText(getcontetn(x.getString("AuditSigns"), 27));
                qlda30.setText(getcontetn(x.getString("AuditSigns"), 28));
                qlda31.setText(getcontetn(x.getString("AuditSigns"), 29));
                qlda32.setText(getcontetn(x.getString("AuditSigns"), 30));
                qlda33.setText(getcontetn(x.getString("AuditSigns"), 31));
                qlda34.setText(getcontetn(x.getString("AuditSigns"), 32));
                qlda35.setText(getcontetn(x.getString("AuditSigns"), 33));
                qlda36.setText(getcontetn(x.getString("AuditSigns"), 34));
                qlda37.setText(getcontetn(x.getString("AuditSigns"), 35));
                qlda38.setText(getcontetn(x.getString("AuditSigns"), 36));
                qlda39.setText(getcontetn(x.getString("AuditSigns"), 37));
                qlda40.setText(getcontetn(x.getString("AuditSigns"), 38));
                qlda41.setText(getcontetn(x.getString("AuditSigns"), 39));
                qlda42.setText(getcontetn(x.getString("AuditSigns"), 40));
                qlda43.setText(getcontetn(x.getString("AuditSigns"), 41));
                qlda44.setText(getcontetn(x.getString("AuditSigns"), 42));
                qlda45.setText(getcontetn(x.getString("AuditSigns"), 43));
                qlda46.setText(getcontetn(x.getString("AuditSigns"), 44));
                qlda47.setText(getcontetn(x.getString("AuditSigns"), 45));
                qlda48.setText(getcontetn(x.getString("AuditSigns"), 46));
                qlda49.setText(getcontetn(x.getString("AuditSigns"), 47));
                qlda50.setText(getcontetn(x.getString("AuditSigns"), 48));
                qlda51.setText(getcontetn(x.getString("AuditSigns"), 49));
                qlda52.setText(getcontetn(x.getString("AuditSigns"), 50));
                qlda53.setText(getcontetn(x.getString("AuditSigns"), 51));
                qlda54.setText(getcontetn(x.getString("AuditSigns"), 52));
                qlda55.setText(getcontetn(x.getString("AuditSigns"), 53));
                qlda56.setText(getcontetn(x.getString("AuditSigns"), 54));
                qlda57.setText(getcontetn(x.getString("AuditSigns"), 55));
                qlda58.setText(getcontetn(x.getString("AuditSigns"), 55));
                qlda59.setText(getcontetn(x.getString("AuditSigns"), 57));
                qlda60.setText(getcontetn(x.getString("AuditSigns"), 58));
                qlda61.setText(getcontetn(x.getString("AuditSigns"), 59));
                qlda62.setText(getcontetn(x.getString("AuditSigns"), 60));
                qlda63.setText(getcontetn(x.getString("AuditSigns"), 61));
                qlda64.setText(getcontetn(x.getString("AuditSigns"), 62));
                qlda65.setText(getcontetn(x.getString("AuditSigns"), 63));
                qlda66.setText(getcontetn(x.getString("AuditSigns"), 64));
                qlda67.setText(getcontetn(x.getString("AuditSigns"), 65));
                qlda68.setText(getcontetn(x.getString("AuditSigns"), 66));
                qlda69.setText(getcontetn(x.getString("AuditSigns"), 67));
                qlda70.setText(getcontetn(x.getString("AuditSigns"), 68));
                qlda71.setText(getcontetn(x.getString("AuditSigns"), 69));
                qlda72.setText(getcontetn(x.getString("AuditSigns"), 70));
                qlda73.setText(getcontetn(x.getString("AuditSigns"), 71));
                qlda74.setText(getcontetn(x.getString("AuditSigns"), 72));
                qlda75.setText(getcontetn(x.getString("AuditSigns"), 73));
                qlda76.setText(getcontetn(x.getString("AuditSigns"), 74));
                qlda77.setText(getcontetn(x.getString("AuditSigns"), 75));


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (!db.getaftercare(mJobObject.getJobID()).equals("null")) {
            String[] slowfast = db.getaftercare(mJobObject.getJobID()).split("~");


            Log.e("slow", slowfast[0]);
            String[] aftercare = slowfast[0].split("-");
            LinearLayout layout = (LinearLayout) findViewById(R.id.slowlanel);

            for (int d = 0; d < layout.getChildCount(); d++) {
                LinearLayout la = (LinearLayout) layout.getChildAt(d);
                Log.e("dapat ", " to" + d);
                for (int c = 0; c < la.getChildCount(); c++) {
                    View v = (View) la.getChildAt(c);
                    if (v instanceof CheckBox) {
                        CheckBox b = (CheckBox) v;
                        if (aftercare[d].equals("1")) {
                            b.setChecked(true);
                        } else {
                            b.setChecked(false);
                        }
                    }
                }
            }
            Log.e("fast", slowfast[1]);
            String[] aftercare2 = slowfast[1].split("-");
            LinearLayout layout2 = (LinearLayout) findViewById(R.id.fastlanel);

            for (int d = 0; d < layout2.getChildCount(); d++) {
                LinearLayout la = (LinearLayout) layout2.getChildAt(d);
                Log.e("dapat ", " to" + d);
                for (int c = 0; c < la.getChildCount(); c++) {
                    View v = (View) la.getChildAt(c);
                    if (v instanceof CheckBox) {
                        CheckBox b = (CheckBox) v;
                        if (aftercare2[d].equals("1")) {
                            b.setChecked(true);
                        } else {
                            b.setChecked(false);
                        }
                    }
                }
            }
        }

        disableEditText();
    }

    public boolean sD(View views) {
        boolean ischecked = false;
        CheckBox layout = null;
        ViewGroup row = (ViewGroup) views.getParent();
        for (int itemPos = 0; itemPos < row.getChildCount(); itemPos++) {
            View view = row.getChildAt(itemPos);
            if (view instanceof CheckBox) {
                layout = (CheckBox) view; //Found it!
                if (layout.isChecked() == true) {
                    ischecked = true;
                } else {
                    ischecked = false;
                }
                break;
            }
        }
        return ischecked;
    }

    public String getcontetn(String gets, int position) {
        String getz = "";
        String[] splitters = gets.split("~");

        String abc = splitters[position].replace("^", "#");
        String[] splitter = abc.split("#");

        if (splitter.length >= 2) {
            getz = splitter[1];
        } else {
            getz = "";
        }
        return getz.replace("Metres:", "");
    }

    public void disableEditText() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.slowlanel);
        for (int d = 0; d < layout.getChildCount(); d++) {
            LinearLayout la = (LinearLayout) layout.getChildAt(d);
            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);
                if (v instanceof EditText) {
                    EditText s = (EditText) v;
                    s.setEnabled(false);
                }
            }
        }

        LinearLayout layout2 = (LinearLayout) findViewById(R.id.fastlanel);

        for (int d = 0; d < layout2.getChildCount(); d++) {
            LinearLayout la = (LinearLayout) layout2.getChildAt(d);
            Log.e("dapat ", " to" + d);
            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);
                if (v instanceof EditText) {
                    EditText s = (EditText) v;
                    s.setEnabled(false);
                }
            }
        }
    }

}
