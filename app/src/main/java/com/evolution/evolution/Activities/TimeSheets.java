package com.evolution.evolution.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ProgressBar;

import com.evolution.evolution.Adapters.AdapterFleetMember;
import com.evolution.evolution.Helpers.ConnectionDetector;
import com.evolution.evolution.Helpers.DatabaseHelper;
import com.evolution.evolution.Helpers.SessionManager;
import com.evolution.evolution.Interface.EventInterface;
import com.evolution.evolution.Models.Job;
import com.evolution.evolution.Models.Timesheet;
import com.evolution.evolution.R;

import org.parceler.Parcels;

import java.util.ArrayList;

public class TimeSheets extends AppCompatActivity implements EventInterface {
    Toolbar toolbar;
    String jobId;
    private RecyclerView mRecyclerView;
    private AdapterFleetMember mAdapter;
    public LinearLayoutManager mLayoutManager;
    private String token;
    public SessionManager session;
    Timesheet item;
    Job mJobObject;
    ConnectionDetector connection;
    DatabaseHelper db;
    private ArrayList<Timesheet> timesheetmem = new ArrayList<Timesheet>();
    ProgressBar progressBar5;
    private EventInterface callback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timesheets);

        progressBar5 = (ProgressBar) findViewById(R.id.progressBar5);
        session = new SessionManager(this);
        token = session.getToken();

        connection = new ConnectionDetector(this);
        db = new DatabaseHelper(getApplicationContext());
        Bundle extras = getIntent().getExtras();
        mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        initToolBar();
        initRecyclerView();
        getJobInfo();
    }

    public void initRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.fleetmembers);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("TimeSheets");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        timesheetmem = db.getallTimesheets(mJobObject.getJobID()).getTimesheets();
        mAdapter = new AdapterFleetMember(db.getallTimesheets(mJobObject.getJobID()).getTimesheets(), getApplicationContext(), "", db.getallTimesheets(mJobObject.getJobID()));
        mRecyclerView.setAdapter(mAdapter);
        progressBar5.setVisibility(progressBar5.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                return true;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getJobInfo() {
        timesheetmem = db.getallTimesheets(mJobObject.getJobID()).getTimesheets();
        mAdapter = new AdapterFleetMember(db.getallTimesheets(mJobObject.getJobID()).getTimesheets(), getApplicationContext(), "", db.getallTimesheets(mJobObject.getJobID()));
        mRecyclerView.setAdapter(mAdapter);
        progressBar5.setVisibility(progressBar5.GONE);
    }

    @Override
    public void openTimeActivity(int position) {

    }
}
