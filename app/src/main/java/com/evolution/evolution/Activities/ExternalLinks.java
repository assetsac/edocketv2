package com.evolution.evolution.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Browser;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.evolution.evolution.Adapters.AdapterExternalLinks;
import com.evolution.evolution.Adapters.AdapterMoreMenu;
import com.evolution.evolution.Helpers.Constants;
import com.evolution.evolution.Helpers.DatabaseHelper;
import com.evolution.evolution.Helpers.RecyclerItemClickListener;
import com.evolution.evolution.Helpers.SessionManager;
import com.evolution.evolution.Models.DocketAttachments;
import com.evolution.evolution.Models.Job;
import com.evolution.evolution.R;

import org.parceler.Parcels;

import java.util.ArrayList;

public class ExternalLinks extends AppCompatActivity {
    Toolbar toolbar;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<DocketAttachments> docketAttachments = new ArrayList<DocketAttachments>();
    DatabaseHelper db;
    Job mJobObject;
    LinearLayout loadingnodata;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_external_links);
        db = new DatabaseHelper(getApplicationContext());
        mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        session = new SessionManager(this);

        initToolBar();
        inivRecyclerView();
    }

    public void inivRecyclerView() {

        mRecyclerView = (RecyclerView) findViewById(R.id.links);
        loadingnodata = (LinearLayout) findViewById(R.id.loadingnodata);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new AdapterExternalLinks(db.getAllDocketAttachments(mJobObject.getJobID()).getDocketAttachments(), getApplicationContext());
        mRecyclerView.setAdapter(mAdapter);
        docketAttachments = db.getAllDocketAttachments(mJobObject.getJobID()).getDocketAttachments();
        if (docketAttachments.size() <= 0) {
            loadingnodata.setVisibility(loadingnodata.VISIBLE);
        } else {
            loadingnodata.setVisibility(loadingnodata.GONE);
        }
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        String urld = docketAttachments.get(position).getAppLink();
                        Intent s = new Intent(Intent.ACTION_VIEW, Uri.parse(urld));
                        Bundle bundle = new Bundle();
                        String[] sd = session.getDocushareCookie().split(",");
                        bundle.putString("X-Auth-Token", session.getToken());
                        bundle.putString("Authorization", Constants.REQUEST_JOBINFO);
                        s.putExtra(Browser.EXTRA_HEADERS, bundle);
                        startActivity(s);
                    }
                })
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                return true;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("External Links");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
