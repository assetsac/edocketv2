package com.evolution.evolution.Activities;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.evolution.evolution.Adapters.AdapterRequirements;
import com.evolution.evolution.Helpers.ConnectionDetector;
import com.evolution.evolution.Helpers.DatabaseHelper;
import com.evolution.evolution.Helpers.RecyclerItemClickListener;
import com.evolution.evolution.Helpers.SessionManager;
import com.evolution.evolution.Models.Job;
import com.evolution.evolution.Models.ModelRequirements;
import com.evolution.evolution.R;

import org.parceler.Parcels;

import java.util.ArrayList;

public class Requirements extends AppCompatActivity {
    Toolbar toolbar;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ModelRequirements item;
    ProgressBar progressBar6;
    private ArrayList<ModelRequirements> requirements = new ArrayList<ModelRequirements>();
    Job mJobObject;
    private String token;
    public SessionManager session;
    ConnectionDetector connection;
    DatabaseHelper db;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.requirements);

        initToolBar();
        initRecyclerView();

        session = new SessionManager(this);
        token = session.getToken();
        connection = new ConnectionDetector(this);
        dialog = new Dialog(this,  R.style.FullHeightDialog);
        Bundle extras = getIntent().getExtras();

        mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        db = new DatabaseHelper(getApplicationContext());
        progressBar6 = (ProgressBar) findViewById(R.id.progressBar6);
        getJobInfo();

    }

    public void initRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.requirementss);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Requirements");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                return true;
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getJobInfo() {
        ArrayList<ModelRequirements> requirements = new ArrayList<ModelRequirements>();

        for (int i = 0; i < db.getallRequirements(mJobObject.getJobID()).getmRequirements().size(); i++) {
            if(!db.getallRequirements(mJobObject.getJobID()).getmRequirements().get(i).getRequirementID().equals("701")){
                requirements.add(db.getallRequirements(mJobObject.getJobID()).getmRequirements().get(i));
            }else{
            }
        }

        mAdapter = new AdapterRequirements(requirements, getApplicationContext());
        mRecyclerView.setAdapter(mAdapter);
        progressBar6.setVisibility(progressBar6.GONE);
        requirements = db.getallRequirements(mJobObject.getJobID()).getmRequirements();
    }
}
