package com.evolution.evolution.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.evolution.evolution.Adapters.AdapterPreSiteChecklost;
import com.evolution.evolution.Helpers.DatabaseHelper;
import com.evolution.evolution.Models.Job;
import com.evolution.evolution.Models.ModelPreSiteChecklistCarParts;
import com.evolution.evolution.Models.ModelPreSiteChecklistInfo;
import com.evolution.evolution.Models.Timesheet;
import com.evolution.evolution.R;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class PreStartChecklist extends AppCompatActivity {

    private static final String TAG = "PreStartChecklist";
    private boolean debug = false;

    //View
    private LinearLayout firstlayer;
    private ViewPager mViewPager;
    private Toolbar toolbar;
    boolean iscomplete;

    private TextView viewCar;

    private AdapterPreSiteChecklost mCustomPagerAdapter;

    //Dialog
    public AlertDialog dialog;
    public AlertDialog.Builder builder;

    private List<String> categories;
    private List<String> vehichels;
    private Job mJobObject;

    Timesheet timesheet;
    DatabaseHelper db;

    int[] pickup = {R.drawable.newpickuptop,R.drawable.newpickupfront,R.drawable.newpickupback,R.drawable.newpickupsideright, R.drawable.newpickupsideleft};
    int[] truck1view =  {R.drawable.newpickuptop,R.drawable.newpickupfront,R.drawable.newpickupback,R.drawable.newpickupsideright, R.drawable.newpickupsideleft};
    int[] truck2view = { R.drawable.truck1_4, R.drawable.truck1_1, R.drawable.truck1_3,R.drawable.truck1_5, R.drawable.truck1_2 };
    int[] truck3view = {R.drawable.truck3_1, R.drawable.truck3_4, R.drawable.truck3_5,R.drawable.truck3_2, R.drawable.truck3_3};

    private ArrayList<ModelPreSiteChecklistCarParts> preSiteChecllistItem;
    ModelPreSiteChecklistCarParts presitechecklistitem;
    ModelPreSiteChecklistInfo mModelPreSiteChecklistInfo;
    int[] selectedVehicle = {};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_site_checklist);

        initToolBar();

        mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        timesheet = Parcels.unwrap(getIntent().getExtras().getParcelable("timesheet"));

        db = new DatabaseHelper(this);

        firstlayer = (LinearLayout) findViewById(R.id.firstlayer);
        viewCar = (TextView) findViewById(R.id.viewCar);

        iniArray();

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mModelPreSiteChecklistInfo = db.getPreSiteCheckistInfo(mJobObject.getJobID(), timesheet.getContactID());

        if (!db.checkPresiteChecklistinfo(mJobObject.getJobID(), timesheet.getContactID())) {
            LayoutInflater inflater = getLayoutInflater();
            dialogSelectVehicle(R.layout.daiglog_select_vehicle_, inflater);
        } else {
            preSiteChecllistItem = db.getAllPreSiteCheckistPartsView(mJobObject.getJobID(), mViewPager.getCurrentItem() + 1, timesheet.getContactID());
            mCustomPagerAdapter = new AdapterPreSiteChecklost(setVehicle((Integer.parseInt(mModelPreSiteChecklistInfo.getVehicleType()) - 1)), this, screenSize(), mJobObject.getJobID(), timesheet.getContactID());
            mViewPager.setAdapter(mCustomPagerAdapter);
            mViewPager.setCurrentItem(0);
        }

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                sliderTitle(position);
                hideKeyboard();
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void loaddata() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mModelPreSiteChecklistInfo = db.getPreSiteCheckistInfo(mJobObject.getJobID(), timesheet.getContactID());
                preSiteChecllistItem = db.getAllPreSiteCheckistPartsView(mJobObject.getJobID(), mViewPager.getCurrentItem() + 1, timesheet.getContactID());
                mCustomPagerAdapter = new AdapterPreSiteChecklost(setVehicle((Integer.parseInt(mModelPreSiteChecklistInfo.getVehicleType()) - 1)), getApplicationContext(), screenSize(), mJobObject.getJobID(), timesheet.getContactID());
                mViewPager.setAdapter(mCustomPagerAdapter);
                mViewPager.setCurrentItem(0);
            }
        }, 500);
    }
    public void sliderTitle(int position) {
        switch (position) {
            case 0:
                viewCar.setText("Top View");
                break;
            case 1:
                viewCar.setText("Front View");
                break;
            case 2:
                viewCar.setText("Rear View");
                break;
            case 3:
                viewCar.setText("Left Side View");
                break;
            case 4:
                viewCar.setText("Right Side View");
                break;
        }
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pre-Start Checklist");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void ClickSelected(View view) {
        LayoutInflater inflater = getLayoutInflater();
        dialogAddNotes(R.layout.daiglog_presitecheck_list, inflater, view);
    }

    public void iniArray() {

        categories = new ArrayList<String>();
        categories.add("");
        categories.add("Body Damage");
        categories.add("Tyres, Tread, Pressure");
        categories.add("Spare Tyre");
        categories.add("Fuel");
        categories.add("Strobes");
        categories.add("Arrow Board Lights");
        categories.add("Arrow Board Operation");
        categories.add("Lights & Indicators");
        categories.add("Reverse Beeper");
        categories.add("Engine Oil Level");
        categories.add("Power Steering Level");
        categories.add("Coolant Level");
        categories.add("Washer Level");
        categories.add("First Aid Kit Contents");
        categories.add("Fire Extinguisher");
        categories.add("Reverse Camera ( If Fitted )");
        categories.add("Tool Kit");
        categories.add("Jack");
        categories.add("Wheel Nut Indicators ( If Fitted )");

        vehichels = new ArrayList<String>();
        vehichels.add("Traffic Ute");
        vehichels.add("Highway Truck (< 3.5t)");
        vehichels.add("POD Truck (>3.5t)");
        vehichels.add("TMA Truck");

    }


    public void dialogSelectVehicle(int layout, LayoutInflater inflater) {
        //TODO: dialog

        builder = new AlertDialog.Builder(this);
        final AlertDialog dialogSelectV = builder.create();
        dialogSelectV.setCancelable(false);
        final View dialogView = inflater.inflate(layout, null);
        dialogSelectV.setView(dialogView);

        final Spinner vehichle3 = (Spinner) dialogView.findViewById(R.id.selectCar);

        final TextInputLayout input_layout_vehicle = (TextInputLayout) dialogView.findViewById(R.id.input_layout_vehicle);
        final TextInputLayout input_layout_odometer = (TextInputLayout) dialogView.findViewById(R.id.input_layout_odometer);

        final TextView t_driver_name = (TextView) dialogView.findViewById(R.id.t_driver_name);
        final EditText t_vehecleregistration = (EditText) dialogView.findViewById(R.id.t_vehecleregistration);
        final EditText t_odometer = (EditText) dialogView.findViewById(R.id.t_odometer);
        final EditText t_serviceduekms = (EditText) dialogView.findViewById(R.id.t_serviceduekms);

        final RadioGroup radioButtonGroup = (RadioGroup) dialogView.findViewById(R.id.piv_daynight);
        final RadioGroup retentionswheelnuts = (RadioGroup) dialogView.findViewById(R.id.retentionswheelnuts);

        Button close = (Button) dialogView.findViewById(R.id.close);
        Button save = (Button) dialogView.findViewById(R.id.save);

        initSpinner(vehichle3, vehichels);

        if (db.checkPresiteChecklistinfo(mJobObject.getJobID(), timesheet.getContactID())) {

            vehichle3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (vehichle3.getSelectedItemPosition() != (Integer.parseInt(mModelPreSiteChecklistInfo.getVehicleType()) - 1)) {
                        Dialog.Builder builder = null;
                        builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                            @Override
                            public void onPositiveActionClicked(DialogFragment fragment) {

                                mModelPreSiteChecklistInfo = db.getPreSiteCheckistInfo(mJobObject.getJobID(), timesheet.getContactID());
                                preSiteChecllistItem = db.getAllPreSiteCheckistPartsView(mJobObject.getJobID(), mViewPager.getCurrentItem() + 1, timesheet.getContactID());
                                mCustomPagerAdapter = new AdapterPreSiteChecklost(setVehicle((Integer.parseInt(mModelPreSiteChecklistInfo.getVehicleType()) - 1)), getApplicationContext(), screenSize(), mJobObject.getJobID(), timesheet.getContactID());
                                mViewPager.setAdapter(mCustomPagerAdapter);
                                mViewPager.setCurrentItem(0);

                                super.onPositiveActionClicked(fragment);
                            }

                            @Override
                            public void onNegativeActionClicked(DialogFragment fragment) {
                                super.onNegativeActionClicked(fragment);
                                vehichle3.setSelection((Integer.parseInt(mModelPreSiteChecklistInfo.getVehicleType()) - 1));
                                hideKeyboard();
                            }
                        };

                        ((SimpleDialog.Builder) builder).message("Changing this will remove data from vehicle parts checklist.")
                                .title("Change Vehicle?")
                                .positiveAction("Yes")
                                .negativeAction("CANCEL");
                        DialogFragment fragment = DialogFragment.newInstance(builder);
                        fragment.show(getSupportFragmentManager(), null);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }

            });

            t_vehecleregistration.setText(mModelPreSiteChecklistInfo.getVehicleRego());
            t_odometer.setText(mModelPreSiteChecklistInfo.getOdometer());
            t_serviceduekms.setText(mModelPreSiteChecklistInfo.getServiceDueKms());

            int piv;

            if (mModelPreSiteChecklistInfo.getDepotPIV().equals("Depot")) {
                piv = 0;
            } else if (mModelPreSiteChecklistInfo.getDepotPIV().equals("PIV")) {
                piv = 1;
            } else {
                piv = -1;
            }

            ((RadioButton) radioButtonGroup.getChildAt(piv)).setChecked(true);
            ((RadioButton) retentionswheelnuts.getChildAt(Integer.parseInt(mModelPreSiteChecklistInfo.getRetensionWheelNuts()))).setChecked(true);
            vehichle3.setSelection((Integer.parseInt(mModelPreSiteChecklistInfo.getVehicleType()) - 1));
        }
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iscomplete = true;
                if ((radioButtonGroup.getCheckedRadioButtonId() == -1) || retentionswheelnuts.getCheckedRadioButtonId() == -1) {
                    iscomplete = false;
                    Toast.makeText(PreStartChecklist.this, "Please Complete all fields.", Toast.LENGTH_SHORT).show();
                }
                validateEditText(t_odometer, input_layout_odometer, "Enter Odometer End.");
                validateEditText(t_vehecleregistration, input_layout_vehicle, "Enter Vehicle Registration.");
                if (iscomplete) {

                    if (db.checkPresiteChecklistinfo(mJobObject.getJobID(), timesheet.getContactID())) {
                        if (vehichle3.getSelectedItemPosition() != (Integer.parseInt(mModelPreSiteChecklistInfo.getVehicleType()) - 1)) {
                            //delete
                            db.removePresiteListItemByChangingVehicle(mJobObject.getJobID(), timesheet.getContactID());
                        }
                    }

                    ModelPreSiteChecklistInfo siteChecklistInfo = new ModelPreSiteChecklistInfo();
                    siteChecklistInfo.setJobID(mJobObject.getJobID());
                    siteChecklistInfo.setContactID(timesheet.getContactID());
                    siteChecklistInfo.setVehicleRego(t_vehecleregistration.getText().toString());
                    siteChecklistInfo.setVehicleType((vehichle3.getSelectedItemPosition() + 1) + "");
                    siteChecklistInfo.setDepotPIV(getRadiogroupValue(radioButtonGroup, true));
                    siteChecklistInfo.setServiceDueKms(t_serviceduekms.getText().toString());
                    siteChecklistInfo.setRetensionWheelNuts(getRadiogroupValue(retentionswheelnuts, false));
                    siteChecklistInfo.setOdometer(t_odometer.getText().toString());
                    db.addPreSiteCheckistInfo(siteChecklistInfo);

                    finish();
                    Intent c = new Intent(PreStartChecklist.this, PreStartChecklist.class);
                    c.putExtra("timesheet", Parcels.wrap(timesheet));
                    c.putExtra("data", Parcels.wrap(mJobObject));
                    view.getContext().startActivity(c);

                    dialogSelectV.dismiss();
                }
            }
        });

        t_driver_name.setText(timesheet.getName());

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!db.checkPresiteChecklistinfo(mJobObject.getJobID(), timesheet.getContactID())) {
                    finish();
                } else {
                    dialogSelectV.dismiss();
                }
            }
        });

        dialogSelectV.show();
        builder.setCancelable(false);
    }

    public int[] setVehicle(int vehicleSelected) {
        switch (vehicleSelected) {
            case 0:
                selectedVehicle = pickup;
                break;
            case 1:
                selectedVehicle = truck1view;
                break;
            case 2:
                selectedVehicle = truck2view;
                break;
            case 3:
                selectedVehicle = truck3view;
                break;
        }
        return selectedVehicle;
    }

    //Todo: Cause of error
    public String getRadiogroupValue(RadioGroup radioButtonGroup, boolean isString) {

        int radioButtonID = radioButtonGroup.getCheckedRadioButtonId();
        View radioButton = radioButtonGroup.findViewById(radioButtonID);
        int idx = radioButtonGroup.indexOfChild(radioButton);

        final String selectedtext;
        if (isString) {
            RadioButton r = (RadioButton) radioButtonGroup.getChildAt(idx);
            selectedtext = r.getText().toString();
        } else {
            selectedtext = idx + "";
        }
        return selectedtext;
    }

    private boolean validateEditText(EditText inputName, TextInputLayout inputLayoutName, String errormessage) {
        if (inputName.getText().toString().trim().isEmpty()) {
            inputLayoutName.setError(errormessage);
            requestFocus(inputName);
            iscomplete = false;
            return false;
        } else {
            inputLayoutName.setErrorEnabled(false);
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void dialogAddNotes(int layout, LayoutInflater inflater, final View view2) {

        final String[] tags = view2.getTag().toString().split(",");
        presitechecklistitem = db.getPreSiteCheckistParts(mJobObject.getJobID(), mViewPager.getCurrentItem() + 1 + "", tags[0], tags[1], timesheet.getContactID());
        //TODO: dialog
        builder = new AlertDialog.Builder(this);
        dialog = builder.create();
        dialog.setCancelable(false);
        View dialogView = inflater.inflate(layout, null);
        dialog.setView(dialogView);

        final Spinner categorys = (Spinner) dialogView.findViewById(R.id.selectParts);
        Button close = (Button) dialogView.findViewById(R.id.close);
        Button save = (Button) dialogView.findViewById(R.id.save);
        ImageView removeselectedpart = (ImageView) dialogView.findViewById(R.id.removeselectedpart);
        initSpinner(categorys, categories);

        final TextInputLayout input_layout_note = (TextInputLayout) dialogView.findViewById(R.id.input_layout_note);
        final EditText t_notes = (EditText) dialogView.findViewById(R.id.t_notes);
        t_notes.setText("");
        t_notes.setText(presitechecklistitem.getNotes());

        categorys.setSelection(categories.indexOf(presitechecklistitem.getItem()));

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iscomplete = true;
                validateEditText(t_notes, input_layout_note, "Please enter Notes.");
                if (iscomplete) {
                    if (!(categorys.getSelectedItemPosition() == 0)) {
                        ModelPreSiteChecklistCarParts preSiteChecllistItem = new ModelPreSiteChecklistCarParts();
                        preSiteChecllistItem.setJobID(mJobObject.getJobID());
                        preSiteChecllistItem.setItem(categorys.getSelectedItem() + "");
                        preSiteChecllistItem.setNotes(t_notes.getText().toString());
                        preSiteChecllistItem.setRow(tags[1]);
                        preSiteChecllistItem.setColumn(tags[0]);
                        preSiteChecllistItem.setView((mViewPager.getCurrentItem() + 1) + "");
                        preSiteChecllistItem.setContactID(timesheet.getContactID());
                        db.addPreSiteCheckistParts(preSiteChecllistItem);
                        view2.setBackgroundResource(R.color.transparentblueselected);
                        hideKeyboard();
                        dialog.dismiss();
                    } else {
                        Toast.makeText(PreStartChecklist.this, "Please Select Vehicle Parts.", Toast.LENGTH_SHORT).show();
                    }
                }
                hideKeyboard();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                dialog.dismiss();
            }
        });

        removeselectedpart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog.Builder builder = null;
                builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                    @Override
                    public void onPositiveActionClicked(DialogFragment fragment) {
                        view2.setBackgroundResource(R.color.transparentblueselectedorig);
                        dialog.dismiss();
                        hideKeyboard();
                        db.removePresiteListItem(mJobObject.getJobID(), mViewPager.getCurrentItem() + 1 + "", tags[0], tags[1], timesheet.getContactID());
                        super.onPositiveActionClicked(fragment);
                    }

                    @Override
                    public void onNegativeActionClicked(DialogFragment fragment) {
                        super.onNegativeActionClicked(fragment);
                        hideKeyboard();
                    }
                };

                ((SimpleDialog.Builder) builder).message("Are you sure?")
                        .title("Remove notes?")
                        .positiveAction("Yes")
                        .negativeAction("CANCEL");
                DialogFragment fragment = DialogFragment.newInstance(builder);
                fragment.show(getSupportFragmentManager(), null);
            }
        });

        dialog.show();
        builder.setCancelable(false);
    }


    public int screenSize() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x / 6;
        return width;
    }


    public void initSpinner(Spinner spinner, List<String> array) {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // On selecting a spinner item
                String item = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnerlocation, array);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.spinnerlocation);
        spinner.setAdapter(dataAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pre_site_checklist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:

                LayoutInflater inflater = getLayoutInflater();
                dialogSelectVehicle(R.layout.daiglog_select_vehicle_, inflater);

                return true;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public void collectionBack(View view) {
        int currentPosition = mViewPager.getCurrentItem();
        Log.e(TAG, "collectionNext: " + currentPosition);
        mViewPager.setCurrentItem(currentPosition - 1);

        sliderTitle(mViewPager.getCurrentItem());
    }

    public void collectionNext(View view) {
        int currentPosition = mViewPager.getCurrentItem();
        Log.e(TAG, "collectionNext: " + currentPosition);
        mViewPager.setCurrentItem(currentPosition + 1);

        sliderTitle(mViewPager.getCurrentItem());
    }

}
