package com.evolution.evolution.Activities.Forms;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.evolution.evolution.Helpers.DateHelper;
import com.evolution.evolution.Helpers.FormValidation;
import com.evolution.evolution.R;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rey.material.app.DatePickerDialog;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.TimePickerDialog;

import java.text.DateFormat;

public class IncidentBasic extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    ImageButton setDate;
    Dialog dialog;
    Dialog.Builder builder = null;
    DateHelper dates;
    MaterialEditText incidentdate, incidentname, incidentdepot, incidentcontact;
    FormValidation val;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_information);
        initToolBar();
        initViews();
        dates = new DateHelper();
        val = new FormValidation(this);
    }

    public void initViews() {
        setDate = (ImageButton) findViewById(R.id.setDate);
        incidentdate = (MaterialEditText) findViewById(R.id.incidentdate);
        incidentname = (MaterialEditText) findViewById(R.id.incidentname);
        incidentdepot = (MaterialEditText) findViewById(R.id.incidentdepot);
        incidentcontact = (MaterialEditText) findViewById(R.id.incidentcontact);
        setDate.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_basic_information, menu);
        return true;
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Basic Information");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_settings:

                val.validateSetError(incidentname, "Please fill up name.");
                val.validateSetError(incidentdepot, "Please fill up depot.");
                val.validateSetError(incidentcontact, "Please fill up contact number.");
                val.validateSetError(incidentdate, "Please fill up date.");

                if (val.validateSetError(incidentname, "Please fill up name.") && val.validateSetError(incidentdepot, "Please fill up depot.") && val.validateSetError(incidentcontact, "Please fill up contact number.") && val.validateSetError(incidentdate, "Please fill up date.")) {
                    Toast.makeText(getApplicationContext(), "Changes has been saved.", Toast.LENGTH_SHORT).show();
                    finish();
                }

                break;
            case android.R.id.home:
                finish();
                overridePendingTransition(0, 0);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.setDate:
                initTimepicker();
                break;
        }
    }

    public void initTimepicker() {

        builder = new DatePickerDialog.Builder() {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                final DatePickerDialog dialog2 = (DatePickerDialog) fragment.getDialog();
                incidentdate.setText(dates.miliSecondConverter(dialog2.getDate() + "", "dd, MMMM yyyy hh:mm"));
                super.onPositiveActionClicked(fragment);
                super.onNegativeActionClicked(fragment);

            }
        };

        builder.positiveAction("OK")
                .negativeAction("CANCEL");

        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(), null);

    }
}
