package com.evolution.evolution.Activities.signs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.evolution.evolution.Helpers.DatabaseHelper;
import com.evolution.evolution.Helpers.DateHelper;
import com.evolution.evolution.Models.Job;
import com.evolution.evolution.R;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class AfterCareNSQ extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    RadioButton slowlane, fastlane;
    LinearLayout slowlanel, fastlanel;
    DatabaseHelper db;
    Job mJobObject;
    EditText n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41;
    EditText na1, na2, na3, na4, na5, na6, na7, na8, na9, na10, na11, na12, na13, na14, na15, na16, na17, na18, na19, na20, na21, na22, na23, na24, na25, na26, na27, na28, na29, na30, na31, na32, na33, na34, na35, na36, na37, na38, na39, na40, na41;
    DateHelper date;

    MaterialEditText cwevolution4, cwlocallandmark4, TimeErected, TimeCollected, TimeChecked1, TimeChecked2, TimeChecked3, TimeChecked4;
    Spinner spinner;
    List<String> categories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_care_nsq);
        initToolBar();
        initViews();

        db = new DatabaseHelper(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        }
        date = new DateHelper();

    }

    public void initSpinner() {
        // Spinner click listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // On selecting a spinner item
                String item = parent.getItemAtPosition(position).toString();

                // Showing selected spinner item
//                Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner.setEnabled(false);
        // Spinner Drop down elements
        categories = new ArrayList<String>();
        categories.add("North");
        categories.add("South");
        categories.add("East");
        categories.add("West");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnerlocation, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.spinnerlocation);

        spinner.setAdapter(dataAdapter);

    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("After Care NSW");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initViews() {

        slowlane = (RadioButton) findViewById(R.id.slowlane);
        fastlane = (RadioButton) findViewById(R.id.fastlane);

        slowlanel = (LinearLayout) findViewById(R.id.slowlanel);
        fastlanel = (LinearLayout) findViewById(R.id.fastlanel);

        slowlane.setOnClickListener(this);
        fastlane.setOnClickListener(this);

        TimeErected = (MaterialEditText) findViewById(R.id.TimeErected);
        TimeCollected = (MaterialEditText) findViewById(R.id.TimeCollected);
        TimeChecked1 = (MaterialEditText) findViewById(R.id.TimeChecked1);
        TimeChecked2 = (MaterialEditText) findViewById(R.id.TimeChecked2);
        TimeChecked3 = (MaterialEditText) findViewById(R.id.TimeChecked3);
        TimeChecked4 = (MaterialEditText) findViewById(R.id.TimeChecked4);


        spinner = (Spinner) findViewById(R.id.cwdirectionlist4);

        cwevolution4 = (MaterialEditText) findViewById(R.id.cwevolution4);
        cwlocallandmark4 = (MaterialEditText) findViewById(R.id.cwlocallandmark4);


        n1 = (EditText) findViewById(R.id.n1);
        n2 = (EditText) findViewById(R.id.n2);
        n3 = (EditText) findViewById(R.id.n3);
        n4 = (EditText) findViewById(R.id.n4);
        n5 = (EditText) findViewById(R.id.n5);
        n6 = (EditText) findViewById(R.id.n6);
        n7 = (EditText) findViewById(R.id.n7);
        n8 = (EditText) findViewById(R.id.n8);
        n9 = (EditText) findViewById(R.id.n9);
        n10 = (EditText) findViewById(R.id.n10);
        n11 = (EditText) findViewById(R.id.n11);
        n12 = (EditText) findViewById(R.id.n12);
        n13 = (EditText) findViewById(R.id.n13);
        n14 = (EditText) findViewById(R.id.n14);
        n15 = (EditText) findViewById(R.id.n15);
        n16 = (EditText) findViewById(R.id.n16);
        n17 = (EditText) findViewById(R.id.n17);
        n18 = (EditText) findViewById(R.id.n18);
        n19 = (EditText) findViewById(R.id.n19);
        n20 = (EditText) findViewById(R.id.n20);
        n21 = (EditText) findViewById(R.id.n21);
        n22 = (EditText) findViewById(R.id.n22);
        n23 = (EditText) findViewById(R.id.n23);
        n24 = (EditText) findViewById(R.id.n24);
        n25 = (EditText) findViewById(R.id.n25);
        n26 = (EditText) findViewById(R.id.n26);
        n27 = (EditText) findViewById(R.id.n27);
        n28 = (EditText) findViewById(R.id.n28);
        n29 = (EditText) findViewById(R.id.n29);
        n30 = (EditText) findViewById(R.id.n30);
        n31 = (EditText) findViewById(R.id.n31);
        n32 = (EditText) findViewById(R.id.n32);
        n33 = (EditText) findViewById(R.id.n33);
        n34 = (EditText) findViewById(R.id.n34);
        n35 = (EditText) findViewById(R.id.n35);
        n36 = (EditText) findViewById(R.id.n36);
        n37 = (EditText) findViewById(R.id.n37);
        n38 = (EditText) findViewById(R.id.n38);
        n39 = (EditText) findViewById(R.id.n39);
        n40 = (EditText) findViewById(R.id.n40);
        n41 = (EditText) findViewById(R.id.n41);

        na1 = (EditText) findViewById(R.id.na1);
        na2 = (EditText) findViewById(R.id.na2);
        na3 = (EditText) findViewById(R.id.na3);
        na4 = (EditText) findViewById(R.id.na4);
        na5 = (EditText) findViewById(R.id.na5);
        na6 = (EditText) findViewById(R.id.na6);
        na7 = (EditText) findViewById(R.id.na7);
        na8 = (EditText) findViewById(R.id.na8);
        na9 = (EditText) findViewById(R.id.na9);
        na10 = (EditText) findViewById(R.id.na10);
        na11 = (EditText) findViewById(R.id.na11);
        na12 = (EditText) findViewById(R.id.na12);
        na13 = (EditText) findViewById(R.id.na13);
        na14 = (EditText) findViewById(R.id.na14);
        na15 = (EditText) findViewById(R.id.na15);
        na16 = (EditText) findViewById(R.id.na16);
        na17 = (EditText) findViewById(R.id.na17);
        na18 = (EditText) findViewById(R.id.na18);
        na19 = (EditText) findViewById(R.id.na19);
        na20 = (EditText) findViewById(R.id.na20);
        na21 = (EditText) findViewById(R.id.na21);
        na22 = (EditText) findViewById(R.id.na22);
        na23 = (EditText) findViewById(R.id.na23);
        na24 = (EditText) findViewById(R.id.na24);
        na25 = (EditText) findViewById(R.id.na25);
        na26 = (EditText) findViewById(R.id.na26);
        na27 = (EditText) findViewById(R.id.na27);
        na28 = (EditText) findViewById(R.id.na28);
        na29 = (EditText) findViewById(R.id.na29);
        na30 = (EditText) findViewById(R.id.na30);
        na31 = (EditText) findViewById(R.id.na31);
        na32 = (EditText) findViewById(R.id.na32);
        na33 = (EditText) findViewById(R.id.na33);
        na34 = (EditText) findViewById(R.id.na34);
        na35 = (EditText) findViewById(R.id.na35);
        na36 = (EditText) findViewById(R.id.na36);
        na37 = (EditText) findViewById(R.id.na37);
        na38 = (EditText) findViewById(R.id.na38);
        na39 = (EditText) findViewById(R.id.na39);
        na40 = (EditText) findViewById(R.id.na40);
        na41 = (EditText) findViewById(R.id.na41);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.slowlane:
                slowlanel.setVisibility(slowlanel.VISIBLE);
                fastlanel.setVisibility(slowlanel.GONE);
                break;
            case R.id.fastlane:
                slowlanel.setVisibility(slowlanel.GONE);
                fastlanel.setVisibility(slowlanel.VISIBLE);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        populateData();
        initSpinner();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_after_care_qld, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.saveaftercare:
                checksaveCheckboxes();
                break;
            case android.R.id.home:
                finish();
                overridePendingTransition(0, 0);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public String getSignageAudit() {
        String temp = "";
        temp = "{\"mac\":" + db.getSignageAudt(mJobObject.getJobID()) + "}";
        return temp;
    }

    public void hideItem(EditText ed, JSONObject c, int position) throws JSONException {
        if (getcontetn(c.getString("AuditSigns"), position).equals("")) {
//            ((ViewGroup) ed.getParent()).setVisibility(((ViewGroup) ed.getParent()).GONE);
            ed.setEnabled(false);
        } else {
            ed.setEnabled(false);
        }
    }


    public void checksaveCheckboxes() {
        String returnsslow = "";
        String returnsfast = "";
        LinearLayout layout = (LinearLayout) findViewById(R.id.slowlanel);
        for (int dfd = 0; dfd < layout.getChildCount(); dfd++) {
            LinearLayout la = (LinearLayout) layout.getChildAt(dfd);
            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);
                if (v instanceof CheckBox) {
                    CheckBox b = (CheckBox) v;
                    if (b.isChecked() == true) {
                        returnsslow = returnsslow + "1-";
                    } else {
                        returnsslow = returnsslow + "0-";
                    }
                }


            }
        }

        LinearLayout layout2 = (LinearLayout) findViewById(R.id.fastlanel);

        for (int fa = 0; fa < layout2.getChildCount(); fa++) {
            LinearLayout la = (LinearLayout) layout2.getChildAt(fa);
            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);
                if (v instanceof CheckBox) {
                    CheckBox b = (CheckBox) v;
                    if (b.isChecked() == true) {
                        returnsfast = returnsfast + "1-";
                    } else {
                        returnsfast = returnsfast + "0-";
                    }
                }
            }
        }

        Log.e("before save", returnsslow + "~" + returnsfast);

        db.updateAfterCare(mJobObject.getId(), returnsslow + "~" + returnsfast);
    }


    public void populateData() {
        JSONArray data = null;
        Log.e("haha", getSignageAudit());
        if (getSignageAudit().equals("{\"mac\":null}")) {

        } else {
            try {
                JSONObject jsonObj = new JSONObject(getSignageAudit());

                // Getting JSON Array node
                data = jsonObj.getJSONArray("mac");

                JSONObject c = data.getJSONObject(0);
                JSONObject x = data.getJSONObject(1);

                cwevolution4.setText(c.getString("RegoNo"));
                cwlocallandmark4.setText(c.getString("Landmark"));

                spinner.setSelection(Integer.parseInt(c.getString("Direction")), true);

                if (!c.getString("TimeErected").equals("null")) {
                    TimeErected.setText(date.miliSecondConverter(c.getString("TimeErected"), "HH:mm:ss a"));
                }
                if (!c.getString("TimeCollected").equals("null")) {
                    TimeCollected.setText(date.miliSecondConverter(c.getString("TimeCollected"), "HH:mm:ss a"));
                }
                if (!c.getString("TimeChecked1").equals("null")) {
                    TimeChecked1.setText(date.miliSecondConverter(c.getString("TimeChecked1"), "HH:mm:ss a"));
                }
                if (!c.getString("TimeChecked2").equals("null")) {
                    TimeChecked2.setText(date.miliSecondConverter(c.getString("TimeChecked2"), "HH:mm:ss a"));
                }
                if (!c.getString("TimeChecked3").equals("null")) {
                    TimeChecked3.setText(date.miliSecondConverter(c.getString("TimeChecked3"), "HH:mm:ss a"));
                }
                if (!c.getString("TimeChecked4").equals("null")) {
                    TimeChecked4.setText(date.miliSecondConverter(c.getString("TimeChecked4"), "HH:mm:ss a"));
                }

                n1.setText(getcontetn(c.getString("AuditSigns"), 0));
                n2.setText(getcontetn(c.getString("AuditSigns"), 1));
                n3.setText(getcontetn(c.getString("AuditSigns"), 2));
                n4.setText(getcontetn(c.getString("AuditSigns"), 3));
                n5.setText(getcontetn(c.getString("AuditSigns"), 4));
                n6.setText(getcontetn(c.getString("AuditSigns"), 5));
                n7.setText(getcontetn(c.getString("AuditSigns"), 6));
                n8.setText(getcontetn(c.getString("AuditSigns"), 7));
                n9.setText(getcontetn(c.getString("AuditSigns"), 8));
                n10.setText(getcontetn(c.getString("AuditSigns"), 9));
                n11.setText(getcontetn(c.getString("AuditSigns"), 10));
                n12.setText(getcontetn(c.getString("AuditSigns"), 11));
                n13.setText(getcontetn(c.getString("AuditSigns"), 12));
                n14.setText(getcontetn(c.getString("AuditSigns"), 13));
                n15.setText(getcontetn(c.getString("AuditSigns"), 14));
                n16.setText(getcontetn(c.getString("AuditSigns"), 15));
                n17.setText(getcontetn(c.getString("AuditSigns"), 16));
                n18.setText(getcontetn(c.getString("AuditSigns"), 17));
                n19.setText(getcontetn(c.getString("AuditSigns"), 18));
                n20.setText(getcontetn(c.getString("AuditSigns"), 19));
                n21.setText(getcontetn(c.getString("AuditSigns"), 20));
                n22.setText(getcontetn(c.getString("AuditSigns"), 21));
                n23.setText(getcontetn(c.getString("AuditSigns"), 22));
                n24.setText(getcontetn(c.getString("AuditSigns"), 23));
                n25.setText(getcontetn(c.getString("AuditSigns"), 24));
                n26.setText(getcontetn(c.getString("AuditSigns"), 25));
                n27.setText(getcontetn(c.getString("AuditSigns"), 26));
                n28.setText(getcontetn(c.getString("AuditSigns"), 27));
                n29.setText(getcontetn(c.getString("AuditSigns"), 28));
                n30.setText(getcontetn(c.getString("AuditSigns"), 29));
                n31.setText(getcontetn(c.getString("AuditSigns"), 30));
                n32.setText(getcontetn(c.getString("AuditSigns"), 31));
                n33.setText(getcontetn(c.getString("AuditSigns"), 32));
                n34.setText(getcontetn(c.getString("AuditSigns"), 33));
                n35.setText(getcontetn(c.getString("AuditSigns"), 34));
                n36.setText(getcontetn(c.getString("AuditSigns"), 35));
                n37.setText(getcontetn(c.getString("AuditSigns"), 36));
                n38.setText(getcontetn(c.getString("AuditSigns"), 37));
                n39.setText(getcontetn(c.getString("AuditSigns"), 38));
                n40.setText(getcontetn(c.getString("AuditSigns"), 39));
                n41.setText(getcontetn(c.getString("AuditSigns"), 40));

                na1.setText(getcontetn(x.getString("AuditSigns"), 0));
                na2.setText(getcontetn(x.getString("AuditSigns"), 1));
                na3.setText(getcontetn(x.getString("AuditSigns"), 2));
                na4.setText(getcontetn(x.getString("AuditSigns"), 3));
                na5.setText(getcontetn(x.getString("AuditSigns"), 4));
                na6.setText(getcontetn(x.getString("AuditSigns"), 5));
                na7.setText(getcontetn(x.getString("AuditSigns"), 6));
                na8.setText(getcontetn(x.getString("AuditSigns"), 7));
                na9.setText(getcontetn(x.getString("AuditSigns"), 8));
                na10.setText(getcontetn(x.getString("AuditSigns"), 9));
                na11.setText(getcontetn(x.getString("AuditSigns"), 10));
                na12.setText(getcontetn(x.getString("AuditSigns"), 11));
                na13.setText(getcontetn(x.getString("AuditSigns"), 12));
                na14.setText(getcontetn(x.getString("AuditSigns"), 13));
                na15.setText(getcontetn(x.getString("AuditSigns"), 14));
                na16.setText(getcontetn(x.getString("AuditSigns"), 15));
                na17.setText(getcontetn(x.getString("AuditSigns"), 16));
                na18.setText(getcontetn(x.getString("AuditSigns"), 17));
                na19.setText(getcontetn(x.getString("AuditSigns"), 18));
                na20.setText(getcontetn(x.getString("AuditSigns"), 19));
                na21.setText(getcontetn(x.getString("AuditSigns"), 20));
                na22.setText(getcontetn(x.getString("AuditSigns"), 21));
                na23.setText(getcontetn(x.getString("AuditSigns"), 22));
                na24.setText(getcontetn(x.getString("AuditSigns"), 23));
                na25.setText(getcontetn(x.getString("AuditSigns"), 24));
                na26.setText(getcontetn(x.getString("AuditSigns"), 25));
                na27.setText(getcontetn(x.getString("AuditSigns"), 26));
                na28.setText(getcontetn(x.getString("AuditSigns"), 27));
                na29.setText(getcontetn(x.getString("AuditSigns"), 28));
                na30.setText(getcontetn(x.getString("AuditSigns"), 29));
                na31.setText(getcontetn(x.getString("AuditSigns"), 30));
                na32.setText(getcontetn(x.getString("AuditSigns"), 31));
                na33.setText(getcontetn(x.getString("AuditSigns"), 32));
                na34.setText(getcontetn(x.getString("AuditSigns"), 33));
                na35.setText(getcontetn(x.getString("AuditSigns"), 34));
                na36.setText(getcontetn(x.getString("AuditSigns"), 35));
                na37.setText(getcontetn(x.getString("AuditSigns"), 36));
                na38.setText(getcontetn(x.getString("AuditSigns"), 37));
                na39.setText(getcontetn(x.getString("AuditSigns"), 38));
                na40.setText(getcontetn(x.getString("AuditSigns"), 39));
                na41.setText(getcontetn(x.getString("AuditSigns"), 40));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (!db.getaftercare(mJobObject.getJobID()).equals("null")) {
            String[] slowfast = db.getaftercare(mJobObject.getJobID()).split("~");
            Log.e("slow", slowfast[0]);
            String[] aftercare = slowfast[0].split("-");


            LinearLayout layout = (LinearLayout) findViewById(R.id.slowlanel);

            for (int d = 0; d < layout.getChildCount(); d++) {
                LinearLayout la = (LinearLayout) layout.getChildAt(d);
                Log.e("dapat ", " to" + d);
                for (int c = 0; c < la.getChildCount(); c++) {
                    View v = (View) la.getChildAt(c);
                    if (v instanceof CheckBox) {
                        CheckBox b = (CheckBox) v;
                        if (aftercare[d].equals("1")) {
                            b.setChecked(true);
                        } else {
                            b.setChecked(false);
                        }
                    }
                }
            }
            Log.e("fast", slowfast[1]);
            String[] aftercare2 = slowfast[1].split("-");
            LinearLayout layout2 = (LinearLayout) findViewById(R.id.fastlanel);

            for (int d = 0; d < layout2.getChildCount(); d++) {
                LinearLayout la = (LinearLayout) layout2.getChildAt(d);
                Log.e("dapat ", " to" + d);
                for (int c = 0; c < la.getChildCount(); c++) {
                    View v = (View) la.getChildAt(c);
                    if (v instanceof CheckBox) {
                        CheckBox b = (CheckBox) v;
                        if (aftercare2[d].equals("1")) {
                            b.setChecked(true);
                        } else {
                            b.setChecked(false);
                        }
                    }
                }
            }
        }

        disableEditText();

    }

    public void disableEditText(){
        LinearLayout layout = (LinearLayout) findViewById(R.id.slowlanel);
        for (int d = 0; d < layout.getChildCount(); d++) {
            LinearLayout la = (LinearLayout) layout.getChildAt(d);
            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);
                if (v instanceof EditText) {
                    EditText s = (EditText) v;
                    s.setEnabled(false);
                }
            }
        }

        LinearLayout layout2 = (LinearLayout) findViewById(R.id.fastlanel);

        for (int d = 0; d < layout2.getChildCount(); d++) {
            LinearLayout la = (LinearLayout) layout2.getChildAt(d);
            Log.e("dapat ", " to" + d);
            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);
                if (v instanceof EditText) {
                    EditText s = (EditText) v;
                    s.setEnabled(false);
                }
            }
        }
    }

    public String getcontetn(String gets, int position) {
        String getz = "";
        String[] splitters = gets.split("~");

        String abc = splitters[position].replace("^", "#");
        String[] splitter = abc.split("#");

        if (splitter.length >= 2) {
            getz = splitter[1];
        } else {
            getz = "";
        }
        return getz.replace("Metres:", "");
    }

}
