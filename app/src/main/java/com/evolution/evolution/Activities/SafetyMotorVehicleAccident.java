package com.evolution.evolution.Activities;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.evolution.evolution.Activities.Forms.EmployeeDeclaratiion;
import com.evolution.evolution.Activities.Forms.IncidentBasic;
import com.evolution.evolution.Activities.Forms.IncidentDetails;
import com.evolution.evolution.Activities.Forms.IncidentLocality;
import com.evolution.evolution.Activities.Forms.NationalSafety;
import com.evolution.evolution.Adapters.AdapterIncidentReportForm;
import com.evolution.evolution.Helpers.RecyclerItemClickListener;
import com.evolution.evolution.R;
import com.rey.material.app.Dialog;

public class SafetyMotorVehicleAccident extends AppCompatActivity {
    Toolbar toolbar;
    String[] myDataset = {"Basic Information", "Locality Details / Vehicle Details", "Motor Vehicle Accident Details", "Injury and Medical Treatment Details(If you did not suffer any injury from the accident go to Section 41)", "Vehicle Damage Details", "Diagram of the Accident Scene", "Worker Declaration(must be and witnessed prior to forwarding to the ERMG safety Manager)", "Depot/Section Manager/Supervisor Report(This section must be completed prior to forwarding to the ERMG Safety Manager) If this report is being completed by a deport/section manager do not complete Section 44. "};

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_safety_motor_vehicle_accident);
        initToolBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_safety_motor_vehicle_accident, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:

                return true;
            case android.R.id.home:
                finish();
                overridePendingTransition(0, 0);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initRecycler();
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Motor Vehicle Accident Report");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initRecycler() {

        mRecyclerView = (RecyclerView) findViewById(R.id.motorvehicle);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Dialog.Builder builder = null;

                        switch (position) {
                            case 0:
                                Intent m = new Intent(getApplicationContext(), IncidentBasic.class);
                                m.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(m);
                                break;
                            case 1:
                                Intent m2 = new Intent(getApplicationContext(), IncidentLocality.class);
                                m2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(m2);
                                break;
                            case 2:
                                Intent m3 = new Intent(getApplicationContext(), IncidentDetails.class);
                                m3.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(m3);
                                break;
                            case 3:
                                Intent m4 = new Intent(getApplicationContext(), EmployeeDeclaratiion.class);
                                m4.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(m4);
                                break;
                            case 4:
                                Intent m5 = new Intent(getApplicationContext(), NationalSafety.class);
                                m5.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(m5);
                                break;
                        }
                    }
                })
        );

        mAdapter = new AdapterIncidentReportForm(myDataset, getApplicationContext());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(0, 0);
    }
}
