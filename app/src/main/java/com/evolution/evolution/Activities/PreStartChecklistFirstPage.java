package com.evolution.evolution.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.evolution.evolution.Adapters.AdapterPresiteChecklistFirstPage;
import com.evolution.evolution.Helpers.ConnectionDetector;
import com.evolution.evolution.Helpers.DatabaseHelper;
import com.evolution.evolution.Helpers.SessionManager;
import com.evolution.evolution.Models.Job;
import com.evolution.evolution.Models.Timesheet;
import com.evolution.evolution.R;
import com.google.gson.Gson;

import org.parceler.Parcels;

import java.util.ArrayList;

public class PreStartChecklistFirstPage extends AppCompatActivity {
    private static final String TAG = "PresiteChecklistFirstPa";
    private Toolbar toolbar;
    private RecyclerView mRecyclerView;
    private AdapterPresiteChecklistFirstPage mAdapter;
    public LinearLayoutManager mLayoutManager;

    ConnectionDetector connection;
    DatabaseHelper db;
    public SessionManager session;
    Job mJobObject;
    private String token;
    private ArrayList<Timesheet> timesheetmem = new ArrayList<Timesheet>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presite_checklist_first_page);
        initToolBar();

        session = new SessionManager(this);
        token = session.getToken();

        connection = new ConnectionDetector(this);
        db = new DatabaseHelper(getApplicationContext());
        Bundle extras = getIntent().getExtras();
        mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));

        initRecyclerView();
        getJobInfo();
    }

    @Override
    protected void onResume() {
        super.onResume();
        timesheetmem = db.getallDriverTimesheets(mJobObject.getJobID()).getTimesheets();
        mAdapter = new AdapterPresiteChecklistFirstPage(timesheetmem, getApplicationContext(), "", db.getallTimesheets(mJobObject.getJobID()), mJobObject);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                return true;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.fleetmembers);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pre Start Checklist");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void getJobInfo() {
        timesheetmem = db.getallDriverTimesheets(mJobObject.getJobID()).getTimesheets();
        Gson m = new Gson();
        Log.e(TAG, "getJobInfo: " + m.toJson(timesheetmem));
        mAdapter = new AdapterPresiteChecklistFirstPage(timesheetmem, getApplicationContext(), "", db.getallTimesheets(mJobObject.getJobID()), mJobObject);
        mRecyclerView.setAdapter(mAdapter);
    }


}
