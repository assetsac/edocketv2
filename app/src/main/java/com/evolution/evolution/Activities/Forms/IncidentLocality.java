package com.evolution.evolution.Activities.Forms;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.evolution.evolution.Helpers.DateHelper;
import com.evolution.evolution.Helpers.FormValidation;
import com.evolution.evolution.R;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rey.material.app.DatePickerDialog;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.TimePickerDialog;

import java.text.DateFormat;

public class IncidentLocality extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    MaterialEditText timecomplitionform, datecomplitionform, incidentlocation, incidentclient;
    ImageButton setDate, setTime;
    DateHelper dates;
    Dialog.Builder builder = null;
    Dialog.Builder timebuilder = null;

    FormValidation val;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incident_locality);
        initToolBar();
        initViews();
        dates = new DateHelper();
        val = new FormValidation(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_basic_information, menu);
        return true;
    }

    public void initToolBar() {

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Incident Locality Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public void initViews() {
        timecomplitionform = (MaterialEditText) findViewById(R.id.timecomplitionform);
        datecomplitionform = (MaterialEditText) findViewById(R.id.datecomplitionform);
        incidentlocation = (MaterialEditText) findViewById(R.id.incidentlocation);
        incidentclient = (MaterialEditText) findViewById(R.id.incidentclient);

        setTime = (ImageButton) findViewById(R.id.setTime);
        setDate = (ImageButton) findViewById(R.id.setDate);

        setTime.setOnClickListener(this);
        setDate.setOnClickListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_settings:

                val.validateSetError(timecomplitionform, "Please fill up time completion.");
                val.validateSetError(datecomplitionform, "Please fill up date completion.");
                val.validateSetError(incidentlocation, "Please fill up location.");
                val.validateSetError(incidentclient, "Please fill up client name.");

                if (val.validateSetError(timecomplitionform, "Please fill up time completion.") && val.validateSetError(datecomplitionform, "Please fill up date completion.") && val.validateSetError(incidentlocation, "Please fill up location.") && val.validateSetError(incidentclient, "Please fill up client name.")) {
                    Toast.makeText(getApplicationContext(), "Changes has been saved.", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
            case android.R.id.home:
                finish();
                overridePendingTransition(0, 0);
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.setTime: {
                timebuilder = new TimePickerDialog.Builder(6, 00) {
                    @Override
                    public void onPositiveActionClicked(DialogFragment fragment) {
                        TimePickerDialog dialog = (TimePickerDialog) fragment.getDialog();
                        timecomplitionform.setText(dialog.getFormattedTime(DateFormat.getTimeInstance()));
                        super.onPositiveActionClicked(fragment);
                        super.onNegativeActionClicked(fragment);
                    }
                };

                timebuilder.positiveAction("OK")
                        .negativeAction("CANCEL");

                DialogFragment fragment = DialogFragment.newInstance(timebuilder);
                fragment.show(getSupportFragmentManager(), null);
            }
            break;
            case R.id.setDate: {

                builder = new DatePickerDialog.Builder() {
                    @Override
                    public void onPositiveActionClicked(DialogFragment fragment) {
                        final DatePickerDialog dialog2 = (DatePickerDialog) fragment.getDialog();
                        datecomplitionform.setText(dates.miliSecondConverter(dialog2.getDate() + "", "dd, MMMM yyyy"));
                        super.onPositiveActionClicked(fragment);
                        super.onNegativeActionClicked(fragment);
                    }
                };

                builder.positiveAction("OK")
                        .negativeAction("CANCEL");

                DialogFragment fragment = DialogFragment.newInstance(builder);
                fragment.show(getSupportFragmentManager(), null);

            }
            break;
        }
    }
}
