package com.evolution.evolution.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TimePicker;
import android.widget.Toast;

import com.evolution.evolution.Helpers.CustomTimePickerDialog;
import com.evolution.evolution.Helpers.CustomTimePickerDialogSign;
import com.evolution.evolution.Helpers.DatabaseHelper;
import com.evolution.evolution.Helpers.DateHelper;
import com.evolution.evolution.Models.JobStatuses;
import com.evolution.evolution.Models.SingageAudit;
import com.evolution.evolution.Models.Job;
import com.evolution.evolution.Models.Timesheet;
import com.evolution.evolution.R;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.rey.material.app.TimePickerDialog;

import org.parceler.Parcels;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SignageAudit extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    Toolbar toolbar;
    LinearLayout signages;
    Job mJobObject;
    Spinner spinner, spinner2, spinner3;
    MaterialEditText cwlocallandmark4, TimeErected, TimeCollected, TimeChecked1, TimeChecked2, TimeChecked3, TimeChecked4;

    String Time1, Time2, Time3, Time4, Time5, Time6;
    String TimeErected2, TimeCollected2, TimeChecked12, TimeChecked22, TimeChecked32, TimeChecked42, lastItem;
    DateHelper date;

    List<String> categories;
    final CharSequence cs1 = "-";
    Switch ishaveSignageAudit;
    private ArrayList<Timesheet> timesheetmem = new ArrayList<Timesheet>();
    Dialog dialog;
    DatabaseHelper db;
    int whoisUsingTimepicter;
    LinearLayout ishaveSignaged;
    CustomTimePickerDialogSign timePic;

    private static final String TAG = "SignageAudit";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signage_audit);
        initToolBar();
        db = new DatabaseHelper(this);
        date = new DateHelper();
        signages = (LinearLayout) findViewById(R.id.signages);
        signages.setOnClickListener(this);
        mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        initViews();
        initSpinner();
        populateData();
        initTimeSpinner();
        if (db.checkJobstatus(mJobObject.getJobID())) {
            if (db.getJobStatusStats(mJobObject.getJobID()).getSingageAudit().equals("1")) {
                Log.e("true", "sdfsdfsdf");
                ishaveSignageAudit.setChecked(true);
            } else {
                Log.e("true", "sdfsdfsdf1");
                ishaveSignageAudit.setChecked(false);
                disAbleViews(false);
            }
        } else {
            ishaveSignageAudit.setChecked(true);
        }

    }

    public void initTimeSpinner() {
        Calendar c = Calendar.getInstance();
        timePic = new CustomTimePickerDialogSign(this, mTimeSetListener, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true);
        timePic.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {

                }
            }
        });
        timePic.setCancelable(false);
    }

    android.app.TimePickerDialog.OnTimeSetListener mTimeSetListener = new android.app.TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {
            switch (whoisUsingTimepicter) {
                case 1:
                    if (checkifhave(getTimeDifference(TimeChecked1.getText().toString(), addZero(hourOfDay + "", minute + "")))) {
                        ifOvernight(addZero(hourOfDay + "", minute + ""), 1, TimeErected);
                    } else {
                        TimeErected.setText(addZero(hourOfDay + "", minute + ""));
                        Time1 = convertDate(addZero(hourOfDay + "", minute + ""));
                    }
                    if (checkifhave(checkifBelow(TimeErected, TimeChecked1))) {
                        resetBelow(1);
                    }
                    break;
                case 3:
                    if (checkifhave(getTimeDifference(TimeErected.getText().toString(), addZero(hourOfDay + "", minute + "")))) {
                        ifOvernight(addZero(hourOfDay + "", minute + ""), 1, TimeChecked1);
                    } else {
                        TimeChecked1.setText(addZero(hourOfDay + "", minute + ""));
                        Time2 = convertDate(addZero(hourOfDay + "", minute + ""));
                    }

                    if (checkifhave(checkifBelow(TimeChecked1, TimeChecked2))) {
                        resetBelow(2);
                    }
                    break;
                case 4:
                    if (checkifhave(getTimeDifference(TimeChecked1.getText().toString(), addZero(hourOfDay + "", minute + "")))) {
                        ifOvernight(addZero(hourOfDay + "", minute + ""), 2, TimeChecked2);
                    } else {
                        TimeChecked2.setText(addZero(hourOfDay + "", minute + ""));
                        Time3 = convertDate(addZero(hourOfDay + "", minute + ""));
                    }

                    if (checkifhave(checkifBelow(TimeChecked2, TimeChecked3))) {
                        resetBelow(3);
                    }
                    break;
                case 5:
                    if (checkifhave(getTimeDifference(TimeChecked2.getText().toString(), addZero(hourOfDay + "", minute + "")))) {
                        ifOvernight(addZero(hourOfDay + "", minute + ""), 3, TimeChecked3);
                    } else {
                        TimeChecked3.setText(addZero(hourOfDay + "", minute + ""));
                        Time4 = convertDate(addZero(hourOfDay + "", minute + ""));
                    }
                    if (checkifhave(checkifBelow(TimeChecked3, TimeChecked4))) {
                        resetBelow(4);
                    }

                    break;
                case 6:
                    if (checkifhave(getTimeDifference(TimeChecked3.getText().toString(), addZero(hourOfDay + "", minute + "")))) {
                        ifOvernight(addZero(hourOfDay + "", minute + ""), 4, TimeChecked4);
                    } else {
                        TimeChecked4.setText(addZero(hourOfDay + "", minute + ""));
                        Time5 = convertDate(addZero(hourOfDay + "", minute + ""));
                    }
                    if (checkifhave(checkifBelow(TimeChecked4, TimeCollected))) {
                        resetBelow(5);
                    }
                    break;
                case 2:
                    if (checkifhave(getTimeDifference(TimeChecked4.getText().toString(), addZero(hourOfDay + "", minute + "")))) {
                        ifOvernight(addZero(hourOfDay + "", minute + ""), 5, TimeCollected);
                    } else {
                        TimeCollected.setText(addZero(hourOfDay + "", minute + ""));
                        Time6 = convertDate(addZero(hourOfDay + "", minute + ""));
                    }
                    break;
            }
        }
    };

    public String checkifBelow(MaterialEditText first, MaterialEditText second) {
        int calTwoData = 0;
        if (!second.getText().toString().isEmpty()) {
            Log.e("return data please", second.getText().toString().replace(":", "") + " return data" + "" + Integer.parseInt(first.getText().toString().replace(":", "")));
            calTwoData = Integer.parseInt(second.getText().toString().replace(":", "")) - Integer.parseInt(first.getText().toString().replace(":", ""));
        }

        return calTwoData + "";
    }

    public void resetBelow(final int position) {

        boolean isHaveValue = false;
        switch (position) {
            case 1:
                if (!TimeChecked1.getText().toString().isEmpty()) {
                    isHaveValue = true;
                }
                break;
            case 2:
                if (!TimeChecked2.getText().toString().isEmpty()) {
                    isHaveValue = true;
                }
                break;
            case 3:
                if (!TimeChecked3.getText().toString().isEmpty()) {
                    isHaveValue = true;
                }
                break;
            case 4:
                if (!TimeChecked4.getText().toString().isEmpty()) {
                    isHaveValue = true;
                }
                break;
            case 5:
                if (!TimeCollected.getText().toString().isEmpty()) {
                    isHaveValue = true;
                }
                break;
        }

        if (isHaveValue) {
            Dialog.Builder builder = null;
            builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                @Override
                public void onPositiveActionClicked(DialogFragment fragment) {
                    switch (position) {
                        case 1:
                            TimeChecked1.setText("");
                            TimeChecked2.setText("");
                            TimeChecked3.setText("");
                            TimeChecked4.setText("");
                            TimeCollected.setText("");
                            TimeChecked12 = "";
                            TimeChecked22 = "";
                            TimeChecked32 = "";
                            TimeChecked42 = "";
                            TimeCollected2 = "";
                            Time2 = null;
                            Time3 = null;
                            Time4 = null;
                            Time5 = null;
                            Time6 = null;
                            break;
                        case 2:
                            TimeChecked2.setText("");
                            TimeChecked3.setText("");
                            TimeChecked4.setText("");
                            TimeCollected.setText("");
                            TimeChecked22 = "";
                            TimeChecked32 = "";
                            TimeChecked42 = "";
                            TimeCollected2 = "";
                            Time3 = null;
                            Time4 = null;
                            Time5 = null;
                            Time6 = null;
                            break;
                        case 3:
                            TimeChecked3.setText("");
                            TimeChecked4.setText("");
                            TimeCollected.setText("");
                            TimeChecked32 = "";
                            TimeChecked42 = "";
                            TimeCollected2 = "";
                            Time4 = null;
                            Time5 = null;
                            Time6 = null;
                            break;
                        case 4:
                            TimeChecked4.setText("");
                            TimeCollected.setText("");
                            TimeChecked42 = "";
                            TimeCollected2 = "";
                            Time5 = null;
                            Time6 = null;
                            break;
                        case 5:
                            TimeCollected.setText("");
                            TimeCollected2 = "";
                            Time6 = null;
                            break;
                    }
                    super.onPositiveActionClicked(fragment);
                }

                @Override
                public void onNegativeActionClicked(DialogFragment fragment) {
                    super.onNegativeActionClicked(fragment);
                }
            };

            ((SimpleDialog.Builder) builder).message("Changing this will reset all the time values below. Proceed?")
                    .positiveAction("Ok")
                    .negativeAction("Cancel");
            DialogFragment fragment = DialogFragment.newInstance(builder);
            fragment.show(getSupportFragmentManager(), null);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    public void disAbleViews(boolean enableDisable) {
        JobStatuses ss = new JobStatuses();
        ss.setJobId(mJobObject.getJobID());

        cwlocallandmark4.setEnabled(enableDisable);
        TimeErected.setEnabled(enableDisable);
        TimeCollected.setEnabled(enableDisable);
        TimeChecked1.setEnabled(enableDisable);
        TimeChecked2.setEnabled(enableDisable);
        TimeChecked3.setEnabled(enableDisable);
        TimeChecked4.setEnabled(enableDisable);
        spinner.setEnabled(enableDisable);
        spinner2.setEnabled(enableDisable);
        spinner3.setEnabled(enableDisable);
        signages.setEnabled(enableDisable);


        if (enableDisable) {
            signages.setBackgroundColor(Color.parseColor("#1793cb"));
            ss.setSingageAudit("1");
        } else {
            signages.setBackgroundColor(Color.parseColor("#888888"));
            ss.setSingageAudit("0");
        }
        db.addJobStatus(ss);
    }

    public void initViews() {
        cwlocallandmark4 = (MaterialEditText) findViewById(R.id.cwlocallandmark4);
        TimeErected = (MaterialEditText) findViewById(R.id.TimeErected);
        TimeCollected = (MaterialEditText) findViewById(R.id.TimeCollected);
        TimeChecked1 = (MaterialEditText) findViewById(R.id.TimeChecked1);
        TimeChecked2 = (MaterialEditText) findViewById(R.id.TimeChecked2);
        TimeChecked3 = (MaterialEditText) findViewById(R.id.TimeChecked3);
        TimeChecked4 = (MaterialEditText) findViewById(R.id.TimeChecked4);
        ishaveSignageAudit = (Switch) findViewById(R.id.ishaveSignageAudit);
        ishaveSignaged = (LinearLayout) findViewById(R.id.ishaveSignaged);
        ishaveSignaged.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ishaveSignageAudit.isChecked()) {
                    disAbleViews(true);
                    ishaveSignageAudit.setChecked(true);
                    Log.e("asdasd", "s");
                    db.updateDockets("1", "null", "null", mJobObject.getJobID());
                    db.updateDockets("2", "null", "null", mJobObject.getJobID());
                } else {
                    disAbleViews(false);
                    ishaveSignageAudit.setChecked(false);

                    db.updateDockets("1", "null", "null", mJobObject.getJobID());
                    db.updateDockets("2", "null", "null", mJobObject.getJobID());

                }
            }
        });

        TimeErected.setOnTouchListener(this);
        TimeCollected.setOnTouchListener(this);

        TimeChecked1.setOnTouchListener(this);
        TimeChecked2.setOnTouchListener(this);
        TimeChecked3.setOnTouchListener(this);
        TimeChecked4.setOnTouchListener(this);

        spinner = (Spinner) findViewById(R.id.cwdirectionlist4);
        spinner2 = (Spinner) findViewById(R.id.erectedby);
        spinner3 = (Spinner) findViewById(R.id.collectedby);

        erec(spinner2);
        erec(spinner3);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void populateData() {
        ArrayList<SingageAudit> signage = db.getAllSignage(mJobObject.getJobID());
        if (!(signage.size() <= 0)) {
            if (!(signage.get(0).getLandmark() == null)) {
                cwlocallandmark4.setText(signage.get(0).getLandmark());
            }
            if (!(signage.get(0).getTimeErected() == null)) {
                TimeErected.setText(date.miliSecondConverter(signage.get(0).getTimeErected(), "HH:mm"));
                lastItem = date.miliSecondConverter(signage.get(0).getTimeErected(), "MMM dd, yyyy hh:mm:ss a");
            }
            if (!(signage.get(0).getTimeChecked1() == null)) {
                TimeChecked1.setText(date.miliSecondConverter(signage.get(0).getTimeChecked1(), "HH:mm"));
                lastItem = date.miliSecondConverter(signage.get(0).getTimeChecked1(), "MMM dd, yyyy hh:mm:ss a");
            }
            if (!(signage.get(0).getTimeChecked2() == null)) {
                TimeChecked2.setText(date.miliSecondConverter(signage.get(0).getTimeChecked2(), "HH:mm"));
                lastItem = date.miliSecondConverter(signage.get(0).getTimeChecked2(), "MMM dd, yyyy hh:mm:ss a");
            }
            if (!(signage.get(0).getTimeChecked3() == null)) {
                TimeChecked3.setText(date.miliSecondConverter(signage.get(0).getTimeChecked3(), "HH:mm"));
                lastItem = date.miliSecondConverter(signage.get(0).getTimeChecked3(), "MMM dd, yyyy hh:mm:ss a");
            }
            if (!(signage.get(0).getTimeChecked4() == null)) {
                TimeChecked4.setText(date.miliSecondConverter(signage.get(0).getTimeChecked4(), "HH:mm"));
                lastItem = date.miliSecondConverter(signage.get(0).getTimeChecked4(), "MMM dd, yyyy hh:mm:ss a");
            }
            if (!(signage.get(0).getTimeCollected() == null)) {
                TimeCollected.setText(date.miliSecondConverter(signage.get(0).getTimeCollected(), "HH:mm"));
            }

            Time1 = signage.get(0).getTimeErected();
            Time6 = signage.get(0).getTimeCollected();
            Time2 = signage.get(0).getTimeChecked1();
            Time3 = signage.get(0).getTimeChecked2();
            Time4 = signage.get(0).getTimeChecked3();
            Time5 = signage.get(0).getTimeChecked4();

            spinner.setSelection(Integer.parseInt(signage.get(0).getDirection()), true);
            spinner2.setSelection(Integer.parseInt(signage.get(0).getErectedBy()), true);
            spinner3.setSelection(Integer.parseInt(signage.get(0).getCollectedBy()), true);
            TimeErected2 = signage.get(0).getTimeErected();
            TimeCollected2 = signage.get(0).getTimeCollected();
            TimeChecked12 = signage.get(0).getTimeChecked1();
            TimeChecked22 = signage.get(0).getTimeChecked2();
            TimeChecked32 = signage.get(0).getTimeChecked3();
            TimeChecked42 = signage.get(0).getTimeChecked4();
        }
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Signage Audit");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signages: {
                db.addSignage(addSignAge("1"));
                db.addSignage(addSignAge("2"));
                Intent s = new Intent(SignageAudit.this, Signages.class);
                s.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                s.putExtra("data", Parcels.wrap(mJobObject));
                startActivity(s);
                break;
            }
        }
    }

    public void initSpinner() {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // On selecting a spinner item
                String item = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        categories = new ArrayList<String>();
        categories.add("North");
        categories.add("South");
        categories.add("East");
        categories.add("West");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnerlocation, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.spinnerlocation);
        spinner.setAdapter(dataAdapter);

    }

    public void erec(Spinner spinnerk) {

        spinnerk.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        timesheetmem = db.getallTimesheets(mJobObject.getJobID()).getTimesheets();
        categories = new ArrayList<String>();
        for (int i = 0; i < timesheetmem.size(); i++) {
            categories.add(timesheetmem.get(i).getName());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnerlocation, categories);
        dataAdapter.setDropDownViewResource(R.layout.spinnerlocation);
        spinnerk.setAdapter(dataAdapter);
    }

    public String addZero(String n, String c) {
        String re = "";
        String re2 = "";
        if (n.length() == 1) {
            re = "0" + n;
        } else {
            re = n;
        }
        if (c.length() == 1) {
            re2 = "0" + c;
        } else {
            re2 = c;
        }
        return re + ":" + re2;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        initTimeSpinner();
        switch (view.getId()) {
            case R.id.TimeErected: {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    timePic.show();
                    whoisUsingTimepicter = 1;
                }
                return true;
            }
            case R.id.TimeCollected:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeErected.getText().toString().equals("")) {
                        Toast.makeText(SignageAudit.this, "Please add Time Erected.", Toast.LENGTH_SHORT).show();
                    } else {
                        timePic.show();
                        whoisUsingTimepicter = 2;
                    }
                }
                return true;
            case R.id.TimeChecked1:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeErected.getText().toString().equals("")) {
                        Toast.makeText(SignageAudit.this, "Please add Time Erected.", Toast.LENGTH_SHORT).show();
                    } else {
                        timePic.show();
                        whoisUsingTimepicter = 3;
                    }
                }
                return true;
            case R.id.TimeChecked2:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked1.getText().toString().equals("")) {
                        Toast.makeText(SignageAudit.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        timePic.show();
                        whoisUsingTimepicter = 4;
                    }
                }
                return true;
            case R.id.TimeChecked3:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked2.getText().toString().equals("")) {
                        Toast.makeText(SignageAudit.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        timePic.show();
                        whoisUsingTimepicter = 5;
                    }
                }
                return true;
            case R.id.TimeChecked4:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked3.getText().toString().equals("")) {
                        Toast.makeText(SignageAudit.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        timePic.show();
                        whoisUsingTimepicter = 6;
                    }
                }
                return true;
        }
        return false;
    }

    public boolean checkifhave(String toValidate) {
        boolean temp = false;
        boolean ishave = toValidate.contains(cs1);
        if (ishave) {
            temp = true;
        }
        return temp;
    }

    public String getTimeDifference(String first, String second) {
        long diffMinutes = 0;
        long diffHours = 0;
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse(first);
            d2 = format.parse(second);

            long diff = d2.getTime() - d1.getTime();
            diffMinutes = diff / (60 * 1000) % 60;
            diffHours = diff / (60 * 60 * 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return diffHours + "h  " + diffMinutes + "m";
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_qldsignages, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.saveqld:
                db.addSignage(addSignAge("1"));
                db.addSignage(addSignAge("2"));
                Toast.makeText(SignageAudit.this, "Changes have been saved.", Toast.LENGTH_SHORT).show();
                db.updateDockets("1", "null", "null", mJobObject.getJobID());
                db.updateDockets("2", "null", "null", mJobObject.getJobID());
                break;
            case android.R.id.home:
                finish();
                overridePendingTransition(0, 0);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public SingageAudit addSignAge(String CarriageWay) {
        SingageAudit signInfo = new SingageAudit();
        signInfo.setJobID(mJobObject.getJobID());
        signInfo.setLandmark(cwlocallandmark4.getText().toString());
        signInfo.setCarriageWay(CarriageWay);
        signInfo.setDirection(spinner.getSelectedItemId() + "");
        signInfo.setErectedBy(spinner2.getSelectedItemId() + "");
        signInfo.setCollectedBy(spinner3.getSelectedItemId() + "");
        signInfo.setTimeErected(Time1);
        signInfo.setTimeCollected(Time6);
        signInfo.setTimeChecked1(Time2);
        signInfo.setTimeChecked2(Time3);
        signInfo.setTimeChecked3(Time4);
        signInfo.setTimeChecked4(Time5);
        return signInfo;
    }


    public String convertDate(String time) {
        String temp = "";
        Log.e("testsetse", time);
        long timeInMilliseconds = 0;
        String givenDateString = time;
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds + "";
    }

    public void ifOvernight(final String dialogr, final int kanino, final MaterialEditText editext) {
        Dialog.Builder builder = null;
        builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                super.onPositiveActionClicked(fragment);
                switch (kanino) {
                    case 1:
                        Time2 = convertDate(dialogr);
                        break;
                    case 2:
                        Time3 = convertDate(dialogr);
                        break;
                    case 3:
                        Time4 = convertDate(dialogr);
                        break;
                    case 4:
                        Time5 = convertDate(dialogr);
                        break;
                    case 5:
                        Time6 = convertDate(dialogr);
                        break;
                }
                editext.setText(dialogr);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
                switch (kanino) {
                    case 1:
                        TimeChecked12 = "";
                        break;
                    case 2:
                        TimeChecked22 = "";
                        break;
                    case 3:
                        TimeChecked32 = "";
                        break;
                    case 4:
                        TimeChecked42 = "";
                        break;
                    case 5:
                        TimeCollected2 = "";
                        break;
                }
                Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
            }
        };

        ((SimpleDialog.Builder) builder).message("Night shift job? ")
                .positiveAction("YES")
                .negativeAction("NO");

        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(), null);
    }


    public void HideKeyboard(View view) {
        hideKeyboard();
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}