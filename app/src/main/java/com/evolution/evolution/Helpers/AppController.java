package com.evolution.evolution.Helpers;

import android.annotation.TargetApi;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.media.RingtoneManager;
import android.os.Build;
import android.view.Display;
import android.widget.Toast;

import com.evolution.evolution.Activities.MainPage;
import com.evolution.evolution.Interface.APIInterface;
import com.evolution.evolution.R;
import com.squareup.okhttp.OkHttpClient;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

@ReportsCrashes(
        formUri = "http://straightarrowasset.com/android_crashreport/acra.php",
        formUriBasicAuthLogin = "your username",
        formUriBasicAuthPassword = "your password",
        reportType = org.acra.sender.HttpSender.Type.JSON,
        sendReportsAtShutdown = false
)

/**
 * Created by mcgwapo on 2/9/16.
 */
public class AppController extends Application {
    public static final String TAG = AppController.class.getSimpleName();
    private static AppController mInstance;
    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public APIInterface sacInterface;
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        ACRA.init(this);
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(100, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(100, TimeUnit.SECONDS);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_URL)
                .setClient(new OkClient(okHttpClient))
                .build();
        sacInterface = restAdapter.create(APIInterface.class);
    }

    public APIInterface getBoardsAPI() {
        return sacInterface;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void notification(String message, String jobid, boolean isStart) {
        PendingIntent pIntent;
        Intent intent = new Intent(mInstance, MainPage.class);
        pIntent = PendingIntent.getActivity(mInstance, 0,
                intent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        if (isStart) {
            Notification n;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                n = new Notification.Builder(mInstance)
                        .setContentTitle("eDocket Video")
                        .setContentText("Video is now uploading..")
                        .setSmallIcon(R.drawable.ic_stat_ic_launcher)
                        .setColor(mInstance.getResources().getColor(R.color.colorPrimary))
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setContentIntent(pIntent)
                        .setProgress(100, 0, true)
                        .setAutoCancel(true).build();

            } else {
                n = new Notification.Builder(mInstance)
                        .setContentTitle("eDocket Video")
                        .setContentText("Video is now uploading..")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setContentIntent(pIntent)
                        .setProgress(100, 0, true)
                        .setAutoCancel(true).build();
            }

            NotificationManager notificationManager =
                    (NotificationManager) mInstance.getSystemService(NOTIFICATION_SERVICE);

            notificationManager.notify(0, n);
        } else {
// build notification
// the addAction re-use the same intent to keep the example short
            Notification n;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                n = new Notification.Builder(mInstance)
                        .setContentTitle("Job #" + jobid + " Video")
                        .setContentText(message)
                        .setSmallIcon(R.drawable.ic_stat_ic_launcher)
                        .setColor(mInstance.getResources().getColor(R.color.colorPrimary))
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setContentIntent(pIntent)
                        .setAutoCancel(true).build();
            } else {
                n = new Notification.Builder(mInstance)
                        .setContentTitle("Job #" + jobid + " Video")
                        .setContentText(message)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setContentIntent(pIntent)
                        .setAutoCancel(true).build();
            }
            NotificationManager notificationManager =
                    (NotificationManager) mInstance.getSystemService(NOTIFICATION_SERVICE);

            notificationManager.notify(0, n);
        }

    }

}
