package com.evolution.evolution.Helpers;

import android.content.Context;
import android.widget.EditText;

/**
 * Created by mcclynreyarboleda on 5/1/15.
 */
public class FormValidation {

    private Context mContext;

    public FormValidation(Context context) {
        this.mContext = context;
    }


    public boolean validatePassword(EditText editText) {
        boolean status;
        if(!isEmpty(editText)) {
            if(isValidLengthPassword(editText, 5, 25)){
                status =  true;
            }else {
                editText.setError("5 minimum, 25 max password required");
                status = false;
            }
        }else {
            editText.setError("Please insert your password.");
            status = false;
        }
        return status;
    }

    public boolean validateUsername(EditText editText) {
        boolean status;
        if(!isEmpty(editText)) {
            if(isValidLengthPassword(editText, 3, 25)){
                status =  true;
            }else {
                editText.setError("3 minimum, 25 max username required");
                status = false;
            }
        }else {
            editText.setError("Please insert your username.");
            status = false;
        }
        return status;
    }

    public boolean validateSetError(EditText edittext, String error){
        boolean temp;
        if(isEmpty(edittext)){
            edittext.setError(error);
            temp = false;
        }else{
            temp = true;
        }
        return temp;
    }

    public boolean isEmpty(EditText edittext)
    {
        return edittext.getText().toString().length() <= 0;
    }

    public boolean isValidLengthPassword(EditText edittext, int i, int j)
    {
        return edittext.getText().toString().length() >= i && edittext.getText().toString().length() <= j;
    }


}
