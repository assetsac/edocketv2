package com.evolution.evolution.Helpers;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

/**
 * Created by mcarboleda on 18/03/2016.
 */
public class BacgroundService extends Service {
    private boolean isRunning;
    private Context context;
    private Thread backgroundThread;

    private SessionManager session;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.context = this;
        this.isRunning = false;
        this.backgroundThread = new Thread(myTask);
        this.session = new SessionManager(this);
    }

    private Runnable myTask = new Runnable() {
        @Override
        public void run() {

            LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            session.createLocaltionData(getGPS());
            Log.e("Sytem", "test mac" + session.getlocation() + "");
            stopSelf();
        }
    };

    private String getGPS() {
        String location = "";
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = lm.getProviders(true);
        Location l = null;

        for (int i = providers.size() - 1; i >= 0; i--) {
            l = lm.getLastKnownLocation(providers.get(i));
            if (l != null) break;
        }
        double[] gps = new double[2];
        if (l != null) {
            gps[0] = l.getLatitude();
            gps[1] = l.getLongitude();
            location = l.getLatitude() + ", " + l.getLongitude();
        } else {
            location = "-27.4996872,153.0354553";
        }
        return location;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        this.isRunning = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!this.isRunning) {
            this.isRunning = true;
            this.backgroundThread.start();
        }
        return START_STICKY;
    }
}