package com.evolution.evolution.Models;

import org.parceler.Parcel;

/**
 * Created by mcarboleda on 23/11/2016.
 */

@Parcel
public class ModelPreSiteChecklistCarParts {

    String _id;
    String JobID;
    String Item;
    String Notes;
    String Row;
    String Column;
    String View;
    String ContactID;

    public ModelPreSiteChecklistCarParts() {
    }

    public ModelPreSiteChecklistCarParts(String _id, String jobID, String item, String notes, String row, String column, String view, String contactID) {
        this._id = _id;
        JobID = jobID;
        Item = item;
        Notes = notes;
        Row = row;
        Column = column;
        View = view;
        ContactID = contactID;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getJobID() {
        return JobID;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public String getItem() {
        return Item;
    }

    public void setItem(String item) {
        Item = item;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public String getRow() {
        return Row;
    }

    public void setRow(String row) {
        Row = row;
    }

    public String getColumn() {
        return Column;
    }

    public void setColumn(String column) {
        Column = column;
    }

    public String getView() {
        return View;
    }

    public void setView(String view) {
        View = view;
    }

    public String getContactID() {
        return ContactID;
    }

    public void setContactID(String contactID) {
        ContactID = contactID;
    }
}