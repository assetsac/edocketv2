package com.evolution.evolution.Models;

import org.parceler.Parcel;

/**
 * Created by mcclynreyarboleda on 4/15/15.
 */
@Parcel
public class SubJob {

    public SubJob() {

    }

    String Id;
    String Code;
    String JobId;
    String AdditionalNotes;
    String AuthorizerID;
    String ContactID;
    String DepotID;
    String JobCode;
    String EndDateTime,
            ForemanID,
            OperatorID,
            OrderNumber,
            ParentJobID,
            StartDateTime,
            StartType,
            ContactPerson,
            AuthorizedPerson,
            Address,
            Depot,
            CompanyName,
            StartDateTimeFormat;
    String EndDateTimeFormat;
    String State;
    String ParentJob;
    String Status;

    public SubJob(String id,
                  String code,
                  String jobId,
                  String additionalNotes,
                  String authorizerID,
                  String contactID,
                  String depotID,
                  String jobCode,
                  String endDateTime,
                  String foremanID,
                  String operatorID,
                  String orderNumber,
                  String parentJobID,
                  String startDateTime,
                  String startType,
                  String contactPerson,
                  String authorizedPerson,
                  String address,
                  String depot,
                  String companyName,
                  String startDateTimeFormat,
                  String endDateTimeFormat,
                  String state,
                  String ParentJob,
                  String Status
    ) {

        Id = id;
        Code = code;
        JobId = jobId;
        AdditionalNotes = additionalNotes;
        AuthorizerID = authorizerID;
        ContactID = contactID;
        DepotID = depotID;
        JobCode = jobCode;
        EndDateTime = endDateTime;
        ForemanID = foremanID;
        OperatorID = operatorID;
        OrderNumber = orderNumber;
        ParentJobID = parentJobID;
        StartDateTime = startDateTime;
        StartType = startType;
        ContactPerson = contactPerson;
        AuthorizedPerson = authorizedPerson;
        Address = address;
        Depot = depot;
        CompanyName = companyName;
        StartDateTimeFormat = startDateTimeFormat;
        EndDateTimeFormat = endDateTimeFormat;
        State = state;
        ParentJob = ParentJob;
        Status = Status;

    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getJobId() {
        return JobId;
    }

    public void setJobId(String jobId) {
        JobId = jobId;
    }

    public String getAdditionalNotes() {
        return AdditionalNotes;
    }

    public void setAdditionalNotes(String additionalNotes) {
        AdditionalNotes = additionalNotes;
    }

    public String getAuthorizerID() {
        return AuthorizerID;
    }

    public void setAuthorizerID(String authorizerID) {
        AuthorizerID = authorizerID;
    }

    public String getContactID() {
        return ContactID;
    }

    public void setContactID(String contactID) {
        ContactID = contactID;
    }

    public String getDepotID() {
        return DepotID;
    }

    public void setDepotID(String depotID) {
        DepotID = depotID;
    }

    public String getJobCode() {
        return JobCode;
    }

    public void setJobCode(String jobCode) {
        JobCode = jobCode;
    }

    public String getEndDateTime() {
        return EndDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        EndDateTime = endDateTime;
    }

    public String getForemanID() {
        return ForemanID;
    }

    public void setForemanID(String foremanID) {
        ForemanID = foremanID;
    }

    public String getOperatorID() {
        return OperatorID;
    }

    public void setOperatorID(String operatorID) {
        OperatorID = operatorID;
    }

    public String getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        OrderNumber = orderNumber;
    }

    public String getParentJobID() {
        return ParentJobID;
    }

    public void setParentJobID(String parentJobID) {
        ParentJobID = parentJobID;
    }

    public String getStartDateTime() {
        return StartDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        StartDateTime = startDateTime;
    }

    public String getStartType() {
        return StartType;
    }

    public void setStartType(String startType) {
        StartType = startType;
    }

    public String getContactPerson() {
        return ContactPerson;
    }

    public void setContactPerson(String contactPerson) {
        ContactPerson = contactPerson;
    }

    public String getAuthorizedPerson() {
        return AuthorizedPerson;
    }

    public void setAuthorizedPerson(String authorizedPerson) {
        AuthorizedPerson = authorizedPerson;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getDepot() {
        return Depot;
    }

    public void setDepot(String depot) {
        Depot = depot;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getStartDateTimeFormat() {
        return StartDateTimeFormat;
    }

    public void setStartDateTimeFormat(String startDateTimeFormat) {
        StartDateTimeFormat = startDateTimeFormat;
    }

    public String getEndDateTimeFormat() {
        return EndDateTimeFormat;
    }

    public void setEndDateTimeFormat(String endDateTimeFormat) {
        EndDateTimeFormat = endDateTimeFormat;
    }

    public String getParentJob() {
        return ParentJob;
    }

    public void setParentJob(String parentJob) {
        ParentJob = parentJob;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}