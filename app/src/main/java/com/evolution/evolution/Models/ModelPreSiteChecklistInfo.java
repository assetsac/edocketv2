package com.evolution.evolution.Models;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by mcarboleda on 23/11/2016.
 */
@Parcel
public class ModelPreSiteChecklistInfo {

     String _id;
     String JobID;
     String ContactID;
     String VehicleRego;
     String VehicleType;
     String DepotPIV;
     String ServiceDueKms;
     String RetensionWheelNuts;
     String Odometer;
     ArrayList<ModelPreSiteChecklistCarParts> CheckListItems;

    public ModelPreSiteChecklistInfo() {
    }

    public ModelPreSiteChecklistInfo(String _id, String jobID, String contactID, String vehicleRego, String vehicleType, String PIV, String serviceDueKms, String retensionWheelNuts, String speedoStart, String speedoFinish, ArrayList<ModelPreSiteChecklistCarParts> checkListItems) {
        this._id = _id;
        JobID = jobID;
        ContactID = contactID;
        VehicleRego = vehicleRego;
        VehicleType = vehicleType;
        this.DepotPIV = PIV;
        ServiceDueKms = serviceDueKms;
        RetensionWheelNuts = retensionWheelNuts;
        Odometer = speedoStart;
        CheckListItems = checkListItems;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getJobID() {
        return JobID;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public String getContactID() {
        return ContactID;
    }

    public void setContactID(String contactID) {
        ContactID = contactID;
    }

    public String getVehicleRego() {
        return VehicleRego;
    }

    public void setVehicleRego(String vehicleRego) {
        VehicleRego = vehicleRego;
    }

    public String getVehicleType() {
        return VehicleType;
    }

    public void setVehicleType(String vehicleType) {
        VehicleType = vehicleType;
    }

    public String getDepotPIV() {
        return DepotPIV;
    }

    public void setDepotPIV(String depotPIV) {
        this.DepotPIV = depotPIV;
    }

    public String getServiceDueKms() {
        return ServiceDueKms;
    }

    public void setServiceDueKms(String serviceDueKms) {
        ServiceDueKms = serviceDueKms;
    }

    public String getRetensionWheelNuts() {
        return RetensionWheelNuts;
    }

    public void setRetensionWheelNuts(String retensionWheelNuts) {
        RetensionWheelNuts = retensionWheelNuts;
    }

    public String getOdometer() {
        return Odometer;
    }

    public void setOdometer(String odometer) {
        Odometer = odometer;
    }

    public ArrayList<ModelPreSiteChecklistCarParts> getCheckListItems() {
        return CheckListItems;
    }

    public void setCheckListItems(ArrayList<ModelPreSiteChecklistCarParts> checkListItems) {
        CheckListItems = checkListItems;
    }
}