package com.evolution.evolution.Models;

/**
 * Created by mcclynreyarboleda on 5/12/15.
 */
public class FleetSignature {

    String id, name, signatureURL;


    public FleetSignature() {

    }

    public FleetSignature(String id, String name, String signatureURL) {
        this.id = id;
        this.name = name;
        this.signatureURL = signatureURL;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSignatureURL() {
        return signatureURL;
    }

    public void setSignatureURL(String signatureURL) {
        this.signatureURL = signatureURL;
    }
}
