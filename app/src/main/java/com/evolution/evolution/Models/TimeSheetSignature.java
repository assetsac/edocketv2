package com.evolution.evolution.Models;

/**
 * Created by mcgwapo on 2/18/16.
 */
public class TimeSheetSignature {
    String contactID, JobID, name, signature;

    public TimeSheetSignature() {
    }

    public TimeSheetSignature(String signature, String name, String jobID, String contactID) {
        this.signature = signature;
        this.name = name;
        JobID = jobID;
        this.contactID = contactID;
    }

    public String getContactID() {
        return contactID;
    }

    public void setContactID(String contactID) {
        this.contactID = contactID;
    }

    public String getJobID() {
        return JobID;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
