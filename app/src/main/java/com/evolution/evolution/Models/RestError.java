package com.evolution.evolution.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mcgwapo on 2/5/16.
 */
public class RestError {
    @SerializedName("getStatus")
    public int getStatus;
    @SerializedName("error")
    public String errorDetails;

}
