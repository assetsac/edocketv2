package com.evolution.evolution.Models;

import org.parceler.Parcel;

/**
 * Created by mcarboleda on 03/03/2017.
 */

@Parcel
public class RosterStatus {

    String JobID;
    boolean Rostered;

    public RosterStatus() {

    }

    public RosterStatus(String jobID, boolean rostered) {
        JobID = jobID;
        Rostered = rostered;
    }

    public String getJobID() {
        return JobID;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public boolean isRostered() {
        return Rostered;
    }

    public void setRostered(boolean rostered) {
        Rostered = rostered;
    }
}
