package com.evolution.evolution.Models;

import org.parceler.Parcel;

@Parcel
public class Notes {
    String status, note;

    public Notes() {
    }

    public Notes(String status, String note) {
        this.status = status;
        this.note = note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

}
