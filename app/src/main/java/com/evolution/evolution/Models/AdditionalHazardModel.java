package com.evolution.evolution.Models;

import org.parceler.Parcel;

/**
 * Created by mcgwapo on 1/27/16.
 */

@Parcel
public class AdditionalHazardModel {
    String anyAdditionalhazard, initialrish, controlmeasures, residualrisk;

    public AdditionalHazardModel() {
    }

    public AdditionalHazardModel(String anyAdditionalhazard, String initialrish, String controlmeasures, String residualrisk) {
        this.anyAdditionalhazard = anyAdditionalhazard;
        this.initialrish = initialrish;
        this.controlmeasures = controlmeasures;
        this.residualrisk = residualrisk;
    }

    public String getAnyAdditionalhazard() {
        return anyAdditionalhazard;
    }

    public void setAnyAdditionalhazard(String anyAdditionalhazard) {
        this.anyAdditionalhazard = anyAdditionalhazard;
    }

    public String getInitialrish() {
        return initialrish;
    }

    public void setInitialrish(String initialrish) {
        this.initialrish = initialrish;
    }

    public String getControlmeasures() {
        return controlmeasures;
    }

    public void setControlmeasures(String controlmeasures) {
        this.controlmeasures = controlmeasures;
    }

    public String getResidualrisk() {
        return residualrisk;
    }

    public void setResidualrisk(String residualrisk) {
        this.residualrisk = residualrisk;
    }
}
