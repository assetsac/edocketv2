package com.evolution.evolution.Models;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by mcclynreyarboleda on 5/29/15.
 */
@Parcel
public class TimeSheetObjects {
    @SerializedName("employees")
    ArrayList<Timesheet> timesheets;

    public TimeSheetObjects() {
    }

    public TimeSheetObjects(ArrayList<Timesheet> timesheets) {
        this.timesheets = timesheets;
    }

    public ArrayList<Timesheet> getTimesheets() {
        return timesheets;
    }

    public void setTimesheets(ArrayList<Timesheet> timesheets) {
        this.timesheets = timesheets;
    }
}
