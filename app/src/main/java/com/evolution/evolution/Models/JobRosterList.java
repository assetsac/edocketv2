package com.evolution.evolution.Models;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by mcarboleda on 03/03/2017.
 */
@Parcel
public class JobRosterList {
    @SerializedName("jobList")
    ArrayList<RosterStatus> jobList;

    public JobRosterList() {
    }

    public JobRosterList(ArrayList<RosterStatus> jobList) {
        this.jobList = jobList;
    }

    public ArrayList<RosterStatus> getJobList() {
        return jobList;
    }

    public void setJobList(ArrayList<RosterStatus> jobList) {
        this.jobList = jobList;
    }
}
