package com.evolution.evolution.Models;

import org.parceler.Parcel;

/**
 * Created by mcclynreyarboleda on 6/2/15.
 */
@Parcel
public class CheckListItemOptions {
    String Id, Value;

    public CheckListItemOptions() {

    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
