package com.evolution.evolution.Models;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by mcclynreyarboleda on 5/29/15.
 */
@Parcel
public class JobObjects {

    @SerializedName("jobData")
    ArrayList<Job> mJob;

    @SerializedName("siteChecklist")
    ArrayList<CheckList> checkList;

    public JobObjects() {

    }

    public ArrayList<CheckList> getCheckList() {
        return checkList;
    }

    public void setCheckList(ArrayList<CheckList> checkList) {
        this.checkList = checkList;
    }

    public JobObjects(ArrayList<Job> mJob) {
        this.mJob = mJob;
    }

    public ArrayList<Job> getmJob() {
        return this.mJob;
    }

    public void setmJob(ArrayList<Job> mJob) {
        this.mJob = mJob;
    }

}
