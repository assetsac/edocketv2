package com.evolution.evolution.Models;

/**
 * Created by mcarboleda on 13/05/2016.
 */
public class JobStatuses {

    String JobId, singageAudit, awayjob;

    public JobStatuses() {

    }

    public JobStatuses(String jobId, String singageAudit, String awayjob) {
        JobId = jobId;
        this.singageAudit = singageAudit;
        this.awayjob = awayjob;
    }

    public String getJobId() {
        return JobId;
    }

    public void setJobId(String jobId) {
        JobId = jobId;
    }

    public String getSingageAudit() {
        return singageAudit;
    }

    public void setSingageAudit(String singageAudit) {
        this.singageAudit = singageAudit;
    }

    public String getAwayjob() {
        return awayjob;
    }

    public void setAwayjob(String awayjob) {
        this.awayjob = awayjob;
    }

}
