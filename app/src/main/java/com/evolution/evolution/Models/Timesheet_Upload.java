package com.evolution.evolution.Models;

import org.parceler.Parcel;

/**
 * Created by mcgwapo on 2/12/16.
 */
@Parcel
public class Timesheet_Upload {

    String JobID,
            WorkerID,
            ShiftID,
            TravelStartDateTime,
            TravelStartDateTimeEnd,
            TravelStartDistance,
            JobStartDateTime,
            JobFinishDateTime,
            BreakStartDateTime,
            BreakFinishDateTime,
            TravelFinishDateTimeStart,
            TravelFinishDateTime,
            TravelFinishDistance,
            FatigueCompliance,
            AttendDepot,
            RegoNo,
            Odometer,
            TraveledKilometers, isHaveMeal, TraveledKilometersRateB;

    public Timesheet_Upload() {

    }

    public Timesheet_Upload(String jobID, String workerID, String shiftID, String travelStartDateTime, String travelStartDateTimeEnd, String travelStartDistance, String jobStartDateTime, String jobFinishDateTime, String breakStartDateTime, String breakFinishDateTime, String travelFinishDateTimeStart, String travelFinishDateTime, String travelFinishDistance, String fatigueCompliance, String attendDepot, String regoNo, String odometer, String traveledKilometers, String isHaveMeal, String traveledKilometersRate) {
        JobID = jobID;
        WorkerID = workerID;
        ShiftID = shiftID;
        TravelStartDateTime = travelStartDateTime;
        TravelStartDateTimeEnd = travelStartDateTimeEnd;
        TravelStartDistance = travelStartDistance;
        JobStartDateTime = jobStartDateTime;
        JobFinishDateTime = jobFinishDateTime;
        BreakStartDateTime = breakStartDateTime;
        BreakFinishDateTime = breakFinishDateTime;
        TravelFinishDateTimeStart = travelFinishDateTimeStart;
        TravelFinishDateTime = travelFinishDateTime;
        TravelFinishDistance = travelFinishDistance;
        FatigueCompliance = fatigueCompliance;
        AttendDepot = attendDepot;
        RegoNo = regoNo;
        Odometer = odometer;
        TraveledKilometers = traveledKilometers;
        this.isHaveMeal = isHaveMeal;
        TraveledKilometersRateB = traveledKilometersRate;
    }

    public String getJobID() {
        return JobID;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public String getWorkerID() {
        return WorkerID;
    }

    public void setWorkerID(String workerID) {
        WorkerID = workerID;
    }

    public String getShiftID() {
        return ShiftID;
    }

    public void setShiftID(String shiftID) {
        ShiftID = shiftID;
    }

    public String getTravelStartDateTime() {
        return TravelStartDateTime;
    }

    public void setTravelStartDateTime(String travelStartDateTime) {
        TravelStartDateTime = travelStartDateTime;
    }

    public String getTravelStartDateTimeEnd() {
        return TravelStartDateTimeEnd;
    }

    public void setTravelStartDateTimeEnd(String travelStartDateTimeEnd) {
        TravelStartDateTimeEnd = travelStartDateTimeEnd;
    }

    public String getTravelStartDistance() {
        return TravelStartDistance;
    }

    public void setTravelStartDistance(String travelStartDistance) {
        TravelStartDistance = travelStartDistance;
    }

    public String getJobStartDateTime() {
        return JobStartDateTime;
    }

    public void setJobStartDateTime(String jobStartDateTime) {
        JobStartDateTime = jobStartDateTime;
    }

    public String getJobFinishDateTime() {
        return JobFinishDateTime;
    }

    public void setJobFinishDateTime(String jobFinishDateTime) {
        JobFinishDateTime = jobFinishDateTime;
    }

    public String getBreakStartDateTime() {
        return BreakStartDateTime;
    }

    public void setBreakStartDateTime(String breakStartDateTime) {
        BreakStartDateTime = breakStartDateTime;
    }

    public String getBreakFinishDateTime() {
        return BreakFinishDateTime;
    }

    public void setBreakFinishDateTime(String breakFinishDateTime) {
        BreakFinishDateTime = breakFinishDateTime;
    }

    public String getTravelFinishDateTimeStart() {
        return TravelFinishDateTimeStart;
    }

    public void setTravelFinishDateTimeStart(String travelFinishDateTimeStart) {
        TravelFinishDateTimeStart = travelFinishDateTimeStart;
    }

    public String getTravelFinishDateTime() {
        return TravelFinishDateTime;
    }

    public void setTravelFinishDateTime(String travelFinishDateTime) {
        TravelFinishDateTime = travelFinishDateTime;
    }

    public String getTravelFinishDistance() {
        return TravelFinishDistance;
    }

    public void setTravelFinishDistance(String travelFinishDistance) {
        TravelFinishDistance = travelFinishDistance;
    }

    public String getFatigueCompliance() {
        return FatigueCompliance;
    }

    public void setFatigueCompliance(String fatigueCompliance) {
        FatigueCompliance = fatigueCompliance;
    }

    public String getAttendDepot() {
        return AttendDepot;
    }

    public void setAttendDepot(String attendDepot) {
        AttendDepot = attendDepot;
    }

    public String getRegoNo() {
        return RegoNo;
    }

    public void setRegoNo(String regoNo) {
        RegoNo = regoNo;
    }

    public String getOdometer() {
        return Odometer;
    }

    public void setOdometer(String odometer) {
        Odometer = odometer;
    }

    public String getTraveledKilometers() {
        return TraveledKilometers;
    }

    public void setTraveledKilometers(String traveledKilometers) {
        TraveledKilometers = traveledKilometers;
    }

    public String getIsHaveMeal() {
        return isHaveMeal;
    }

    public void setIsHaveMeal(String isHaveMeal) {
        this.isHaveMeal = isHaveMeal;
    }

    public String getTraveledKilometersRateB() {
        return TraveledKilometersRateB;
    }

    public void setTraveledKilometersRateB(String traveledKilometersRateB) {
        TraveledKilometersRateB = traveledKilometersRateB;
    }
}
