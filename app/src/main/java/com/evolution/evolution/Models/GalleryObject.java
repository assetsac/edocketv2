package com.evolution.evolution.Models;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by mcgwapo on 10/9/15.
 */
@Parcel
public class GalleryObject {
    ArrayList<PhotoItem> mGallery;

    public GalleryObject() {
    }

    public GalleryObject(ArrayList<PhotoItem> mGallery) {
        this.mGallery = mGallery;
    }

    public ArrayList<PhotoItem> getmGallery() {
        return mGallery;
    }

    public void setmGallery(ArrayList<PhotoItem> mGallery) {
        this.mGallery = mGallery;
    }

}
