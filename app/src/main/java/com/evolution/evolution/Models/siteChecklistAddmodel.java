package com.evolution.evolution.Models;

import org.parceler.Parcel;

/**
 * Created by mcgwapo on 12/23/15.
 */
@Parcel
public class siteChecklistAddmodel {
    String AddHazard, InitialRisk, ControlMeasures, ResidualRisk, JobID, position, randomID;

    public siteChecklistAddmodel() {
    }

    public siteChecklistAddmodel(String addHazard, String initialRisk, String controlMeasures, String residualRisk, String jobID, String position, String randomID) {
        AddHazard = addHazard;
        InitialRisk = initialRisk;
        ControlMeasures = controlMeasures;
        ResidualRisk = residualRisk;
        JobID = jobID;
        this.position = position;
        this.randomID = randomID;
    }

    public String getAddHazard() {
        return AddHazard;
    }

    public void setAddHazard(String addHazard) {
        AddHazard = addHazard;
    }

    public String getInitialRisk() {
        return InitialRisk;
    }

    public void setInitialRisk(String initialRisk) {
        InitialRisk = initialRisk;
    }

    public String getControlMeasures() {
        return ControlMeasures;
    }

    public void setControlMeasures(String controlMeasures) {
        ControlMeasures = controlMeasures;
    }

    public String getResidualRisk() {
        return ResidualRisk;
    }

    public void setResidualRisk(String residualRisk) {
        ResidualRisk = residualRisk;
    }

    public String getJobID() {
        return JobID;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getRandomID() {
        return randomID;
    }

    public void setRandomID(String randomID) {
        this.randomID = randomID;
    }
}
