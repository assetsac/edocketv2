package com.evolution.evolution.Models;

import org.parceler.Parcel;

/**
 * Created by mcclynreyarboleda on 5/20/15.
 */
@Parcel
public class ModelRequirements {
    String RowID, Description, JobID,Quantity, RequirementID;

    public ModelRequirements() {
    }

    public ModelRequirements(String RowID, String description, String jobID, String quantity, String requirementID) {
        this.RowID = RowID;
        Description = description;
        JobID = jobID;
        Quantity = quantity;
        RequirementID = requirementID;
    }

    public String getRowID() {
        return RowID;
    }

    public void setRowID(String RowID) {
        this.RowID = RowID;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getJobID() {
        return JobID;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getRequirementID() {
        return RequirementID;
    }

    public void setRequirementID(String requirementID) {
        RequirementID = requirementID;
    }
}
