package com.evolution.evolution.Models;

/**
 * Created by mcclynreyarboleda on 5/19/15.
 */
public class Testingmode {

    String idFleet, nameFleet, urlFleet;

    public Testingmode() {

    }

    public Testingmode(String idFleet, String nameFleet, String urlFleet) {
        this.idFleet = idFleet;
        this.nameFleet = nameFleet;
        this.urlFleet = urlFleet;
    }

    public String getIdFleet() {
        return idFleet;
    }

    public void setIdFleet(String idFleet) {
        this.idFleet = idFleet;
    }

    public String getNameFleet() {
        return nameFleet;
    }

    public void setNameFleet(String nameFleet) {
        this.nameFleet = nameFleet;
    }

    public String getUrlFleet() {
        return urlFleet;
    }

    public void setUrlFleet(String urlFleet) {
        this.urlFleet = urlFleet;
    }
}
