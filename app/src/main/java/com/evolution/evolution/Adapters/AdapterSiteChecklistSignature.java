package com.evolution.evolution.Adapters;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.evolution.evolution.Helpers.Encoder;
import com.evolution.evolution.Interface.PhotoInterface;
import com.evolution.evolution.Interface.SiteChecklistSignatureInterface;
import com.evolution.evolution.Models.SiteCheckListSignatureModel;
import com.evolution.evolution.R;
import com.github.gcacace.signaturepad.views.SignaturePad;

import java.util.ArrayList;
import java.util.Calendar;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class AdapterSiteChecklistSignature extends RecyclerView.Adapter<AdapterSiteChecklistSignature.ViewHolder> {
    public ArrayList<SiteCheckListSignatureModel> mDataset;
    public Context context;
    public SiteChecklistSignatureInterface pListener;

    boolean ishaveSIgDraw = false;
    boolean isActiveSigView = false;
    public AdapterSiteChecklistSignature(ArrayList<SiteCheckListSignatureModel> mDataset, Context context, SiteChecklistSignatureInterface pListener) {
        this.mDataset = mDataset;
        this.context = context;
        this.pListener = pListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.sitechecklist_signature_item, null);

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.name.setText(mDataset.get(position).getTCName());
        if (mDataset.get(position).getAttachment().equals("")) {
            viewHolder.signature.setBackgroundResource(R.drawable.signaturebuttonno);
        } else {
            viewHolder.signature.setBackgroundResource(R.drawable.signaturebuttonnogree);
        }
        viewHolder.signature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(context, R.style.FullHeightDialog);
                dialog.setContentView(R.layout.dialog_signature);
                final LinearLayout cancel = (LinearLayout) dialog.findViewById(R.id.cancel);
                final LinearLayout save = (LinearLayout) dialog.findViewById(R.id.save);
                final LinearLayout save2 = (LinearLayout) dialog.findViewById(R.id.save2);
                final SignaturePad mSignaturePad = (SignaturePad) dialog.findViewById(R.id.signaturearea);
                final LinearLayout clear = (LinearLayout) dialog.findViewById(R.id.clear);
                final TextView signaturename = (TextView) dialog.findViewById(R.id.signaturename);
                final ImageView signatureprev = (ImageView) dialog.findViewById(R.id.signatureprev);
                signaturename.setText("Employee Signature");
                clear.setEnabled(false);
                save.setVisibility(save.GONE);
                save2.setVisibility(save2.VISIBLE);

                if (mDataset.get(position).getAttachment().equals("")) {
                    signatureprev.setVisibility(signatureprev.GONE);
                    mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                    isActiveSigView = false;
                } else {
                    byte[] decodedString = Base64.decode(mDataset.get(position).getAttachment(), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    signatureprev.setImageBitmap(decodedByte);
                    clear.setEnabled(true);
                    signatureprev.setVisibility(signatureprev.VISIBLE);
                    mSignaturePad.setVisibility(mSignaturePad.GONE);
                    isActiveSigView = true;
                }

                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                        String signaterImage = Encoder.encodeTobase64(signatureBitmap);
                        mDataset.get(position).setAttachedOn("" + Calendar.getInstance().getTimeInMillis());
                        mDataset.get(position).setAttachment(signaterImage);
                        pListener.updateSignature(mDataset);
                        dialog.dismiss();

                        if(!ishaveSIgDraw) {
                            mDataset.get(position).setAttachment("");
                            pListener.updateSignature(mDataset);
                        }
                    }
                });

                save2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!isActiveSigView) {
                            mDataset.get(position).setAttachment("");
                            pListener.updateSignature(mDataset);
                        } else {

                        }


                        dialog.dismiss();
                    }
                });

                clear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isActiveSigView = false;
                        mSignaturePad.clear();
                        clear.setEnabled(false);
                        signatureprev.setVisibility(signatureprev.GONE);
                        mSignaturePad.setVisibility(mSignaturePad.VISIBLE);


                        mDataset.get(position).setAttachment("");
                        pListener.updateSignature(mDataset);

                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
                    @Override
                    public void onStartSigning() {

                    }

                    @Override
                    public void onSigned() {
                        save.setEnabled(true);
                        clear.setEnabled(true);
                        ishaveSIgDraw = true;
                        save.setVisibility(save.VISIBLE);
                        save2.setVisibility(save2.GONE);
                    }

                    @Override
                    public void onClear() {
                        ishaveSIgDraw = false;
                        clear.setEnabled(false);
                        isActiveSigView = false;
                    }
                });
                dialog.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public LinearLayout signature;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            name = (TextView) itemLayoutView
                    .findViewById(R.id.name);
            signature = (LinearLayout) itemLayoutView
                    .findViewById(R.id.signature);
        }
    }
}
