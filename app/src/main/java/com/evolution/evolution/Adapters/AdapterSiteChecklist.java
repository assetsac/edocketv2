package com.evolution.evolution.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.evolution.evolution.Interface.SiteChecklistInterface;
import com.evolution.evolution.Models.siteChecklistAddmodel;
import com.evolution.evolution.R;

import java.util.ArrayList;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class AdapterSiteChecklist extends RecyclerView.Adapter<AdapterSiteChecklist.ViewHolder> {
    public ArrayList<siteChecklistAddmodel> sites;
    public Context context;
    public SiteChecklistInterface ee;

    public AdapterSiteChecklist(ArrayList<siteChecklistAddmodel> sites, Context context, SiteChecklistInterface ee) {
        this.sites = sites;
        this.context = context;
        this.ee = ee;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.addsitechecklistitem, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }
    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        viewHolder.hazardcounter.setText("#" + (position + 2));
        viewHolder.anyAdditionalHazard.setText(sites.get(position).getAddHazard());

        viewHolder.controlMeasures.setText(sites.get(position).getControlMeasures());

        String ss = sites.get(position).getInitialRisk() + "sd";
        String ss2 = sites.get(position).getResidualRisk() + "sd";
        Log.e("sdfsdf", ss + "--" + ss2);

        if (!ss.equals("nullsd")) {
            Log.e("checker-test 1", sites.get(position).getInitialRisk());
            if (sites.get(position).getInitialRisk().equals("High")) {
                ((RadioButton) viewHolder.radioGroup.getChildAt(0)).setChecked(true);
            } else if (sites.get(position).getInitialRisk().toString().equals("Med")) {
                ((RadioButton) viewHolder.radioGroup.getChildAt(1)).setChecked(true);
            } else if (sites.get(position).getInitialRisk().toString().equals("Low")) {
                ((RadioButton) viewHolder.radioGroup.getChildAt(2)).setChecked(true);
            }
            ee.updateAnyAdditionalHazard(sites, position);
        }

        if (!ss2.equals("nullsd")) {
            Log.e("checker-test 2", sites.get(position).getResidualRisk());
            if (sites.get(position).getResidualRisk().equals("High")) {
                ((RadioButton) viewHolder.radioGroup2.getChildAt(0)).setChecked(true);
            } else if (sites.get(position).getResidualRisk().toString().equals("Med")) {
                ((RadioButton) viewHolder.radioGroup2.getChildAt(1)).setChecked(true);
            } else if (sites.get(position).getResidualRisk().toString().equals("Low")) {
                ((RadioButton) viewHolder.radioGroup2.getChildAt(2)).setChecked(true);
            }
        }

        viewHolder.minushazard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("chickenpox", sites.get(position).getPosition() + " ");
                String idtoDelete = sites.get(position).getPosition() + "";
                if (idtoDelete.equals("null")) {
                    ee.removeitem(position, position + "");
                } else {
                    ee.removeitem(position, sites.get(position).getPosition());
                }
            }
        });

        viewHolder.anyAdditionalHazard.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                sites.get(position).setAddHazard(viewHolder.anyAdditionalHazard.getText().toString());
                ee.updateAnyAdditionalHazard(sites, position);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        viewHolder.controlMeasures.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                sites.get(position).setControlMeasures(viewHolder.controlMeasures.getText().toString());
                ee.controlMeasures(sites, position);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        viewHolder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                int radioButtonID = viewHolder.radioGroup.getCheckedRadioButtonId();
                View radioButton = viewHolder.radioGroup.findViewById(radioButtonID);
                int idx = viewHolder.radioGroup.indexOfChild(radioButton);
                switch (idx) {
                    case 0:
                        sites.get(position).setInitialRisk(idx + "");
                        ee.initialriskInter(sites, position);
                        break;
                    case 1:
                        sites.get(position).setInitialRisk(idx + "");
                        ee.initialriskInter(sites, position);
                        break;
                    case 2:
                        sites.get(position).setInitialRisk(idx + "");
                        ee.initialriskInter(sites, position);
                        break;
                }

            }
        });


        viewHolder.radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                int radioButtonID = viewHolder.radioGroup2.getCheckedRadioButtonId();
                View radioButton = viewHolder.radioGroup2.findViewById(radioButtonID);
                int idx = viewHolder.radioGroup2.indexOfChild(radioButton);
                switch (idx) {
                    case 0:
                        sites.get(position).setResidualRisk(idx + "");
                        ee.resiDualInter(sites, position);
                        break;
                    case 1:
                        sites.get(position).setResidualRisk(idx + "");
                        ee.resiDualInter(sites, position);
                        break;
                    case 2:
                        sites.get(position).setResidualRisk(idx + "");
                        ee.resiDualInter(sites, position);
                        break;
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return sites.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public EditText anyAdditionalHazard, controlMeasures;
        public LinearLayout minushazard;
        public RadioGroup radioGroup, radioGroup2;
        public TextView hazardcounter;
        public RadioButton initialriskhigh, initialriskmed, initialrisklow, residualhigh, residualmed, residualow;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            anyAdditionalHazard = (EditText) itemLayoutView.findViewById(R.id.anyAdditionalHazard);
            controlMeasures = (EditText) itemLayoutView.findViewById(R.id.controlMeasures);
            minushazard = (LinearLayout) itemLayoutView.findViewById(R.id.minushazard);

            radioGroup = (RadioGroup) itemLayoutView.findViewById(R.id.initialrisk);
            radioGroup2 = (RadioGroup) itemLayoutView.findViewById(R.id.residual);
            initialriskhigh = (RadioButton) itemLayoutView.findViewById(R.id.initialriskhigh);
            initialriskmed = (RadioButton) itemLayoutView.findViewById(R.id.initialriskmed);
            initialrisklow = (RadioButton) itemLayoutView.findViewById(R.id.initialrisklow);

            residualhigh = (RadioButton) itemLayoutView.findViewById(R.id.residualhigh);
            residualmed = (RadioButton) itemLayoutView.findViewById(R.id.residualmed);
            residualow = (RadioButton) itemLayoutView.findViewById(R.id.residualow);

            hazardcounter = (TextView) itemLayoutView.findViewById(R.id.hazardcounter);
        }
    }
}
