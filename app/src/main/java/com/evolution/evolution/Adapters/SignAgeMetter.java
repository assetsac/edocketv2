package com.evolution.evolution.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.evolution.evolution.Helpers.DatabaseHelper;
import com.evolution.evolution.Interface.SignagesInterface;
import com.evolution.evolution.Models.SignagesMeterModel;
import com.evolution.evolution.R;

import java.util.ArrayList;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class SignAgeMetter extends RecyclerView.Adapter<SignAgeMetter.ViewHolder> {
    public ArrayList<SignagesMeterModel> mDataset;
    public Context context;
    boolean aftercare;
    public SignagesInterface pListener3;
    String jobid;
    String mode;
    int positionpub;
    public DatabaseHelper db;

    public SignAgeMetter(ArrayList<SignagesMeterModel> myDataset, boolean aftercare, String jobid, String mode, Context context) {
        mDataset = myDataset;
        this.context = context;
        this.aftercare = aftercare;
        this.pListener3 = (SignagesInterface) context;
        this.jobid = jobid;
        this.mode = mode;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.signages_item, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        db = new DatabaseHelper(context);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        String[] m = mDataset.get(position).getImage().split("~");

        viewHolder.itemmeter.setTag(position + "");
        viewHolder.quantity.setTag(position + "");
        viewHolder.aftercaremeter.setTag(position + "");
        viewHolder.aftercarequantity.setTag(position + "");
        viewHolder.isaftercare.setTag(position + "");

        Glide.with(context).load(Integer.parseInt(m[0])).into(viewHolder.imagepm);
        positionpub = position;
        viewHolder.itemmeter.setText(mDataset.get(position).getMetress());
        viewHolder.quantity.setText(mDataset.get(position).getQuantity());
        viewHolder.aftercaremeter.setText(mDataset.get(position).getMetress2());
        viewHolder.aftercarequantity.setText(mDataset.get(position).getQuantity2());

        String after = mDataset.get(position).getAfter_care();
        if (!after.equals("null")) {
            if (after.equals("1")) {
                viewHolder.isaftercare.setChecked(true);
                if (aftercare) {
                    viewHolder.aftercaremeter.setEnabled(true);
                    viewHolder.aftercarequantity.setEnabled(true);
                }

                if(!aftercare) {
                    viewHolder.isaftercare.setChecked(false);
                    viewHolder.aftercaremeter.setText("");
                    viewHolder.aftercarequantity.setText("");
                    mDataset.get(position).setAfter_care("0");
                }

            } else if (after.equals("0")) {
                viewHolder.isaftercare.setChecked(false);
                viewHolder.aftercaremeter.setEnabled(false);
                viewHolder.aftercarequantity.setEnabled(false);
                viewHolder.isaftercare.setChecked(false);

            } else {
                viewHolder.isaftercare.setChecked(true);
                Log.e("log00", "-case " + after + mDataset.get(position).getMetress2());
            }
        }

        viewHolder.isaftercare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewHolder.isaftercare.isChecked()) {
                    viewHolder.aftercaremeter.setText(viewHolder.itemmeter.getText().toString());
                    viewHolder.aftercarequantity.setText(viewHolder.quantity.getText().toString());
                    mDataset.get(position).setMetress2(viewHolder.itemmeter.getText().toString());
                    mDataset.get(position).setQuantity2(viewHolder.quantity.getText().toString());
                    mDataset.get(position).setAfter_care("1");
                    pListener3.returnNewSign(mDataset, mode);
                    viewHolder.aftercaremeter.setEnabled(true);
                    viewHolder.aftercarequantity.setEnabled(true);
                } else {
                    viewHolder.aftercaremeter.setText("");
                    viewHolder.aftercarequantity.setText("");
                    mDataset.get(position).setMetress2("");
                    mDataset.get(position).setQuantity2("");
                    mDataset.get(position).setAfter_care("0");
                    pListener3.returnNewSign(mDataset, mode);
                    viewHolder.aftercaremeter.setEnabled(false);
                    viewHolder.aftercarequantity.setEnabled(false);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public ArrayList<SignagesMeterModel> getStudentList() {
        return mDataset;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public EditText itemmeter, quantity;
        public ImageView imagepm;
        public CheckBox isaftercare;
        public EditText aftercaremeter, aftercarequantity;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            itemmeter = (EditText) itemLayoutView
                    .findViewById(R.id.itemmeter);
            imagepm = (ImageView) itemLayoutView
                    .findViewById(R.id.imagepm);
            quantity = (EditText) itemLayoutView
                    .findViewById(R.id.quantity);
            isaftercare = (CheckBox) itemLayoutView
                    .findViewById(R.id.isaftercare);
            aftercaremeter = (EditText) itemLayoutView
                    .findViewById(R.id.aftercaremeter);
            aftercarequantity = (EditText) itemLayoutView
                    .findViewById(R.id.aftercarequantity);

            if (aftercare) {
                Log.e("Log", "Aftercare is true");
                isaftercare.setEnabled(true);
                aftercaremeter.setEnabled(true);
                aftercarequantity.setEnabled(true);
            } else {
                Log.e("Log", "Aftercare is false");
                isaftercare.setEnabled(false);
                isaftercare.setChecked(false);
                aftercaremeter.setText("");
                aftercarequantity.setText("");
            }


            itemmeter.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                    mDataset.get(Integer.parseInt(ViewHolder.this.itemmeter.getTag() + "")).setMetress(s.toString() + "");
                    pListener3.returnNewSign(mDataset, mode);
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });

            quantity.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                    mDataset.get(Integer.parseInt(ViewHolder.this.quantity.getTag() + "")).setQuantity(s.toString() + "");
                    pListener3.returnNewSign(mDataset, mode);
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
            });


            aftercaremeter.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {

                    mDataset.get(Integer.parseInt(ViewHolder.this.aftercaremeter.getTag() + "")).setMetress2(s.toString() + "");
                    pListener3.returnNewSign(mDataset, mode);
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
            });

            aftercarequantity.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                    mDataset.get(Integer.parseInt(ViewHolder.this.aftercarequantity.getTag() + "")).setQuantity2(s.toString() + "");
                    pListener3.returnNewSign(mDataset, mode);
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

            });


        }
    }
}
