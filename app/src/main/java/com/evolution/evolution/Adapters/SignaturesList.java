package com.evolution.evolution.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.evolution.evolution.Models.SiteCheckListSignatureModel;
import com.evolution.evolution.R;

import java.util.ArrayList;

/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class SignaturesList extends RecyclerView.Adapter<SignaturesList.ViewHolder> {
    public  ArrayList<SiteCheckListSignatureModel> mDataset;
    public Context context;

    public SignaturesList( ArrayList<SiteCheckListSignatureModel> mDataset, Context context) {
        this.mDataset = mDataset;
        this.context = context;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.testingitem, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }
    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        viewHolder.namefield.setText(mDataset.get(position).getTCName());
    }
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView namefield;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            namefield = (TextView) itemLayoutView
                    .findViewById(R.id.namefield);

        }
    }
}
