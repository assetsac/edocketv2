package com.evolution.evolution.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.evolution.evolution.Activities.Assets;
import com.evolution.evolution.Activities.EmployeeList;
import com.evolution.evolution.Activities.SignatureView;
import com.evolution.evolution.Activities.TimeSheetInfo;
import com.evolution.evolution.Activities.TimeSheets;
import com.evolution.evolution.Helpers.DatabaseHelper;
import com.evolution.evolution.Interface.EventInterface;
import com.evolution.evolution.Interface.TimesheetEvents;
import com.evolution.evolution.Models.TimeSheetObjects;
import com.evolution.evolution.Models.Timesheet;
import com.evolution.evolution.R;

import org.parceler.Parcels;

import java.util.ArrayList;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class AdapterFleetMember extends RecyclerView.Adapter<AdapterFleetMember.ViewHolder> implements View.OnClickListener {
    public ArrayList<Timesheet> mDataset;
    DatabaseHelper db;
    public String workerid;
    public Context context;
    private EventInterface callback;
    TimeSheets md;
    TimeSheetObjects data;

    public AdapterFleetMember(Context context) {
        callback = (EventInterface) context;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        md = new TimeSheets();
    }

    public AdapterFleetMember(ArrayList<Timesheet> myDataset, Context context, String ID, TimeSheetObjects data) {
        mDataset = myDataset;
        this.context = context;
        this.workerid = ID;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.fleetlist, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        db = new DatabaseHelper(context);
        final Timesheet m = mDataset.get(position);
        viewHolder.tvtinfo_text.setText(m.getName());

        if (db.checkTimesheetUploadSigExisting(m.getJobID(), m.getContactID())) {
            viewHolder.imagestatus.setImageResource(R.mipmap.checkme);
            viewHolder.duplicate.setVisibility(viewHolder.duplicate.VISIBLE);
        } else if (db.checkTimesheetUploadExisting(m.getJobID(), m.getContactID())) {
            viewHolder.imagestatus.setImageResource(R.mipmap.tpedit);
            viewHolder.duplicate.setVisibility(viewHolder.duplicate.VISIBLE);
        } else {
            viewHolder.duplicate.setVisibility(viewHolder.duplicate.GONE);
        }

        viewHolder.imagestatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent clickMe = new Intent(context, TimeSheetInfo.class);
                clickMe.putExtra("data", Parcels.wrap(data.getTimesheets().get(position)));
                clickMe.putExtra("position", position + "");
                clickMe.putExtra("status", data.getTimesheets().get(position).getDescription());
                view.getContext().startActivity(clickMe);
            }
        });

        viewHolder.tvtinfo_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent clickMe = new Intent(context, TimeSheetInfo.class);
                clickMe.putExtra("data", Parcels.wrap(data.getTimesheets().get(position)));
                clickMe.putExtra("position", position + "");
                clickMe.putExtra("status", data.getTimesheets().get(position).getDescription());
                view.getContext().startActivity(clickMe);
            }
        });

        viewHolder.duplicate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent c = new Intent(context, EmployeeList.class);
                c.putExtra("ID", m.getJobID());
                c.putExtra("position", position + "");
                c.putExtra("data", Parcels.wrap(m));
                c.putExtra("status", data.getTimesheets().get(position).getDescription());
                view.getContext().startActivity(c);
            }
        });

        if (Integer.parseInt(db.getCountTime(m.getJobID())) <= 1) {
            viewHolder.duplicate.setVisibility(viewHolder.duplicate.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return (null != mDataset ? mDataset.size() : 0);
    }

    @Override
    public void onClick(View view) {

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvtinfo_text;
        public ImageView imagestatus;
        public ImageButton duplicate;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tvtinfo_text = (TextView) itemLayoutView
                    .findViewById(R.id.fleetname);
            imagestatus = (ImageView) itemLayoutView.findViewById(R.id.imagestatus);
            duplicate = (ImageButton) itemLayoutView.findViewById(R.id.duplicate);
        }
    }
}
