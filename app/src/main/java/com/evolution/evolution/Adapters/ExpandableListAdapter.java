package com.evolution.evolution.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.evolution.evolution.Models.Timesheet_Upload;
import com.evolution.evolution.R;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;

    public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLasdtChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition);

        Log.e("adataer", childText);
        String[] m = childText.split("~");
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);


        TextView sumtraveltostartd = (TextView) convertView
                .findViewById(R.id.sumtraveltostartd);
        TextView sumtraveltostartt = (TextView) convertView
                .findViewById(R.id.sumtraveltostartt);

        TextView sumtraveltoendd = (TextView) convertView
                .findViewById(R.id.sumtraveltoendd);
        TextView sumtraveltoendt = (TextView) convertView
                .findViewById(R.id.sumtraveltoendt);

        TextView firstkm = (TextView) convertView
                .findViewById(R.id.firstkm);
        TextView secondkm = (TextView) convertView
                .findViewById(R.id.secondkm);

        TextView tatal1 = (TextView) convertView
                .findViewById(R.id.tatal1);

        TextView onsite1 = (TextView) convertView
                .findViewById(R.id.onsite1);
        TextView onsite2 = (TextView) convertView
                .findViewById(R.id.onsite2);
        TextView onsite3 = (TextView) convertView
                .findViewById(R.id.onsite3);
        TextView onsite4 = (TextView) convertView
                .findViewById(R.id.onsite4);


        TextView tatal2 = (TextView) convertView
                .findViewById(R.id.total2);


        TextView meal1 = (TextView) convertView
                .findViewById(R.id.meal1);
        TextView meal2 = (TextView) convertView
                .findViewById(R.id.meal2);
        TextView meal3 = (TextView) convertView
                .findViewById(R.id.meal3);
        TextView meal4 = (TextView) convertView
                .findViewById(R.id.meal4);


        TextView total3 = (TextView) convertView
                .findViewById(R.id.total3);


        TextView travelfrom1 = (TextView) convertView
                .findViewById(R.id.travelfrom1);
        TextView travelfrom2 = (TextView) convertView
                .findViewById(R.id.travelfrom2);
        TextView travelfrom3 = (TextView) convertView
                .findViewById(R.id.travelfrom3);
        TextView travelfrom4 = (TextView) convertView
                .findViewById(R.id.travelfrom4);


        TextView total4 = (TextView) convertView
                .findViewById(R.id.total4);


        try {
            JSONArray j = new JSONArray(childText);
            for (int c = 0; c < j.length(); c++) {
                JSONObject sd = j.getJSONObject(c);
                Log.e("sdfsdf", sd.getString("TravelStartDateTime"));

                dateTimePopulate(sumtraveltostartd, Long.parseLong(sd.getString("TravelStartDateTime")), true);
                dateTimePopulate(sumtraveltostartt, Long.parseLong(sd.getString("TravelStartDateTime")), false);
                dateTimePopulate(sumtraveltoendd, Long.parseLong(sd.getString("TravelStartDateTimeEnd")), true);
                dateTimePopulate(sumtraveltoendt, Long.parseLong(sd.getString("TravelStartDateTimeEnd")), false);

                tatal1.setText(getTimeDifference(sumtraveltostartd.getText().toString() + " " + sumtraveltostartt.getText().toString(), sumtraveltoendd.getText().toString() + " " + sumtraveltoendt.getText().toString()));

                dateTimePopulate(onsite1, Long.parseLong(sd.getString("JobStartDateTime")), true);
                dateTimePopulate(onsite2, Long.parseLong(sd.getString("JobStartDateTime")), false);

                dateTimePopulate(onsite3, Long.parseLong(sd.getString("JobFinishDateTime")), true);
                dateTimePopulate(onsite4, Long.parseLong(sd.getString("JobFinishDateTime")), false);

                tatal2.setText(getTimeDifference(onsite1.getText().toString() + " " + onsite2.getText().toString(), onsite3.getText().toString() + " " + onsite4.getText().toString()));

                if (!sd.getString("BreakStartDateTime").equals("")) {
                    dateTimePopulate(meal1, Long.parseLong(sd.getString("BreakStartDateTime")), true);
                    dateTimePopulate(meal2, Long.parseLong(sd.getString("BreakStartDateTime")), false);
                } else {
                    meal1.setText("Date");
                    meal2.setText("Time");
                }

                if (!sd.getString("BreakFinishDateTime").equals("")) {
                    dateTimePopulate(meal3, Long.parseLong(sd.getString("BreakFinishDateTime")), true);
                    dateTimePopulate(meal4, Long.parseLong(sd.getString("BreakFinishDateTime")), false);
                } else {
                    meal3.setText("Date");
                    meal4.setText("Time");
                }

                total3.setText(getTimeDifference(meal1.getText().toString() + " " + meal2.getText().toString(), meal3.getText().toString() + " " + meal4.getText().toString()));

                dateTimePopulate(travelfrom1, Long.parseLong(sd.getString("TravelFinishDateTimeStart")), true);
                dateTimePopulate(travelfrom2, Long.parseLong(sd.getString("TravelFinishDateTimeStart")), false);

                dateTimePopulate(travelfrom3, Long.parseLong(sd.getString("TravelFinishDateTime")), true);
                dateTimePopulate(travelfrom4, Long.parseLong(sd.getString("TravelFinishDateTime")), false);

                total4.setText(getTimeDifference(travelfrom1.getText().toString() + " " + travelfrom2.getText().toString(), travelfrom3.getText().toString() + " " + travelfrom4.getText().toString()));

                firstkm.setText(sd.getString("TravelStartDistance") + "km");
                secondkm.setText(sd.getString("TravelFinishDistance") + "km");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void dateTimePopulate(TextView v, long s, boolean f) {
        DateFormat date = new SimpleDateFormat("dd MMM yyyy");
        DateFormat time = new SimpleDateFormat("HH:mm");
        long milliSeconds = s;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        if (f) {
            v.setText(date.format(calendar.getTime()));
        } else {
            v.setText(time.format(calendar.getTime()));
        }
    }

    public String getTimeDifference(String first, String second) {
        long diffMinutes = 0;
        long diffHours = 0;
        SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy HH:mm");
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse(first);
            d2 = format.parse(second);
            long diff = d2.getTime() - d1.getTime();
            diffMinutes = diff / (60 * 1000) % 60;
            diffHours = diff / (60 * 60 * 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return diffHours + "h  " + diffMinutes + "m";
    }

}




