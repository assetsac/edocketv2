package com.evolution.evolution.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.evolution.evolution.R;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class AdapterMoreMenu extends RecyclerView.Adapter<AdapterMoreMenu.ViewHolder> {
    public String[] mDataset;
    public int[] mIconSet;
    public Context context;

    public AdapterMoreMenu(String[] myDataset, int[] mIconSet, Context context) {
        mDataset = myDataset;
        this.mIconSet = mIconSet;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.menuitem, null);

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {


        viewHolder.tvtinfo_text.setText(mDataset[position].toString());
        viewHolder.avatar.setImageResource(mIconSet[position]);
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvtinfo_text;
        public ImageView avatar;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            tvtinfo_text = (TextView) itemLayoutView
                    .findViewById(R.id.info_text);

            avatar = (ImageView) itemLayoutView.findViewById(R.id.avatar);


        }
    }
}
