package com.evolution.evolution.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.evolution.evolution.Activities.JobView;
import com.evolution.evolution.Activities.TimesheetExpandable;
import com.evolution.evolution.Helpers.DatabaseHelper;
import com.evolution.evolution.Models.SignagesMeterModel;
import com.evolution.evolution.Models.SingageAudit;
import com.evolution.evolution.R;
import com.google.gson.Gson;

import org.parceler.Parcels;

import java.util.ArrayList;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class AdapterJobMenu extends RecyclerView.Adapter<AdapterJobMenu.ViewHolder> {
    private static final String TAG = "AdapterJobMenu";
    public String[] mDataset;
    public int[] mIconSet;
    public Context context;
    String idSelected;
    DatabaseHelper db;
    JobView getID;
    String id;

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterJobMenu(String[] myDataset, int[] mIconSet, String idSelected, Context context, String id) {
        mDataset = myDataset;
        this.mIconSet = mIconSet;
        this.context = context;
        this.idSelected = idSelected;
        this.id = id;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.jobmenu, null);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        getID = new JobView();
        db = new DatabaseHelper(context);

        final CharSequence cs1 = "null";

        int sizeofTao = db.getAllTimeSigBy(id).size();
        int mdd = db.counttimeSheetTao(id);

        if (sizeofTao == mdd) {
            if (position == 4) {
                viewHolder.checkker.setImageDrawable(context.getResources().getDrawable(R.drawable.circlegreen));
            }
        }

        boolean ishave2;
        ishave2 = getSignageAudit(id).contains(cs1);

        //PreStart Checklist
        if (position == 2) {
            if (checkPreStartChecklist(id)) {
                viewHolder.checkker.setImageDrawable(context.getResources().getDrawable(R.drawable.circlegreen));
            }
        }

        if (!db.checkOktoupload(id)) {
            if (position == 3) {
                if (!metresQtyChecker(id)) {
                    viewHolder.checkker.setImageDrawable(context.getResources().getDrawable(R.drawable.circlegreen));
                }
            }
        }

        if (db.checkExistCreateJObs(idSelected) != null) {
            viewHolder.menuname.setText(mDataset[position].toString());
            viewHolder.avatar.setImageResource(mIconSet[position]);
            if (db.checkExistCreateJObs3(idSelected).equals("mayroon")) {
                if (position == 1) {
                    viewHolder.checkker.setImageDrawable(context.getResources().getDrawable(R.drawable.circlegreen));
                }
            }else if(db.checkExistCreateJObs3(idSelected).equals("mayroon2")) {
                viewHolder.menuname.setText(mDataset[position].toString());
                viewHolder.avatar.setImageResource(mIconSet[position]);
            }
        } else if (db.checkExistCreateJObs2(idSelected) != null) {
            viewHolder.menuname.setText(mDataset[position].toString());
            viewHolder.avatar.setImageResource(mIconSet[position]);
        } else {
            viewHolder.menuname.setText(mDataset[position].toString());
            viewHolder.avatar.setImageResource(mIconSet[position]);
        }

        if (db.checkJobstatus(id)) {
            if (db.getJobStatusStats(id).getSingageAudit().equals("1")) {

            } else {
                if (position == 3) {
                    if (!metresQtyChecker(id)) {
                        viewHolder.checkker.setImageDrawable(context.getResources().getDrawable(R.drawable.circlegreen));
                    }
                }
            }
        }

    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public String getSignageAudit(String ids) {
        String temp = "";
        temp = "{\"mac\":" + db.getSignageAudt(ids) + "}";
        return temp;
    }


    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView menuname;
        public ImageView avatar;
        public ImageView checkker;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            menuname = (TextView) itemLayoutView
                    .findViewById(R.id.menuname);
            avatar = (ImageView) itemLayoutView.findViewById(R.id.avatar);
            checkker = (ImageView) itemLayoutView.findViewById(R.id.checkker);
        }
    }

    public boolean checkifAftercareisOK(String ids) {
        boolean s = false;
        String[] signages = db.getAllSelectedSignstoUpload(ids, "0").split("~");
        for (int i = 0; i < signages.length; i++) {
            if (signages[i].contains("AfterCare:1")) {
                String d = signages[i] + "s";
                if (d.contains("^AfterCareQty:nulls") || (d.contains("^AfterCareQty:s")) || (d.contains("^AfterCareQty:0s"))) {
                    s = true;
                }
                if ((signages[i].contains("^AfterCareMetres:null")) || (signages[i].contains("^AfterCareMetres:^")) || (signages[i].contains("^AfterCareMetres:0^"))) {
                    s = true;
                }
            }
        }

        String[] signages2 = db.getAllSelectedSignstoUpload(ids, "1").split("~");
        for (int i = 0; i < signages2.length; i++) {
            if (signages2[i].contains("AfterCare:1")) {
                Log.e("loveis", signages2[i]);
                String d = signages2[i] + "s";
                if (d.contains("^AfterCareQty:nulls") || (d.contains("^AfterCareQty:s")) || (d.contains("^AfterCareQty:0s"))) {
                    s = true;
                }
                if ((signages2[i].contains("^AfterCareMetres:null")) || (signages2[i].contains("^AfterCareMetres:^")) || (signages2[i].contains("^AfterCareMetres:0^"))) {
                    s = true;
                }
            }
        }

        return s;
    }

    public boolean checkPreStartChecklist(String id) {
        boolean status = true;
        for (int ss = 0; ss < db.getallDriverTimesheets(id).getTimesheets().size(); ss++) {
            if (!db.checkPresiteChecklistinfo(id, db.getallDriverTimesheets(id).getTimesheets().get(ss).getContactID())) {
                status = false;
            }
        }
        return status;
    }

    public boolean metresQtyChecker(String ids) {
        boolean s = false;

        if ((db.getAllSelectedSignstoUpload(ids, "0").contains("^Metres:null")) || (db.getAllSelectedSignstoUpload(ids, "0").contains("^Metres:^")) || (db.getAllSelectedSignstoUpload(ids, "0").contains("^Metres:0^"))) {
            s = true;
        }
        if ((db.getAllSelectedSignstoUpload(ids, "1").contains("^Metres:null")) || (db.getAllSelectedSignstoUpload(ids, "1").contains("^Metres:^")) || (db.getAllSelectedSignstoUpload(ids, "1").contains("^Metres:0^"))) {
            s = true;
        }

        if (db.getAllSelectedSignstoUpload(ids, "1").contains("^Qty:null") || (db.getAllSelectedSignstoUpload(ids, "1").contains("^Qty:^")) || (db.getAllSelectedSignstoUpload(ids, "1").contains("^Qty:0^"))) {
            s = true;
        }

        if (db.getAllSelectedSignstoUpload(ids, "0").contains("^Qty:null") || (db.getAllSelectedSignstoUpload(ids, "0").contains("^Qty:^")) || (db.getAllSelectedSignstoUpload(ids, "0").contains("^Qty:0^"))) {
            s = true;
        }
        if (checkifAftercareisOK(ids)) {
            s = true;
        }
        return s;
    }
}
