package com.evolution.evolution.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.evolution.evolution.Activities.EmployeeList;
import com.evolution.evolution.Activities.TimeSheetInfo;
import com.evolution.evolution.Activities.TimeSheets;
import com.evolution.evolution.Helpers.DatabaseHelper;
import com.evolution.evolution.Interface.EventInterface;
import com.evolution.evolution.Models.TimeSheetObjects;
import com.evolution.evolution.Models.Timesheet;
import com.evolution.evolution.R;

import org.parceler.Parcels;

import java.util.ArrayList;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class AdapterTimesheetSummary extends RecyclerView.Adapter<AdapterTimesheetSummary.ViewHolder> implements View.OnClickListener {
    public ArrayList<Timesheet> mDataset;
    DatabaseHelper db;
    public String workerid;
    public Context context;
    private EventInterface callback;
    TimeSheets md;
    TimeSheetObjects data;

    public AdapterTimesheetSummary(Context context) {
        callback = (EventInterface) context;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        md = new TimeSheets();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.timesheetsummary, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        db = new DatabaseHelper(context);

        final Timesheet m = mDataset.get(position);
        viewHolder.tvtinfo_text.setText(m.getName());

    }

    @Override
    public int getItemCount() {
        return (null != mDataset ? mDataset.size() : 0);
    }

    @Override
    public void onClick(View view) {

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvtinfo_text;
        public ImageView imagestatus;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tvtinfo_text = (TextView) itemLayoutView
                    .findViewById(R.id.fleetname);
            imagestatus = (ImageView) itemLayoutView.findViewById(R.id.imagestatus);
        }
    }
}
