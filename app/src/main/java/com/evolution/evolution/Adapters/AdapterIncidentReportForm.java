package com.evolution.evolution.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.evolution.evolution.R;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class AdapterIncidentReportForm extends RecyclerView.Adapter<AdapterIncidentReportForm.ViewHolder> {
    public String[] mDataset;
    public int[] mIconSet;
    public Context context;

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterIncidentReportForm(String[] myDataset, Context context) {
        mDataset = myDataset;
        this.mIconSet = mIconSet;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.incidentreportformitem, null);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        viewHolder.tvtinfo_text.setText(mDataset[position].toString());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvtinfo_text;
        public ImageView avatar;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            tvtinfo_text = (TextView) itemLayoutView
                    .findViewById(R.id.info_text);

            avatar = (ImageView) itemLayoutView.findViewById(R.id.avatar);


        }
    }
}
