package com.evolution.evolution.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.evolution.evolution.R;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class AdapterSafetyForms extends RecyclerView.Adapter<AdapterSafetyForms.ViewHolder> {
    public String[] mDataset;
    public Context context;

    public AdapterSafetyForms(String[] myDataset, Context context) {
        mDataset = myDataset;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.safetyformitems, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.tvtinfo_text.setText(mDataset[position].toString());
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvtinfo_text;
        public ImageView avatar;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            tvtinfo_text = (TextView) itemLayoutView
                    .findViewById(R.id.requirementname);

        }
    }
}
