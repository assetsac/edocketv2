package com.evolution.evolution.Adapters;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.evolution.evolution.Helpers.DatabaseHelper;
import com.evolution.evolution.Models.ModelPreSiteChecklistCarParts;
import com.evolution.evolution.Models.PhotoItem;
import com.evolution.evolution.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by mcclynreyarboleda on 4/1/15.
 */
public class AdapterPreSiteChecklost extends PagerAdapter {
    private static final String TAG = "AdapterPreSiteChecklost";
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    int[] car1view;
    private int screenSize;
    private DatabaseHelper db;
    private String Jobid;
    private String ContactID;
    private int viewpager;
    private ArrayList<ModelPreSiteChecklistCarParts> preSiteChecllistItem;

    public AdapterPreSiteChecklost(int[] car1view, Context context, int screenSize, String Jobid, String ContactID) {
        mContext = context;
        this.car1view = car1view;
        this.screenSize = screenSize;
        this.Jobid = Jobid;
        this.ContactID = ContactID;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return car1view.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.pager_vehicle_item, container, false);
        LinearLayout firstlayer = (LinearLayout) itemView.findViewById(R.id.firstlayer);
        db = new DatabaseHelper(mContext);
        preSiteChecllistItem = db.getAllPreSiteCheckistPartsView(Jobid, position + 1, ContactID);
        TextView textView14 = (TextView) itemView.findViewById(R.id.textView14);
        firstlayer.setBackground(ContextCompat.getDrawable(mContext, car1view[position]));
        getAllChildElements(firstlayer, preSiteChecllistItem);
        container.addView(itemView);
        Log.e(TAG, "instantiateItemposition: " + position);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    public final void getAllChildElements(LinearLayout layoutCont, ArrayList<ModelPreSiteChecklistCarParts> preSiteChecllistItem) {
        if (layoutCont == null) return;

        final int mCount = layoutCont.getChildCount();

        // Loop through all of the children.
        for (int i = 0; i < mCount; ++i) {
            final View mChild = layoutCont.getChildAt(i);
            if (mChild instanceof LinearLayout) {
                LinearLayout ss = (LinearLayout) mChild;
                final int mCount3 = ss.getChildCount();
                for (int t = 0; t < mCount3; ++t) {
                    final View mChild2 = ss.getChildAt(t);
                    if (mChild2 instanceof View) {
                        ViewGroup.LayoutParams params = mChild2.getLayoutParams();
                        double d = screenSize;
                        DecimalFormat f = new DecimalFormat("##.00");
                        String zzz = f.format(d) + "";
                        params.width = Integer.parseInt(zzz.replace(".00", ""));
                        params.height = Integer.parseInt("" + zzz.replace(".00", ""));
                        for (int z = 0; z < preSiteChecllistItem.size(); z++) {
                            String rowandcolumn = preSiteChecllistItem.get(z).getColumn() + "," + preSiteChecllistItem.get(z).getRow();
                            if (rowandcolumn.equals(mChild2.getTag().toString())) {
                                mChild2.setBackgroundResource(R.color.transparentblueselected);
                            }
                        }
                        mChild2.setLayoutParams(params);

                    } else {
                    }
                }
            } else {

            }
        }
    }

}
