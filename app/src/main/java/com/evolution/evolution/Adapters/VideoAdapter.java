package com.evolution.evolution.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.evolution.evolution.Models.VideoModel;
import com.evolution.evolution.R;
import com.evolution.evolution.Activities.VideoViewActivity;

import java.util.ArrayList;

/**
 * Created by mcarboleda on 27/05/2016.
 */
public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder> {
    ArrayList<VideoModel> item;
    public Context context;

    public VideoAdapter(ArrayList<VideoModel> item, Context context) {
        this.item = item;
        this.context = context;
    }

    @Override
    public VideoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.videositem, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(VideoAdapter.ViewHolder holder, final int position) {
        Glide.with(context).load(item.get(position).getVideo_url()).into(holder.videoPreview);
        holder.videoPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent openVideoView = new Intent(context, VideoViewActivity.class);
                openVideoView.putExtra("filePath", item.get(position).getVideo_url());
                openVideoView.putExtra("id", item.get(position).get_id());
                context.startActivity(openVideoView);
            }
        });
    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView videoPreview;
        public RelativeLayout parentVideo;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            videoPreview = (ImageView) itemLayoutView.findViewById(R.id.videoPreview);
            parentVideo = (RelativeLayout) itemLayoutView.findViewById(R.id.parentVideo);
        }
    }
}