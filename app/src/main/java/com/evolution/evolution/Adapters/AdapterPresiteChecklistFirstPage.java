package com.evolution.evolution.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.evolution.evolution.Activities.PreStartChecklist;
import com.evolution.evolution.Activities.TimeSheets;
import com.evolution.evolution.Helpers.DatabaseHelper;
import com.evolution.evolution.Interface.EventInterface;
import com.evolution.evolution.Models.Job;
import com.evolution.evolution.Models.TimeSheetObjects;
import com.evolution.evolution.Models.Timesheet;
import com.evolution.evolution.R;

import org.parceler.Parcels;

import java.util.ArrayList;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class AdapterPresiteChecklistFirstPage extends RecyclerView.Adapter<AdapterPresiteChecklistFirstPage.ViewHolder> implements View.OnClickListener {
    public ArrayList<Timesheet> mDataset;
    DatabaseHelper db;
    public String workerid;
    public Context context;
    private EventInterface callback;
    TimeSheets md;
    TimeSheetObjects data;
    Job mJobObject;

    public AdapterPresiteChecklistFirstPage(Context context) {
        callback = (EventInterface) context;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        md = new TimeSheets();
    }

    public AdapterPresiteChecklistFirstPage(ArrayList<Timesheet> myDataset, Context context, String ID, TimeSheetObjects data, Job mJobObject) {
        mDataset = myDataset;
        this.context = context;
        this.workerid = ID;
        this.mJobObject = mJobObject;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.fleetlist, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        db = new DatabaseHelper(context);
        final Timesheet m = mDataset.get(position);
        viewHolder.tvtinfo_text.setText(m.getName());

//        if (db.checkTimesheetUploadSigExisting(m.getJobID(), m.getContactID())) {
//            viewHolder.imagestatus.setImageResource(R.mipmap.checkme);
//            viewHolder.duplicate.setVisibility(viewHolder.duplicate.VISIBLE);
//        } else if (db.checkTimesheetUploadExisting(m.getJobID(), m.getContactID())) {
//            viewHolder.imagestatus.setImageResource(R.mipmap.tpedit);
//            viewHolder.duplicate.setVisibility(viewHolder.duplicate.VISIBLE);
//        } else {
//            viewHolder.duplicate.setVisibility(viewHolder.duplicate.GONE);
//        }

        viewHolder.imagestatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentPresite(m, view);
            }
        });

        viewHolder.tvtinfo_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentPresite(m, view);
            }
        });

        viewHolder.duplicate.setVisibility(viewHolder.duplicate.GONE);

        if (db.checkPresiteChecklistinfo(mJobObject.getJobID(),  m.getContactID())) {
            viewHolder.imagestatus.setImageResource(R.mipmap.checkme);
        }
    }

    public void intentPresite(Timesheet m, View view) {
        Intent c = new Intent(context, PreStartChecklist.class);
        c.putExtra("timesheet", Parcels.wrap(m));
        c.putExtra("data", Parcels.wrap(mJobObject));
        view.getContext().startActivity(c);
    }

    @Override
    public int getItemCount() {
        return (null != mDataset ? mDataset.size() : 0);
    }

    @Override
    public void onClick(View view) {

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvtinfo_text;
        public ImageView imagestatus;
        public ImageView duplicate;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tvtinfo_text = (TextView) itemLayoutView
                    .findViewById(R.id.fleetname);
            imagestatus = (ImageView) itemLayoutView.findViewById(R.id.imagestatus);
            duplicate = (ImageButton) itemLayoutView.findViewById(R.id.duplicate);


        }
    }

}
