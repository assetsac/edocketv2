package com.evolution.evolution.Interface;

import com.evolution.evolution.Models.SignagesMeterModel;

import java.util.ArrayList;

/**
 * Created by mcgwapo on 11/18/15.
 */
public interface SignagesInterface {
    public void returnNewSign(ArrayList<SignagesMeterModel> mDataset, String mode);
}
