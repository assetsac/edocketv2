package com.evolution.evolution.Interface;

import com.evolution.evolution.Helpers.Constants;
import com.evolution.evolution.Models.CheckManstatStatus;
import com.evolution.evolution.Models.JobObjects;
import com.evolution.evolution.Models.JobRosterList;
import com.evolution.evolution.Models.Notes;
import com.evolution.evolution.Models.RosterStatus;

import java.util.Map;
import retrofit.Callback;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by mcclynreyarboleda on 3/27/15.
 */
public interface APIInterface {

    //updateandsync
    @FormUrlEncoded
    @POST(Constants.REQUEST_UPLOAD)
    void uploads(@Header(Constants.TOKEN_ID) String token, @Header(Constants.AUT) String auth, @FieldMap Map<String, String> params, Callback<Object> cb);

    @FormUrlEncoded
    @POST(Constants.FAIL_SAFE)
    void uploadsFailFE(@Header(Constants.TOKEN_ID) String token, @Header(Constants.AUT) String auth, @FieldMap Map<String, String> params, Callback<Object> cb);

    //Generate Job Image Report
    @FormUrlEncoded
    @POST(Constants.jobImageReport)
    void jobimageReportGenerator(@Header(Constants.TOKEN_ID) String token, @Header(Constants.AUT) String auth, @FieldMap Map<String, String> params, Callback<Object> cb);

    //Generate Job Image Report
    @FormUrlEncoded
    @POST(Constants.PRESTARTCHECKLIST)
    void jobpreStartChecklistReportGenerator(@Header(Constants.TOKEN_ID) String token, @Header(Constants.AUT) String auth, @FieldMap Map<String, String> params, Callback<Object> cb);


    //updateandsync
    @FormUrlEncoded
    @POST(Constants.SiteCheckListSync)
    void siteChecklistSync(@Header(Constants.TOKEN_ID) String token, @Header(Constants.AUT) String auth, @FieldMap Map<String, String> params, Callback<Object> cb);

    // Login
    @FormUrlEncoded
    @POST(Constants.REQUEST_LOGIN)
    void getLogin(@Header(Constants.AUT) String auth, @FieldMap Map<String, String> params, Callback<Object> cb);

    // Get all jobs
    @FormUrlEncoded
    @POST(Constants.REQUEST_JOBS)
    void downloadActivities(@Header(Constants.TOKEN_ID) String token, @Header(Constants.AUT) String auth, @FieldMap Map<String, String> params, Callback<JobObjects> cb);

    // Get all jobs
    @GET(Constants.notes)
    void getNotes(@Header(Constants.TOKEN_ID) String token, @Header(Constants.AUT) String auth, Callback<Notes> cb);

    @GET(Constants.JOBSYNC + "{id}")
    void checkJob(@Header(Constants.TOKEN_ID) String token, @Header(Constants.AUT) String auth, @Path("id") String jobID, Callback<Object> cb);

    @GET(Constants.JOBEXIST + "{id}")
    void checkExist(@Header(Constants.TOKEN_ID) String token, @Header(Constants.AUT) String auth, @Path("id") String jobID, Callback<Object> cb);

    @GET(Constants.checkReports + "{id}")
    void checReport(@Header(Constants.TOKEN_ID) String token, @Header(Constants.AUT) String auth, @Path("id") String jobID, Callback<Object> cb);

    // Get manstat status
    @GET(Constants.CHECKMANSTAT)
    void getCheckManstat(@Header(Constants.TOKEN_ID) String token, @Header(Constants.AUT) String auth, Callback<CheckManstatStatus> cb);

    //updateandsync
    @Multipart
    @POST(Constants.UPLOADPHOTO)
    void uploadPhotos(@Header(Constants.TOKEN_ID) String token, @Header(Constants.AUT) String auth, @Query("jobId") String jobId, @Query("geotag") String geotag, @Part("image") TypedFile file, Callback<Object> cb);

    //updateandsync
    @Multipart
    @POST(Constants.UPLOADVIDEO)
    void uploadVideos(@Header(Constants.TOKEN_ID) String token, @Header(Constants.AUT) String auth, @Part("jobID") String jobID, @Part("subject") String subject,@Part("note") String note, @Part("startCapture") String startCapture, @Part("endCapture") String endCapture,@Part("startLocation") String startLocation,@Part("endLocation") String endLocation, @Part("video") TypedFile file, Callback<Object> cb);

    // Get all jobs
    @FormUrlEncoded
    @POST(Constants.ROSTER_STATUS)
    void rosterStatus(@Header(Constants.TOKEN_ID) String token, @Header(Constants.AUT) String auth, @FieldMap Map<String, String> params, Callback<JobRosterList> cb);

}
