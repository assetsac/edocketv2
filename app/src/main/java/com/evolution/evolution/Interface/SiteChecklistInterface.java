package com.evolution.evolution.Interface;

import com.evolution.evolution.Models.siteChecklistAddmodel;

import java.util.ArrayList;

/**
 * Created by mcgwapo on 7/28/15.
 */
public interface SiteChecklistInterface {
    void removeitem(int id, String iddb);
    void updateAnyAdditionalHazard(ArrayList<siteChecklistAddmodel> sitews, int position);
    void controlMeasures(ArrayList<siteChecklistAddmodel> sitews, int position);
    void initialriskInter(ArrayList<siteChecklistAddmodel> sitews, int position);
    void resiDualInter(ArrayList<siteChecklistAddmodel> sitews, int position);
}
