package com.evolution.evolution.Interface;

import com.evolution.evolution.Models.SiteCheckListSignatureModel;

import java.util.ArrayList;

/**
 * Created by McClynrey on 3/2/2016.
 */
public interface SiteChecklistSignatureInterface {

    void updateSignature(ArrayList<SiteCheckListSignatureModel> mDataset);
}
